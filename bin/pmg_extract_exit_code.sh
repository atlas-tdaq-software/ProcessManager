#!/bin/sh

declare fileName;
declare errorMarker="ERROR";
declare exitID="EXIT";

# The help
help() {
    echo "This script uses the ouput file produced by \"pmg_start_app\" or or \"pmg_wait_app\" to extract the process exit code."
    echo ""
    echo "Usage: pmg_extract_exit_code.sh -f <file name>"
    echo ""
    echo "*Return value*: the process exit code as defined in the input file."
    echo "*Error detection*: if any error happens then this script output will contain the string \"$errorMarker\"."
}

# Parse command line
while test $# != 0; do
    case "$1" in
        -[h?x]*) help ; exit ;;
        -f)     shift; fileName=$1 ;;
    esac
    shift;
done

# Check the file has been provided
if [ -z "$fileName" ]; then
    echo "$errorMarker! The file name is requested!"
    echo ""
    help;
    exit;
fi

# Grep the file and return the process exit code
if [ -a "$fileName" ]; then
    exitLine=`grep "$exitID" "$fileName"`;
    if [ -z "$exitLine" ]; then
	echo "$errorMarker! The file format is not valid!";
    else
	exit $(echo "$exitLine" | cut -f 2);
    fi
else
    echo "$errorMarker! File \"$fileName\" does not exist";
fi
