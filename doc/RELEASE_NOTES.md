# ProcessManager

The `ProcessManager` twiki can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltProcessManager).

## tdaq-09-03-00

**Changes in public APIs**:

-   Java: added a method to the `PmgClient` class in order get all the processes running on a host and in the context of a defined partition.

**Internal changes**:   

-   Implemented the `daq tokens` mechanism;
-   Java: increased max file size for log retrieval;
-   Use full path of pmglauncher binary.

## tdaq-09-02-01

**Internal changes**:   

-   `pmg_start_app`: fixed issue with parsing complex environment in command line options.


## tdaq-09-00-00

**Changes in public APIs**:

-   Added information about the availability of process out/err files:  

    The `PMGProcessStatusInfo` structure has been extended with two additional attributes (`out_is_available` and `err_is_available`) indicating whether the concerned application has produced any `out` or `err` log.
  
-   Using smart pointers in client library:  

    All the references to `Process` instances are encapsulated in shared pointers. 

**Internal changes**:

-   Not deleting or changing permissions of err/out files if they are not regular files (this covers the case in which the err and/or out streams are sent to `/dev/null`).