#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include <string.h>


/*! \file PMGLauncher.cxx
  \brief The first stage launcher.
    
  This very simple executable is the one executed in the daq::pmg::Application::start().
  When the ProcessManager server is running in the super-user mode (i.e., it is able to start
  processes on behalf of a different user with the right process owner) this binary must
  have the SETUID bit set. In this case it just changes the process user to the requested one
  and then execs the daq::pmg::Launcher (which will start the real process). 
  This is needed because the LD_LIBRARY_PATH variable is ignored for binaries with the
  SETUID bit.
  It receives as arguments the LD_LIBRARY_PATH environment variable value, the manifest to pass to
  the Launcher and the final user of the process to be started.

*/

int main(int argc, char** argv) {
  uid_t effective_uid = 0; /* effective user id of this process */
  uid_t final_uid = 0; /* the final user of the process to start */

  struct passwd* pws = NULL; /* pointer to structure used to get the final user groups */
  int sgResult = 0; /* result of call to setgid */
  int ng = 0; /* the number of groups the final user belongs to */
  gid_t* groups = NULL; /* array of groups the final user belongs to */
  int sgsResult = 0; /* result of call to setgroups */
  int uidResult = 0; /* result of call to setuid */

  char program[PATH_MAX]; /* name of the final launcher binary */
  char *arg_list[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}; /* max arguments to pass to the launcher */
  int   arg_index    = 0; /* index for next argument */

  const char* perrorMsg_1 = NULL; /* string part of the message sent to the stderr when the launcher cannot be started */
  char* perrorMsg = NULL; /* message sent to the stderr if the launcher cannot be started*/

  const char* fifoFileName = "/pmg.report"; /* name of the reposrt FIFO */
  char* fifoFileFullName = NULL; /* full path of the report FIFO */
  int fifoFlags = O_WRONLY | O_NONBLOCK; /* flags to open the report FIFO */
  int fifoFD = -1; /* the report FIFO file descriptor */

  const char* failedCommand = NULL; /* command to put into the report FIFO in case of launcher failure */
  const char* quitCommand = NULL; /* command to put into the report FIFO to quit the report thread in the server */
  char* failureMessage = NULL; /* global message to write into the report FIFO in case of failure */
  size_t failureMessageLen = 0; /* total length of the report FIFO message */

  int use_sudo = 0; /* are we supposed to use sudo ? */

  char uid_arg[20];  /* numerical user id for sudo branch */
  char ld_library_env[132767]; /* ld library path for sudo command */

  /* Check command line parameters */
  if(argc < 4) {
    fprintf(stderr, "PMGLauncher, usage: <LD path> <manifest dir> <user>\n"); 
    exit(EX_USAGE); 
  }

  {
    /* test if we should use sudo and not rely on suid bit */
    char *sudo_env = getenv("TDAQ_PMG_USE_SUDO");
    use_sudo = sudo_env && (strcmp(sudo_env, "1") == 0);
  }

  strcpy(program, getenv("TDAQ_INST_PATH"));
  strcat(program, "/");
  strcat(program, getenv("CMTCONFIG"));
  strcat(program, "/bin/pmglauncher");

  /* Get the effective uid */
  effective_uid = geteuid();
  final_uid = atoi(argv[3]);

  /* 
     Change the process owner if needed (and allowed)
     From setuid man pages:
     setuid sets the effective user ID of the current process. If the
     effective userid of the caller is root, the real and saved user ID's
     are also set.
  */

  if(final_uid != effective_uid) {

    if (use_sudo) { /* if enabled, highest priority, skip suid path */

      /* we have to pass LD_LIBRARY_PATH explicitly because despite
       * setting it later it will be lost since sudo is a suid program
       */
      snprintf(uid_arg, sizeof(uid_arg), "#%s", argv[3]);
      snprintf(ld_library_env, sizeof(ld_library_env), "LD_LIBRARY_PATH=%s", argv[1]);

      arg_list[arg_index++] = "/usr/bin/sudo";
      arg_list[arg_index++] = "-n";
      arg_list[arg_index++] = "-E";
      arg_list[arg_index++] = "-u";
      arg_list[arg_index++] = uid_arg;
      arg_list[arg_index++] = ld_library_env;
      arg_list[arg_index++] = "--";

    } else if (effective_uid == 0) { /* It means ROOT */

      printf("PMGLauncher, setting process groups for user %d\n", final_uid);
 
      errno = 0;
      pws = getpwuid(final_uid);

      if(pws != NULL) {
	/* Set process gid */
	errno = 0;
	sgResult = setgid(pws->pw_gid);
	if(sgResult == -1) {
	  perror("PMGLauncher, error in setgid");
	}

	/* Set supplementary groups */
	ng = 0;
	groups = NULL;

	errno = 0;
	if(getgrouplist(pws->pw_name, pws->pw_gid, NULL, &ng) < 0) {
	  groups = (gid_t*) malloc(ng*sizeof(gid_t));
	  errno = 0;
	  getgrouplist(pws->pw_name, pws->pw_gid, groups, &ng);
	}

	if(ng > 0 && groups != NULL) {
	  errno = 0;
	  sgsResult = setgroups(ng, groups);
	  if(sgsResult == -1) {
	    perror("PMGLauncher, error in setgroups");
	  }
	} else {
	  perror("PMGLauncher, error in getgrouplist");
	}

	free(groups);
      } else {
	perror("PMGLauncher, error in getpwuid");
      }

      printf("PMGLauncher, changing process user from %d to %d\n", effective_uid, final_uid);
      uidResult = setuid(final_uid);
      if(uidResult != 0) {
	perror("PMGLauncher, error in setuid");
      } 

    } else {
      fprintf(stderr, "PMGLauncher, cannot change process user from %d to %d. I am not ROOT!\n", effective_uid, final_uid);
    }
  }

  /* Set the LD_LIBRARY_PATH */
  setenv("LD_LIBRARY_PATH", argv[1], 1);

  /* Set KRB5CCNAME
   * This is needed so that the variable is
   * already set in pmglauncher process. This
   * is required by EOS which accesses the
   * kernel /proc filesystem to find the value.
   *
   * Changing the variable inside pmglauncher
   * has no effect and will lead to a permission
   * error when the final user executable is on
   * EOS and protected.
   */
  {
    char ccache[1024];
    snprintf(ccache, sizeof(ccache), "KEYRING:persistent:%d:pmg", getuid());
    setenv("KRB5CCNAME", ccache, 1);
  }

  /* Exec the pmglauncher */
  arg_list[arg_index++] = program;
  arg_list[arg_index++] = argv[2];
  arg_list[arg_index++] = NULL;

  errno = 0;
  execv(arg_list[0], arg_list);

  /* The exec function will return only in case of error */
  perrorMsg_1 = "PMGLauncher, failed executing the pmglauncher for the application ";
  perrorMsg = (char*) malloc((strlen(perrorMsg_1) + 1 + strlen(argv[2]) + 1)*sizeof(char));
  strcpy(perrorMsg, perrorMsg_1); /* iniutialize */
  strcat(perrorMsg, argv[2]); /* concatenate */
  perror(perrorMsg);
  free(perrorMsg);

  /* Try to send a message to the pmgserver using the report FIFO */

  /* The report FIFO */
  fifoFileFullName = (char*) malloc((strlen(fifoFileName) + 1 + strlen(argv[2]) + 1)*sizeof(char));
  strcpy(fifoFileFullName, argv[2]); /* initialize */
  strcat(fifoFileFullName, fifoFileName); /* concatenate */

  /* Open the FIFO in non-blocking mode and write the message */
  fifoFD = open(fifoFileFullName, fifoFlags);
  if(fifoFD != -1) {
    /* <--- WARNING! '8' indicates the pmgpub::::FAILED state (see the ProcessState enum in idl/pmgpub/pmgpub.idl). */ 
    /* <--- WARNING! This code is hard coded since it is not possible to include the ProcessManager/defs.h header */
    /* <--- WARNING! beacause of the setuid bit on this binary which does not honor the LD_LIBRARY_PATH. */
    failedCommand = "8\n";
    quitCommand = "q\n";
    failureMessageLen = strlen(failedCommand) + 1 + strlen(quitCommand) + 1;
    failureMessage = (char*) malloc(failureMessageLen*sizeof(char));
    strcpy(failureMessage, failedCommand); /* initialize */
    strcat(failureMessage, quitCommand); /* concatenate */
    
    write(fifoFD, failureMessage, strlen(failureMessage));                        
    free(failureMessage);
  }
  
  free(fifoFileFullName);
  
  exit(EXIT_FAILURE);
}
