/*
 *  LauncherMain.cxx
 *  PMG Launcher
 *
 *  Created by Matthias Wiesmann on 07.04.05.
 *  Copyright 2005 CERN. All rights reserved.
 *
 */

#include <cstdio>

#include "ProcessManager/Launcher.h"

#include <ipc/core.h>

#include <sysexits.h>
#include <ers/ers.h>
#include <signal.h>
#include <stdlib.h>

int main(int argc, char** argv) {

	::setenv("TDAQ_APPLICATION_NAME", "PMGLauncher", 1);

	try {
		IPCCore::init(argc, argv);
	}
	catch(...) {
	}

	if(argc < 2) {
		fprintf(stderr, "usage: %s <configuration directory>", argv[0]);
		exit(EX_USAGE);
	} //

	signal(SIGTERM, daq::pmg::signal_handler);
	signal(SIGINT, daq::pmg::signal_handler);
	signal(SIGPIPE, SIG_IGN);

	ERS_DEBUG(2,"Starting launcher for process "<<argv[1] << ".\n *\n *\n*");
	int exit_code = 0;
	try {
		System::Process::set_name(argv[0]);
		System::File directory(argv[1]);
		ERS_DEBUG(3,"Launching process defined in "<< argv[1]);
		daq::pmg::Launcher launcher(directory);
		launcher.launch();
	}
	catch(ers::Issue &issue) {
		ers::error(issue);
		exit_code = -1;
	} //
	ERS_DEBUG(2,"Exiting launcher for process "<<argv[1] << " with code --> " << exit_code << ".\n.\n.\n.");
	exit(exit_code);

} // main
