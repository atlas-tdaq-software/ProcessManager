/*
 *  Launcher.cxx
 *
 *  Created by Matthias Wiesmann on 16.02.05.
 *
 */

#include <sys/types.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sysexits.h>
#include <unistd.h>
#include <errno.h>

extern "C" {
#define private private_
#include <keyutils.h>
#undef private
}

#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include <functional>
#include <filesystem>

#include <ers/ers.h>
#include <system/exceptions.h>
#include <system/FIFOConnection.h>
#include <system/Executable.h>
#include <pmg/pmg_syncMacros.h>

#include "ProcessManager/Launcher.h"
#include "ProcessManager/private_defs.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/Utils.h"

using namespace daq;

const int VerifyCreationAlarmPeriod = 500000000; //nanoseconds

System::FIFOConnection * pmg::Launcher::m_report_fifo = 0;
System::FIFOConnection * pmg::Launcher::m_control_fifo = 0;
System::Process * pmg::Launcher::m_process_static = 0;

namespace {

    // Prepare to pass kerberos and AFS credentials to child
    // Execute eosfusebind if available
    void prepare_creds()
    {
        // our new KRB5CCNAME
        std::string ccname = std::string("KEYRING:persistent:") + std::to_string(getuid()) + ":pmg";
        setenv("KRB5CCNAME", ccname.c_str(), 1);

        // creates the proper structure if not there
        system("/usr/bin/klist -s");

        long persistent = keyctl_get_persistent(-1, KEY_SPEC_PROCESS_KEYRING);
        long krb = keyctl_search(persistent, "keyring", "_krb", 0);
        if(krb != -1) {
            if(keyctl_search(KEY_SPEC_SESSION_KEYRING, "keyring", "pmg", krb) != -1 && 
               access("/usr/bin/eosfusebind", X_OK) == 0) {

                // Optimization to avoid running eosfusebind
                uid_t uid = getuid();
                pid_t sid = getsid(0);
                long  sst = 0;

                std::ifstream proc{("/proc/" + std::to_string(sid) + "/stat").c_str()};

                if(proc.is_open()) {
                    std::string buffer;

                    for(size_t i = 0; i < 21; i++) {
                      proc >> buffer;
                    }

                    proc >> sst;
                    proc.close();

                    sst /= sysconf(_SC_CLK_TCK);

                    std::filesystem::path creds{"/var/run/eos/credentials/uid" + std::to_string(uid) +
                        "_sid" + std::to_string(sid) +
                        "_sst" + std::to_string(sst) +
                        ".krk5"};

                    if(!std::filesystem::is_symlink(creds) || ccname != std::filesystem::read_symlink(creds).string()) {
                      system("/usr/bin/eosfusebind krb5");
                    }
                }
            }
        }
        keyctl_unlink(persistent, KEY_SPEC_PROCESS_KEYRING);
    }
}

pmg::Launcher::Launcher(const System::File &dir) :
	m_control_thread(), m_sync_alarm(), m_cond_mutex(), m_cond_var(), m_manifest(dir.child(pmg::Manifest::FILENAME), true, true), m_mutex(),
	m_process(0), launcher_pid(::getpid()), m_process_handle(), m_process_joined(false)
{
}

pmg::Launcher::~Launcher() {
	if(m_process)
		delete m_process;
	m_process = 0;

	if(m_control_thread.joinable()) {
	    m_control_thread.join();
	}

	if(m_sync_alarm.joinable()) {
	    m_sync_alarm.join();
	}

	if(pmg::Launcher::m_report_fifo)
		delete pmg::Launcher::m_report_fifo;
	if(pmg::Launcher::m_control_fifo)
		delete pmg::Launcher::m_control_fifo;
	if(pmg::Launcher::m_process_static)
		delete pmg::Launcher::m_process_static;
} // ~Launcher

void pmg::Launcher::start_child() {

	ERS_ASSERT_MSG(m_manifest.check_sign(),"signature is not valid for "<< m_manifest);

	time(m_manifest.start_time_ptr()); // start time recorded.

	std::string eMsg;
	bool permission_error = false;

	try {
		if(m_manifest.working_directory().exists()) {
			int acc_result = access(m_manifest.working_directory().full_name().c_str(), R_OK | W_OK | X_OK);
			if(acc_result != 0) {
				int error = errno;
				eMsg = "Cannot access working directory " + m_manifest.working_directory().full_name();
				if(error != 0) {
					eMsg += ": " + daq::pmg::utils::errno2String(error);
				}
				permission_error = true;
			}
		} else {
			const System::File wd(m_manifest.working_directory());
			mode_t wdPerms(0777);
			std::string dirName(wd.parent().full_name()); // If we are here then the wd does not exist
			if(pmg::propagateGID(dirName)) {
				wdPerms = wdPerms | S_ISGID;
			}
			wd.make_path(wdPerms);
		}
	}
	catch(System::PosixIssue &e) {
		eMsg = "Cannot create the working directory " + m_manifest.working_directory().full_name() + ": " + e.message();
		if(e.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(e.get_error()) + ")";
		}
		permission_error = true;
	}

	if(permission_error == true) {
		const pmg::Handle h(m_process_handle);
		ers::error(pmg::Launch_Process_Failed(ERS_HERE, h.applicationName(), h.partitionName(), eMsg));
		m_manifest.process_state(PMGProcessState_FAILED);
		m_manifest.error_msg(eMsg.c_str());
		this->update_state();
		doCleanUp();
		exit(EXIT_FAILURE);
	}

	System::File::working_directory(m_manifest.working_directory()); // go to the working directory

	// Make sure the directory is created for the output and error files.
	if(m_manifest.output_file().full_name() != "/dev/null") {
        try {
            const System::File logDir(m_manifest.output_file().parent());
            mode_t ldPerms(0777);
            std::string dirName(logDir.full_name());
            if(pmg::propagateGID(dirName)) {
                ldPerms = ldPerms | S_ISGID;
            }
            logDir.make_path(ldPerms);
        }
        catch(System::PosixIssue &ex) {
            eMsg = "Directory " + m_manifest.output_file().parent().full_name() + " for log files cannot be created: "
                    + ex.message();
            if(ex.get_error() != 0) {
                eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
            }
            const pmg::Handle h(m_process_handle);
            ers::error(pmg::Launch_Process_Failed(ERS_HERE, h.applicationName(), h.partitionName(), eMsg, ex));
            m_manifest.process_state(PMGProcessState_FAILED);
            m_manifest.error_msg(eMsg.c_str());
            this->update_state();
            doCleanUp();
            exit(EXIT_FAILURE);
        }
	}

	// If sync file requested add it to the environment; try to remove it if it already existed
	System::Executable::env_collection envs = m_manifest.environnements();
	if(m_manifest.init_timeout() > 0) {
		System::File syncF(this->sync_file());
		if(syncF.exists()) {
			try {
				syncF.unlink();
			}
			catch(System::PosixIssue &e) {
				eMsg = "PMG SYNC file " + syncF.full_name() + " already exists and cannot be removed: " + e.message();
				if(e.get_error() != 0) {
					eMsg += " (" + daq::pmg::utils::errno2String(e.get_error()) + ")";
				}
				const pmg::Handle h(m_process_handle);
				ers::error(pmg::Launch_Process_Failed(ERS_HERE, h.applicationName(), h.partitionName(), eMsg, e));
				m_manifest.process_state(PMGProcessState_FAILED);
				m_manifest.error_msg(eMsg.c_str());
				this->update_state();
				doCleanUp();
				exit(EXIT_FAILURE);
			}
		} else {
			// ensure that the sync file can be crated
			try {
				syncF.parent().make_path(0777);
			}
			catch(System::PosixIssue &e) {
				eMsg = "PMG SYNC file " + syncF.full_name() + " is needed but cannot be created: " + e.message();
				if(e.get_error() != 0) {
					eMsg += " (" + daq::pmg::utils::errno2String(e.get_error()) + ")";
				}
				const pmg::Handle h(m_process_handle);
				ers::error(pmg::Launch_Process_Failed(ERS_HERE, h.applicationName(), h.partitionName(), eMsg, e));
				m_manifest.process_state(PMGProcessState_FAILED);
				m_manifest.error_msg(eMsg.c_str());
				this->update_state();
				doCleanUp();
				exit(EXIT_FAILURE);
			}
		}
	}

	ERS_DEBUG(1,"Starting process " << m_manifest.executable().to_string(m_manifest.parameters()));

	try {
                prepare_creds();
		const System::Process p = m_manifest.executable().pipe_out(m_manifest.parameters(),
		                                                           envs,
		                                                           m_manifest.input_file(),
		                                                           m_manifest.output_file(),
		                                                           m_manifest.error_file(),
		                                                           m_manifest.output_file_permission());
		m_manifest.process_id(p); // we record the process id
		m_process = new System::Process(p); // we build internal process pointer

		m_process_static = new System::Process(p);

		ERS_ASSERT(m_process); // paranoia check
	}
	catch(System::PosixIssue &Issue) {
		const pmg::Handle h(m_process_handle);
		ers::error(pmg::Launch_Process_Failed(ERS_HERE, h.applicationName(), h.partitionName(), Issue.message(), Issue));

		m_manifest.process_state(PMGProcessState_FAILED);
		m_manifest.exit_code(Issue.get_error());
		m_manifest.exit_signal(0);
		eMsg = Issue.message();
		if(Issue.get_error() != 0) {
			eMsg += " (" + daq::pmg::utils::errno2String(Issue.get_error()) + ")";
		}
		m_manifest.error_msg(eMsg.c_str());

		update_state();
		doCleanUp();
		exit(EXIT_FAILURE);
	}

} // start_child

void pmg::Launcher::wait_child() {

	ERS_PRECONDITION(m_process);

	ERS_DEBUG(2,"Start wait child loop");
	while(m_process->exists()) {
		try {
			unsigned int status = m_process->join(false);

			{
			    std::lock_guard<std::mutex> lk(m_cond_mutex);
			    m_process_joined = true;
			    m_cond_var.notify_all();
			}

			{
                std::lock_guard<std::mutex> lk(m_mutex);
                if(status != 0) {
                    unsigned int status_new = status + EX__BASE - 1;
                    m_manifest.error_msg((System::Process::exit_pretty(status_new)).c_str());
                }
                m_manifest.exit_code(status);
                m_manifest.exit_signal(0);
                if(m_manifest.process_state() != PMGProcessState_SYNCERROR) {
                    if(m_manifest.process_state() != PMGProcessState_RUNNING) {
                        m_manifest.process_state(PMGProcessState_RUNNING);
                        update_state();
                    }
                    m_manifest.process_state(PMGProcessState_EXITED);
                }
                update_state();
                ERS_DEBUG(2,"Out of wait child via exit!");
			}

			return;
		}
		catch(System::SignalIssue &issue) {
		    {
		        std::lock_guard<std::mutex> lk(m_cond_mutex);
		        m_process_joined = true;
		        m_cond_var.notify_all();
		    }

			{
			    std::lock_guard<std::mutex> lk(m_mutex);
                m_manifest.exit_code(issue.get_error());
                m_manifest.exit_signal(issue.get_signal());
                if(m_manifest.process_state() != PMGProcessState_SYNCERROR) {
                    if(m_manifest.process_state() != PMGProcessState_RUNNING) {
                        // This extra check is needed when a process is started with the
                        // sync procedure but it exits just after starting (i.e., bad command line options).
                        // In this case the client would receive only the EXITED callback,
                        // and the RUNNING callback will never be sent.
                        m_manifest.process_state(PMGProcessState_RUNNING);
                        update_state();
                    }
                    m_manifest.process_state(PMGProcessState_SIGNALED);
                }
                std::string gmsg = issue.message();
                gmsg += " - " + std::string(strsignal(issue.get_signal())) + " - ";
                if(issue.get_error() != 0) {
                    gmsg += " (" + daq::pmg::utils::errno2String(issue.get_error()) + ")";
                }
                m_manifest.error_msg(gmsg.c_str());
                update_state();
                ers::info(pmg::Exception(ERS_HERE, issue.message(), issue));
                ERS_DEBUG(2,"Out of wait child via signal " << issue.get_signal() << "!");
			}

			return;
		} // catch SignalIssue
		catch(System::TerminationIssue &issue) {
		    {
		        std::lock_guard<std::mutex> lk(m_cond_mutex);
		        m_process_joined = true;
		        m_cond_var.notify_all();
		    }

			{
			    std::lock_guard<std::mutex> lk(m_mutex);
                int status = issue.get_status();
                if(status != 0) {
                    unsigned int status_new = status + EX__BASE - 1;
                    m_manifest.error_msg((System::Process::exit_pretty(status_new)).c_str());
                }
                m_manifest.exit_code(status);
                m_manifest.exit_signal(0);
                if(m_manifest.process_state() != PMGProcessState_SYNCERROR) {
                    if(m_manifest.process_state() != PMGProcessState_RUNNING) {
                        m_manifest.process_state(PMGProcessState_RUNNING);
                        update_state();
                    }
                    m_manifest.process_state(PMGProcessState_EXITED);
                }
                update_state();
                ers::info(pmg::Exception(ERS_HERE, issue.message(), issue));
                ERS_DEBUG(2,"Out of wait child via exit!");
			}

			return;
		}
		catch(System::SystemCallIssue &issue) {
		    {
		        std::lock_guard<std::mutex> lk(m_cond_mutex);
		        m_process_joined = true;
		        m_cond_var.notify_all();
		    }

			{
			    std::lock_guard<std::mutex> lk(m_mutex);
                m_manifest.exit_code(issue.get_error());
                std::string gmsg = issue.message();
                if(issue.get_error() != 0) {
                    gmsg += " (" + daq::pmg::utils::errno2String(issue.get_error()) + ")";
                }
                m_manifest.error_msg(gmsg.c_str());
                m_manifest.exit_signal(0);
                if(m_manifest.process_state() != PMGProcessState_SYNCERROR) {
                    if(m_manifest.process_state() != PMGProcessState_RUNNING) {
                        m_manifest.process_state(PMGProcessState_RUNNING);
                        update_state();
                    }
                    m_manifest.process_state(PMGProcessState_EXITED);
                }
                update_state();
                ers::error(pmg::Exception(ERS_HERE, issue.message(), issue));
			}

			return;
		}
	} // while process exists

} // wait_child

void pmg::Launcher::end_child() {
	ERS_DEBUG(2,"Start end_child");
	update_data();
	ERS_DEBUG(2,"Stop end_child");
} // end_child

std::string pmg::Launcher::sync_file() {
	std::string syncFile;
	System::Executable::env_collection envs = m_manifest.environnements();
	std::map<std::string, std::string>::iterator iter = envs.find(PMG_SYNC_ENV_VAR_NAME);
	if(iter != envs.end()) {
		syncFile = iter->second;
	}
	return syncFile;
}

bool pmg::Launcher::continue_run() {
	if(0 == m_process)
		return false;
	if(!m_process->exists())
		return false;
	switch(m_manifest.process_state()) {
		case PMGProcessState_CREATED:
		case PMGProcessState_RUNNING:
			return true;
		default:
			return false;
	} // switch
} // continue_run

void pmg::Launcher::start_control() {
	m_control_thread = std::thread(std::bind(&pmg::Launcher::control_loop, this));
} // start_control

void pmg::Launcher::control_loop() {

	bool quit_requested = false;

	ERS_DEBUG(2,"Start control loop");

	while(!quit_requested) { // Exit only if wait_control() sends the quit command

		std::string Message = m_control_fifo->read();

		std::lock_guard<std::mutex> lk(m_mutex);

		std::string::size_type first = 0;
		std::string::size_type last = 0;
		std::string::size_type len = Message.length();

		while((first < len) && (last != std::string::npos)) { // Parse the Message string to look for multiple command in the FIFO
			last = Message.find("\n", first);
			if(last == std::string::npos) {
				continue;
			} else {
				std::string message = Message.substr(first, last - first);

				char command = message[0];
				ERS_DEBUG(3,"Received: "<< message);

				switch(command) {
					case pmg::UPDATE_CODE: // update
						update_data();
						break;
					case pmg::SIGNAL_CODE: // signal
						try {
							if(m_process != 0)
								m_process->signal(atoi(message.substr(1).c_str()));
						}
						catch(System::PosixIssue &ex) {
							std::string sigErr = ex.message();

							const int errorCode = ex.get_error();
							if(errorCode != 0) {
								sigErr += " (" + daq::pmg::utils::errno2String(errorCode) + ")";
							}

							const pmg::Handle h(m_process_handle);
							if(errorCode == 3) { // No such process
								ers::info(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                      message.substr(1),
								                                      h.applicationName(),
								                                      h.partitionName(),
								                                      errorCode,
								                                      sigErr,
								                                      ex));
							} else {
								ers::error(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                       message.substr(1),
								                                       h.applicationName(),
								                                       h.partitionName(),
								                                       errorCode,
								                                       sigErr,
								                                       ex));
							}
						}
						break;
					case pmg::KILL_SOFT_CODE: // signal
						try {
							if(m_process != 0) {
								ERS_DEBUG(3,"Sending SIGTERM to " << m_manifest.full_name() << " and waiting " << message.substr(1).c_str() << " seconds before sending SIGKILL");

								m_process->signal(SIGTERM);

								bool timeoutElapsed = false;
								{
								    std::unique_lock<std::mutex> lk(m_cond_mutex);
								    timeoutElapsed = !m_cond_var.wait_for(lk, std::chrono::seconds(atoi(message.substr(1).c_str())), [this]() { return (m_process_joined == true); });
								}

								if(timeoutElapsed == true) {
		                            try {
		                                ERS_DEBUG(3,"Timeout elapsed, sending SIGKILL to " << m_manifest.full_name());
		                                m_process->signal(SIGKILL);
		                            }
		                            catch(System::PosixIssue &ex) {
		                                std::string sigErr = ex.message();

		                                const int errorCode = ex.get_error();
		                                if(errorCode != 0) {
		                                    sigErr += " (" + daq::pmg::utils::errno2String(errorCode) + ")";
		                                }

		                                const pmg::Handle h(m_process_handle);
		                                if(errorCode == 3) { // No such process
		                                    ers::info(pmg::Launcher_Signal_Failed(ERS_HERE,
		                                                                          "SIGKILL",
		                                                                          h.applicationName(),
		                                                                          h.partitionName(),
		                                                                          errorCode,
		                                                                          sigErr,
		                                                                          ex));
		                                } else {
		                                    ers::error(pmg::Launcher_Signal_Failed(ERS_HERE,
		                                                                           "SIGKILL",
		                                                                           h.applicationName(),
		                                                                           h.partitionName(),
		                                                                           errorCode,
		                                                                           sigErr,
		                                                                           ex));
		                                }
		                            }
								}
							}
						}
						catch(System::PosixIssue &ex) {
							std::string sigErr = ex.message();

							const int errorCode = ex.get_error();
							if(errorCode != 0) {
								sigErr += " (" + daq::pmg::utils::errno2String(errorCode) + ")";
							}

							const pmg::Handle h(m_process_handle);
							if(errorCode == 3) { // No such process
								ers::info(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                      "SIGTERM",
								                                      h.applicationName(),
								                                      h.partitionName(),
								                                      errorCode,
								                                      sigErr,
								                                      ex));
							} else {
								ers::error(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                       "SIGTERM",
								                                       h.applicationName(),
								                                       h.partitionName(),
								                                       errorCode,
								                                       sigErr,
								                                       ex));
							}
						}

						break;
					case pmg::KILL_CODE: // kill
						try {
							if(m_process != 0)
								m_process->signal(SIGKILL);
						}
						catch(System::PosixIssue &ex) {
							std::string sigErr = ex.message();

							const int errorCode = ex.get_error();
							if(errorCode != 0) {
								sigErr += " (" + daq::pmg::utils::errno2String(errorCode) + ")";
							}

							const pmg::Handle h(m_process_handle);
							if(errorCode == 3) { // No such process
								ers::info(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                      "SIGKILL",
								                                      h.applicationName(),
								                                      h.partitionName(),
								                                      errorCode,
								                                      sigErr,
								                                      ex));
							} else {
								ers::error(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                       "SIGKILL",
								                                       h.applicationName(),
								                                       h.partitionName(),
								                                       errorCode,
								                                       sigErr,
								                                       ex));
							}

						}
						break;
					case pmg::QUIT_CODE: // quit
						quit_requested = true;
						break;
					case pmg::TERM_CODE: // terminate
						try {
							if(m_process != 0)
								m_process->terminate();
						}
						catch(System::PosixIssue &ex) {
							std::string sigErr = ex.message();

							const int errorCode = ex.get_error();
							if(errorCode != 0) {
								sigErr += " (" + daq::pmg::utils::errno2String(errorCode) + ")";
							}

							const pmg::Handle h(m_process_handle);
							if(errorCode == 3) { // No such process
								ers::info(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                      "SIGTERM",
								                                      h.applicationName(),
								                                      h.partitionName(),
								                                      ex.get_error(),
								                                      sigErr,
								                                      ex));
							} else {
								ers::error(pmg::Launcher_Signal_Failed(ERS_HERE,
								                                       "SIGTERM",
								                                       h.applicationName(),
								                                       h.partitionName(),
								                                       ex.get_error(),
								                                       sigErr,
								                                       ex));
							}
						}
						break;
					default: // not understood
						break;
				} // switch

				first = last + 1;
			}

		} // while on Message string

	} // while
	ERS_DEBUG(2,"Out of control loop!");
} // control_loop

void pmg::Launcher::wait_control() {

	ERS_DEBUG(2,"Start of wait control!");

	std::ostringstream message_stream;
	message_stream << pmg::QUIT_COMMAND;
	m_control_fifo->send(message_stream.str());

	if(m_control_thread.joinable()) {
	    m_control_thread.join();
	}

	ERS_DEBUG(2,"Out of wait control!");
}

void pmg::Launcher::update_data() {
	const int status = ::getrusage(RUSAGE_CHILDREN, m_manifest.resource_usage_ptr());
	if(status < 0)
		throw System::PosixIssue(ERS_HERE, "getrusage failed", errno);
} // update_data

void pmg::Launcher::update_state() {

	const PMGProcessState procState = m_manifest.process_state();

	pmg::Handle procHandle(m_process_handle);

	std::string stateMsg = "Process " + procHandle.applicationName() + " (started in partition "
	        + procHandle.partitionName() + " as " + m_manifest.executable().to_string(m_manifest.parameters())
	        + ") in status " + state_string(m_manifest.process_state()) + ". ";

	if(procState == PMGProcessState_FAILED || procState == PMGProcessState_SYNCERROR || procState
	        == PMGProcessState_EXITED || procState == PMGProcessState_SIGNALED) {
		time(m_manifest.stop_time_ptr());
		std::ostringstream manifestDump;
		manifestDump << m_manifest;
		stateMsg += "\nDumping the manifest:\n" + manifestDump.str();
	}

	ers::info(pmg::Exception(ERS_HERE, stateMsg));

	// We notify the Application

	std::ostringstream message_stream;
	message_stream << procState << "\n";

	try {
		m_report_fifo->send(message_stream.str());
	}
	catch(System::PosixIssue &ex) {
		ers::error(pmg::Launcher_Update_State_Error(ERS_HERE,
		                                            m_report_fifo->full_name(),
		                                            procHandle.applicationName(),
		                                            procHandle.partitionName(),
		                                            ex.get_error(),
		                                            ex.message(),
		                                            ex));
	}

} // update_state

void pmg::Launcher::doCleanUp() {

	// Write command into the report FIFO to stop the server report thread

	std::ostringstream message_stream;
	message_stream << pmg::QUIT_COMMAND;
	try {
		if(m_report_fifo) {
			m_report_fifo->send(message_stream.str());
		}
	}
	catch(System::PosixIssue& ex) {
		ers::warning(pmg::Exception(ERS_HERE, ex.message(), ex));
	}

	// Remove out and err files if empty and unmap the manifest

	try {
		if(m_manifest.is_mapped()) {
			try {
				const System::File& errFile = m_manifest.error_file();
				const System::File& outFile = m_manifest.output_file();
				if(errFile.size() == 0 && errFile.is_regular() == true) {
					ERS_DEBUG(3, "Removing empty file: " << errFile.full_name());
					errFile.unlink();
				}
				if(outFile.size() == 0 && outFile.is_regular() == true) {
					ERS_DEBUG(3, "Removing empty file: " << outFile.full_name());
					outFile.unlink();
				}
			}
			catch(System::PosixIssue& issue) {
			}
			m_manifest.unmap();
		}
	}
	catch(System::PosixIssue& e) {
		ERS_DEBUG(1, "Got exception " << e.what() << " while unmapping manifest.");
	}

	// Unlink the control FIFO

	try {
		if(m_control_fifo) {
			m_control_fifo->unlink();
		}
	}
	catch(System::PosixIssue &ex) {
		// Do nothing
	}

}

void pmg::Launcher::launch() {

	try {
		try {
			System::File mFile(m_manifest.full_name());
			System::Descriptor mDescr(&mFile, System::Descriptor::flags(true, false), 0666); // To be sure the manifest file exists...

			ERS_DEBUG(5,"loading manifest "<<m_manifest.c_full_name());
			m_manifest.map();

			try {
				(m_manifest.fd())->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
				std::string msg = ex.message() + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg, ex));
			}
		}
		catch(System::PosixIssue &ex) {
			ers::fatal(pmg::Manifest_Not_Mapped(ERS_HERE, m_manifest.full_name(), ex.message(), ex));
			exit(EXIT_FAILURE);
		}

		// Try to get the handle from the manifest
		// The Handle object is built just to check its validity
		// If the handle is not built correctly probably the
		// manifest is corrupted: no reason to continue!
		try {
			m_process_handle = m_manifest.handle();
			const pmg::Handle h(m_process_handle);
		}
		catch(pmg::Invalid_Handle& ex) {
			ers::fatal(ex);
			exit(EXIT_FAILURE);
		}

		m_manifest.error_msg(pmg::Manifest::DEFAULT_ERR_MSG); // empty error msg
		m_manifest.launcher_pid(*System::Process::instance()); // we set the launcher id

		// Get report FIFO from the manifest
		m_report_fifo = new System::FIFOConnection(m_manifest.report_file());

		// Open the report FIFO in write non-blocking mode

		try {
			System::Descriptor* r_fifoDescriptor = m_report_fifo->open_w(false);
			try {
				r_fifoDescriptor->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
				std::string msg = ex.message() + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg, ex));
			}
		}
		catch(System::OpenFileIssue &ex) {
			const pmg::Handle h(m_process_handle);
			ers::fatal(pmg::Launcher_FIFO_Error(ERS_HERE,
			                                    m_report_fifo->full_name(),
			                                    h.applicationName(),
			                                    h.partitionName(),
			                                    ex.get_error(),
			                                    ex.message(),
			                                    ex));
			exit(EXIT_FAILURE);
		}

		m_manifest.process_state(PMGProcessState_LAUNCHING); // process is now launching

		// Open the control fifo in read-write mode. This avoids blocking in opening the FIFO by the threads.

		try {
			m_control_fifo = new System::FIFOConnection(m_manifest.control_file());
			if(!m_control_fifo->exists()) {
				const mode_t perm = m_manifest.control_file_permission();
				m_control_fifo->make(perm);
			}

			System::Descriptor* c_fifoDescriptor = m_control_fifo->open_rw();

			try {
				c_fifoDescriptor->closeOnExec();
			}
			catch(System::SystemCallIssue &ex) { // Exception thrown by closeOnExec()
				std::string msg = ex.message() + ". File descriptor will stay open in the child process";
				ers::warning(pmg::Exception(ERS_HERE, msg, ex));
			}
		}
		catch(System::SystemCallIssue &ex) {
			const pmg::Handle h(m_process_handle);
			std::string eMsg = "Error while creating control fifo: " + ex.message();
			if(ex.get_error() != 0) {
				eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
			}
			m_manifest.process_state(PMGProcessState_FAILED);
			m_manifest.error_msg(eMsg.c_str());
			this->update_state();
			doCleanUp();
			ers::error(pmg::Launcher_FIFO_Error(ERS_HERE,
			                                    m_control_fifo->full_name(),
			                                    h.applicationName(),
			                                    h.partitionName(),
			                                    ex.get_error(),
			                                    eMsg,
			                                    ex));
			exit(EXIT_FAILURE);
		}
		catch(System::OpenFileIssue &ex) {
			const pmg::Handle h(m_process_handle);
			std::string eMsg = "Error while opening control fifo: " + ex.message();
			if(ex.get_error() != 0) {
				eMsg += " (" + daq::pmg::utils::errno2String(ex.get_error()) + ")";
			}
			m_manifest.process_state(PMGProcessState_FAILED);
			m_manifest.error_msg(eMsg.c_str());
			this->update_state();
			doCleanUp();
			ers::error(pmg::Launcher_FIFO_Error(ERS_HERE,
			                                    m_control_fifo->full_name(),
			                                    h.applicationName(),
			                                    h.partitionName(),
			                                    ex.get_error(),
			                                    eMsg,
			                                    ex));
			exit(EXIT_FAILURE);
		}

		start_child();

		if(m_process == 0) {
			doCleanUp();
			exit(EXIT_FAILURE);
		}

		start_control();

		std::thread autoKillThread;
		if(m_manifest.init_timeout() > 0) {
			m_manifest.process_state(PMGProcessState_CREATED);
			m_sync_alarm = std::thread(std::bind(&Launcher::sync_loop, this));
		} else {
		    {
		        std::lock_guard<std::mutex> lk(m_mutex);
		        m_manifest.process_state(PMGProcessState_RUNNING);
		        update_state();
		    }

		    // Here start the "auto kill check"
			const int autoKillTimeout = m_manifest.auto_kill_timeout();
			if(autoKillTimeout > 0) {
			    autoKillThread = std::thread(&Launcher::auto_kill_loop, this, std::chrono::steady_clock::now() + std::chrono::seconds(autoKillTimeout));
			}
		}

		wait_child(); // blocking
		end_child();
		wait_control(); //blocking again (on control thread)
		if(m_sync_alarm.joinable()) {
		    m_sync_alarm.join();
		}

		if(autoKillThread.joinable()) {
		    autoKillThread.join();
		}

		doCleanUp();
	}
	catch(System::PosixIssue &e) {
		std::string mmms = e.message();
		if(e.get_error() != 0) {
			mmms += " (" + daq::pmg::utils::errno2String(e.get_error()) + ")";
		}
		ers::error(pmg::Exception(ERS_HERE, mmms, e));
		m_manifest.error_msg(mmms.c_str());
		m_manifest.process_state(PMGProcessState_FAILED);
		update_state();
		doCleanUp();
	}
	catch(ers::Issue &e) {
		ers::error(pmg::Exception(ERS_HERE, e.message(), e));
		m_manifest.error_msg(e.what());
		m_manifest.process_state(PMGProcessState_FAILED);
		update_state();
		doCleanUp();
	}
	catch(std::exception &e) {
		ers::error(pmg::Exception(ERS_HERE, e.what(), e));
		m_manifest.error_msg(e.what());
		m_manifest.process_state(PMGProcessState_FAILED);
		update_state();
		doCleanUp();
	} // catch

} // launch

void pmg::Launcher::auto_kill_loop(const std::chrono::steady_clock::time_point& endTime) {
    bool done = false;
    while(done == false) {
        {
            std::lock_guard<std::mutex> lk(m_mutex);

            if((m_manifest.process_state() == PMGProcessState_RUNNING) &&
               (m_process->exists() == true))
            {
                if(std::chrono::steady_clock::now() > endTime) {
                    if(pmg::Launcher::m_control_fifo->exists()) {
                        std::ostringstream message_stream;
                        message_stream << pmg::KILL_SOFT_CODE << 5 << "\n";
                        pmg::Launcher::m_control_fifo->send(message_stream.str());
                    }

                    done = true;
                }
            } else {
                done = true;
            }
        }

        std::this_thread::sleep_for(std::chrono::nanoseconds(VerifyCreationAlarmPeriod));
    }
}

void pmg::Launcher::sync_loop() {
	System::File syncF(sync_file());

	struct timespec delay, res;
	delay.tv_sec = 0;
	delay.tv_nsec = VerifyCreationAlarmPeriod;
	const time_t start = *(m_manifest.start_time_ptr());

	ERS_DEBUG(2,"Start sync thread loop");

	bool done = false;
	while(done == false) {
	    {
	        std::lock_guard<std::mutex> lk(m_mutex);

            if((m_manifest.process_state() == PMGProcessState_CREATED) &&
               (m_process->exists() == true))
            {
                int elapsed_time = (int) difftime(time(NULL), start);
                if(elapsed_time > m_manifest.init_timeout()) {
                    // Report SYNC error, terminate process and terminate alarm
                    m_manifest.process_state(PMGProcessState_SYNCERROR);
                    if(pmg::Launcher::m_control_fifo->exists()) {
                        std::ostringstream message_stream;
                        message_stream << pmg::KILL_SOFT_CODE << 5 << "\n";
                        pmg::Launcher::m_control_fifo->send(message_stream.str());
                    }

                    done = true;
                } else {
                    if(syncF.exists()) {
                        ERS_DEBUG(2, "Sync file " << sync_file() << " exists.");
                        // confirm running
                        std::ostringstream message_stream;
                        m_manifest.process_state(PMGProcessState_RUNNING);
                        update_state();

                        done = true;
                    }
                }
            } else {
                done = true;
            }
	    }

	    // sleep a bit and continue checking
	    nanosleep(&delay, &res);
	}

	// Here start the "auto kill check"
	const int autoKillTimeout = m_manifest.auto_kill_timeout();
	if(autoKillTimeout > 0) {
	    auto_kill_loop(std::chrono::steady_clock::now() + std::chrono::seconds(autoKillTimeout));
	}

	// Small clean-up
	try {
	    if(syncF.exists()) {
	        syncF.unlink();
	        }
	}
	catch(System::PosixIssue &e) {
	    std::string em = "Failed to remove sync file " + syncF.full_name() + ". : " + e.message();
	    if(e.get_error() != 0) {
	        em += " (" + daq::pmg::utils::errno2String(e.get_error()) + ")";
	    }
	    ers::warning(pmg::Exception(ERS_HERE, em, e));
	}

	ERS_DEBUG(2, "Out of SyncThread");
}

void pmg::signal_handler(int sig) {
    // This is to remove compiler's warning about not used variable in opt mode
    (void) sig;

	ERS_DEBUG(3,"Caught signal " << sig << ". Kill the process and exit...");

	// Remove FIFOs

	try {
		if(pmg::Launcher::m_report_fifo) {
			std::ostringstream message_stream;
			message_stream << pmg::QUIT_COMMAND;
			pmg::Launcher::m_report_fifo->send(message_stream.str());
		}
		if(pmg::Launcher::m_control_fifo)
			pmg::Launcher::m_control_fifo->unlink();
	}
	catch(System::PosixIssue &ex) {
		ERS_DEBUG(3,ex.what());
	}
	catch(std::exception &ex) {
		ERS_DEBUG(3,ex.what());
	}

	// Kill child process

	try {
		if(pmg::Launcher::m_process_static != 0)
			pmg::Launcher::m_process_static->signal(SIGKILL);
	}
	catch(System::PosixIssue &ex) {
		ERS_DEBUG(3,ex.what());
	}
	catch(std::exception &ex) {
		ERS_DEBUG(3,ex.what());
	}

	exit(EXIT_SUCCESS);

}

bool pmg::propagateGID(std::string& startDir) {

	bool foundGID(false);

	size_t lastSlash = startDir.find_last_of('/');
	while(lastSlash != std::string::npos) {
		{
			// Do your check
			ERS_DEBUG(5, "Checking directory tree: " << startDir);

			struct stat dirInfo;
			int result = ::stat(startDir.c_str(), &dirInfo);
			if(result == 0) {
				mode_t dirPerms = dirInfo.st_mode;
				if((dirPerms & S_IFDIR)) { // Is it a dir?
					if(dirPerms & S_ISGID) { // Has the GID set?
						foundGID = true;
					}
					break; // Check only the last existing dir
				}
			}
		} // scope
		startDir.erase(lastSlash); // The first dir is the wd (which has been checked to not exist)
		lastSlash = startDir.find_last_of('/');
	} // while()

	return foundGID;
}
