/*
 *  ProcessData.cxx
 *
 *  Created by Matthias Wiesmann on 15.02.05.
 *
 */

#include "ProcessManager/ProcessData.h"

const char* daq::pmg::state_string(PMGProcessState state) {
    return p_state_strings[state] ;
} // state_string

