#include <stdlib.h>
#include <syslog.h>

#include <sstream>

#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <ers/Severity.h>

#include "ProcessManager/SyslogStream.h"

ERS_REGISTER_OUTPUT_STREAM(daq::pmg::SyslogStream, "syslog", syslogLocalFacility)
  
using namespace daq;
 
boost::recursive_mutex pmg::SyslogStream::mutex_;
unsigned int pmg::SyslogStream::msgID = 0;

pmg::SyslogStream::SyslogStream(const std::string& syslogLocalFacility) : maxLen_(512) {

  // Set the syslog local facility to use (LOG_LOCAL0 through LOG_LOCAL7 reserved for local use)
  if(syslogLocalFacility.empty()) {
    localFacility_ = LOG_LOCAL5;
  } else {
    switch(atoi(syslogLocalFacility.c_str())) {
    case 0:
      localFacility_ = LOG_LOCAL0;
      break;
    case 1:
      localFacility_ = LOG_LOCAL1;
      break;
    case 2:
      localFacility_ = LOG_LOCAL2;
      break;
    case 3:
      localFacility_ = LOG_LOCAL3;
      break;
    case 4:
      localFacility_ = LOG_LOCAL4;
      break;
    case 5:
      localFacility_ = LOG_LOCAL5;
      break;
    case 6:
      localFacility_ = LOG_LOCAL6;
      break;
    case 7:
      localFacility_ = LOG_LOCAL7;
      break;    
    default:
      localFacility_ = LOG_LOCAL5;
    }
  }

  // Open connection to the syslogd daemon
  ::openlog(NULL, LOG_PID, localFacility_);

}

void pmg::SyslogStream::write(const ers::Issue& issue) {
  
  // Get the issue severity
  ers::Severity issueSeverity = issue.severity();  
  const std::string sevStr = ers::to_string(issueSeverity); 
  
  // Translate issue severity to syslog severity
  int logLevel;
  switch((ers::severity)issueSeverity) {
  case ers::Debug:
    logLevel = LOG_DEBUG;
    break;
  case ers::Information:
    logLevel = LOG_NOTICE;
    break;
  case ers::Warning:
    logLevel = LOG_WARNING;
    break;
  case ers::Error:
    logLevel = LOG_ERR;
    break;
  case ers::Fatal:
    logLevel = LOG_ALERT;
    break;
  default:
    logLevel = LOG_INFO;
  }
  
  // Format the message removing consecutive blank spaces
  std::string strMsg(issue.message());
  trimStr(strMsg);

  // Syslog truncates log strings: divide the messgae strings in substrings with maxLen_ size
  std::vector<std::string> strVect;
  for(size_t s = 0; s < strMsg.size(); s += maxLen_) {
    strVect.push_back(strMsg.substr(s, maxLen_));
  }

  // Lock the mutex (to avoid mixing of multiple messages) and send string to syslog
  {
    boost::recursive_mutex::scoped_lock scoped_lock(mutex_);
    
    // Increase the message ID
    ++msgID;    
    std::stringstream msgId;
    msgId << "[MSG ID = " << msgID << "]";

    for(std::vector<std::string>::const_iterator it = strVect.begin(); it != strVect.end(); ++it) {
      
      // Send message to syslog
      ::syslog(logLevel|localFacility_, "%s %s%s%s %s", msgId.str().c_str(), "[", sevStr.c_str(), "]", it->c_str());
      
    }
  }

}

void pmg::SyslogStream::trimStr(std::string& str) {

  // Remove consecutive blank spaces

  size_t nextChar = 0;
  size_t emptyChar = 0;
  size_t len = str.size();

  while(nextChar < len) {

    len = str.size();
    emptyChar = str.find(" ", emptyChar);
    if(emptyChar == std::string::npos) {
      break;
    }
    nextChar = len;
    for(size_t i = emptyChar; i < len; ++i) {
      if(str[i] != ' ') {
	nextChar = i;
	break;
      }
    }
    if( (nextChar - emptyChar - 1) > 0 ) {
      str.erase(emptyChar, nextChar - emptyChar - 1);
    } else {
      ++emptyChar;
    }
    
  }

}
