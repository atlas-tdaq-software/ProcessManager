#include <sstream>
#include <fstream>

#include "ProcessManager/Exceptions.h"
#include "ProcessManager/ProcInterface.h"
#include "ProcessManager/private_defs.h"

namespace daq {
namespace pmg {

ProcInterface::ProcInterface(const unsigned int pid) :
	m_pid(pid), m_statFileName(setStatFile()), m_statmFileName(setStatmFile())
{
	readStat();
	readStatm();
}

ProcInterface::~ProcInterface() {
}

void ProcInterface::readStat() {
	std::ifstream fileStream(m_statFileName.c_str(), std::ios::in);
	if(fileStream.good()) {
		fileStream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
		try {
			fileStream >> m_total_stat.stat_info.pid >> m_total_stat.stat_info.comm
			        >> m_total_stat.stat_info.state >> m_total_stat.stat_info.ppid
			        >> m_total_stat.stat_info.pgrp >> m_total_stat.stat_info.session
			        >> m_total_stat.stat_info.tty_nr >> m_total_stat.stat_info.tpgid
			        >> m_total_stat.stat_info.flags >> m_total_stat.stat_info.minflt
			        >> m_total_stat.stat_info.cminflt >> m_total_stat.stat_info.majflt
			        >> m_total_stat.stat_info.cmajflt >> m_total_stat.stat_info.utime
			        >> m_total_stat.stat_info.stime >> m_total_stat.stat_info.cutime
			        >> m_total_stat.stat_info.cstime >> m_total_stat.stat_info.priority
			        >> m_total_stat.stat_info.nice >> m_total_stat.stat_info.num_threads
			        >> m_total_stat.stat_info.itrealvalue >> m_total_stat.stat_info.starttime
			        >> m_total_stat.stat_info.vsize >> m_total_stat.stat_info.rss
			        >> m_total_stat.stat_info.rsslim >> m_total_stat.stat_info.startcode
			        >> m_total_stat.stat_info.endcode >> m_total_stat.stat_info.startstack
			        >> m_total_stat.stat_info.kstkesp >> m_total_stat.stat_info.kstkeip
			        >> m_total_stat.stat_info.signal >> m_total_stat.stat_info.blocked
			        >> m_total_stat.stat_info.sigignore >> m_total_stat.stat_info.sigcatch
			        >> m_total_stat.stat_info.wchan >> m_total_stat.stat_info.nswap
			        >> m_total_stat.stat_info.cnswap >> m_total_stat.stat_info.exit_signal
			        >> m_total_stat.stat_info.processor >> m_total_stat.stat_info.rt_priority
			        >> m_total_stat.stat_info.policy;
		}
		catch(std::ios_base::failure& ex) {
			std::string exString = "Error while reading " + m_statFileName
			        + " file. The file format could be changed.";
			throw(pmg::Exception(ERS_HERE, exString));
		}
		//The next check is not needed: it seems that new entries are always added at the end of the file
		//fileStream.exceptions(std::ios_base::goodbit);
		//std::string scratch;
		//fileStream >> scratch;
		//if(!fileStream.eof()) {
		//	std::string exString = "Bad format of " + m_statFileName + " file";
		//	throw(pmg::Exception(ERS_HERE, exString));
		//}
	} else {
		std::string exString = "Unable to open " + m_statFileName + " file";
		throw(pmg::Exception(ERS_HERE, exString));
	}
}

void ProcInterface::readStatm() {
	std::ifstream fileStream(m_statmFileName.c_str(), std::ios::in);
	if(fileStream.good()) {
		fileStream.exceptions(std::ios_base::failbit | std::ios_base::badbit);
		try {
			fileStream >> m_total_stat.statm_info.size >> m_total_stat.statm_info.resident
			        >> m_total_stat.statm_info.share >> m_total_stat.statm_info.text
			        >> m_total_stat.statm_info.lib >> m_total_stat.statm_info.data
			        >> m_total_stat.statm_info.dt;
		}
		catch(std::ios_base::failure& ex) {
			std::string exString = "Error while reading " + m_statmFileName
			        + " file. The file format could be changed.";
			throw(pmg::Exception(ERS_HERE, exString));
		}
		//The next check is not needed: it seems that new entries are always added at the end of the file
		//fileStream.exceptions(std::ios_base::goodbit);
		//std::string scratch;
		//fileStream >> scratch;
		//if(!fileStream.eof()) {
		//	std::string exString = "Bad format of " + m_statmFileName + " file";
		//	throw(pmg::Exception(ERS_HERE, exString));
		//}
	} else {
		std::string exString = "Unable to open " + m_statmFileName + " file";
		throw(pmg::Exception(ERS_HERE, exString));
	}
}

std::string ProcInterface::setStatFile() {
	std::ostringstream fileName;
	fileName << PROC_DIR << "/" << m_pid << "/" << STAT_FILE;
	return fileName.str();
}

std::string ProcInterface::setStatmFile() {
	std::ostringstream fileName;
	fileName << PROC_DIR << "/" << m_pid << "/" << STATM_FILE;
	return fileName.str();
}

const proc_usage& ProcInterface::procInfo() const {
	return m_total_stat;
}

}
}
