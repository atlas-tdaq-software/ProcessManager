/*
 *    ClientList.cxx
 *
 *    Created by Matthias Wiesmann on 12.04.05.
 *
 */

#include <ers/ers.h>
#include <ipc/core.h>
#include <sstream>
#include <filesystem>

#include "ProcessManager/ClientList.h"
#include "ProcessManager/Exceptions.h"

using namespace daq ;

pmg::ClientList::ClientList() {}

pmg::ClientList::~ClientList() {}

bool pmg::ClientList::check(const pmgpriv::CLIENT_var& client_ref) const {
  
  // Check if 'client_ref' is already present in the client list

  std::list<pmgpriv::CLIENT_var>::const_iterator iter;
  for (iter = m_clientList.begin(); iter != m_clientList.end(); iter++) {
    pmgpriv::CLIENT_var client = (*iter);
    if(client->_is_equivalent(client_ref)) return true;
  } 
  return false;
}

void pmg::ClientList::add(const pmgpriv::CLIENT_var& client_ref) {
    m_clientList.push_back(client_ref);
} // add

    
void pmg::ClientList::remove(const pmgpriv::CLIENT_var& client_ref) {
    m_clientList.remove(client_ref);
} // remove


void pmg::ClientList::notify_update(const pmg::Handle &handle,
                                    const Manifest* manifest,
                                    const PMGProcessState state) const {
    ERS_PRECONDITION(manifest);
    ERS_DEBUG(3,"application " << handle.toString() << " has been updated");
    ERS_DEBUG(5,"Manifest:\n"  << *manifest);

    // Fill the ProcessStatusInfo structure
    // Note: environment and start args filed are not filled 
    // because that action may cause performance degradation.
    // That info is anyway put in the structure when the process
    // is started (see Proxy::info() and ProcessDescriptionImpl::start())
    pmgpub::ProcessStatusInfo info;
    
    // ProcessState state
    info.state = state;

    // string failure_str
    info.failure_str = CORBA::string_dup(state_string(state));
    info.failure_str_hr = CORBA::string_dup(manifest->error_msg());

    // start/stop time
    info.start_time = *(manifest->start_time_ptr());
    info.stop_time = *(manifest->stop_time_ptr());

    // ProcessStartInfo start_info
    info.start_info.client_host = CORBA::string_dup(manifest->requesting_host().c_str());
    info.start_info.host = CORBA::string_dup(handle.server().c_str());
    info.start_info.app_name = CORBA::string_dup(handle.applicationName().c_str());
    info.start_info.partition = CORBA::string_dup(handle.partitionName().c_str());
    info.start_info.executable = CORBA::string_dup(manifest->executable().full_name().c_str());
    info.start_info.wd = CORBA::string_dup(manifest->working_directory().c_full_name());
    info.start_info.rm_token = manifest->rm_token();

    // StreamInfo streams
    std::string outFile = manifest->output_file().full_name();
    std::string errFile = manifest->error_file().full_name();
    info.start_info.streams.log_path = CORBA::string_dup(manifest->output_file().parent_name().c_str());
    info.start_info.streams.out_path = CORBA::string_dup(outFile.c_str());
    info.start_info.streams.err_path = CORBA::string_dup(errFile.c_str());
    info.start_info.streams.in_path = CORBA::string_dup(manifest->input_file().c_full_name());
    info.start_info.streams.perms = manifest->output_file_permission();

    if(manifest->output_file().full_name() != "/dev/null") {
      info.start_info.streams.out_is_null = false;

      try {
          info.out_is_available = !std::filesystem::is_empty(outFile);
      }
      catch(std::filesystem::filesystem_error& ex) {
          ERS_DEBUG(1, "Failed to get the status of out log file for process \"" << handle.toString() << "\" (" << outFile << "): " << ex.what());
          info.out_is_available = false;
      }

      try {
          info.err_is_available = !std::filesystem::is_empty(errFile);
      }
      catch(std::filesystem::filesystem_error& ex) {
          ERS_DEBUG(1, "Failed to get the status of err log file for process \"" << handle.toString() << "\" (" << errFile << "): " << ex.what());
          info.err_is_available = false;
      }
    } else {
      info.start_info.streams.out_is_null = true;
      info.out_is_available = false;
      info.err_is_available = false;
    }

    const std::vector<std::string>& param_collection = manifest->parameters();
    info.start_info.start_args.length(param_collection.size());
    int pos = 0;
    std::vector<std::string>::const_iterator itt;
    for(itt = param_collection.begin(); itt != param_collection.end(); ++itt) {
        info.start_info.start_args[pos] = CORBA::string_dup(itt->c_str());
        ++pos;
    }
    
    const std::map<std::string, std::string>& envMap = manifest->environnements();
    info.start_info.envs.length(envMap.size());
    pos = 0;
    std::map<std::string, std::string>::const_iterator envIt;
    for(envIt = envMap.begin(); envIt != envMap.end(); ++envIt) {
        info.start_info.envs[pos].name = CORBA::string_dup((envIt->first).c_str());
        info.start_info.envs[pos].value = CORBA::string_dup((envIt->second).c_str());
        ++pos;
    }
    
    info.start_info.init_time_out = manifest->init_timeout();
    info.start_info.auto_kill_time_out = manifest->auto_kill_timeout();
    
    // UserInfo user
    info.start_info.user.name = CORBA::string_dup(manifest->user().name_safe().c_str());
    
    // ResourceInfo resource_usage
    info.resource_usage.total_user_time = ((manifest->resource_usage_ptr())->ru_utime).tv_sec;
    info.resource_usage.total_system_time = ((manifest->resource_usage_ptr())->ru_stime).tv_sec;
    info.resource_usage.memory_usage = (manifest->resource_usage_ptr())->ru_maxrss;
    
    // ProcessInfo info
    info.info.process_id = manifest->process_id().process_id();
    info.info.signal_value = manifest->exit_signal();
    info.info.exit_value = manifest->exit_code();
    
    // Iterate over all clients which want callbacks
    std::list<pmgpriv::CLIENT_var>::const_iterator iter;
    ERS_DEBUG(5,"Client List size is "<< m_clientList.size());
    for (iter = m_clientList.begin(); iter != m_clientList.end(); ++iter) {
        pmgpriv::CLIENT_var client = (*iter);

        // Call the client callback
        try {
            ERS_DEBUG(5,"Calling client callback");
            client->callback(handle.toString().c_str(), info);
        }
        catch (CORBA::Exception& ex) {
            ers::debug(daq::pmg::CannotNotifyClient(ERS_HERE,
                                                    IPCCore::objectToString(client, IPCCore::Corbaloc),
                                                    handle.toString(),
                                                    std::string("got CORBA exception ") + ex._name()),
                      0);
        }
    }

    ERS_DEBUG(5,"At end of ClientList loop");
} // notify_update

