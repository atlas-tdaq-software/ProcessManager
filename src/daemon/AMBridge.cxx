#include <AccessManager/xacml/impl/PMGResource.h>
#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/client/ServerInterrogator.h>
#include <AccessManager/util/ErsIssues.h>

#include "ProcessManager/AMBridge.h"
#include "ProcessManager/private_defs.h"
#include "ProcessManager/Exceptions.h"

using namespace daq;

pmg::AMBridge::AMBridge() :
	pResource(new am::PMGResource()), pInterrogator(new am::ServerInterrogator()), pRequestor()
{
}

pmg::AMBridge::AMBridge(const std::string& processBinaryPath,
                        const std::string& processHostName,
                        const std::string& processArguments,
                        const std::string& partitionName,
                        const bool processOwnedByRequester) :
	pResource(new am::PMGResource(processBinaryPath,
	                              processHostName,
	                              processArguments,
	                              processOwnedByRequester,
	                              partitionName)),
	        pInterrogator(new am::ServerInterrogator()), pRequestor()
{
}

pmg::AMBridge::AMBridge(const std::string& requestUserName,
                        const std::string& requestHostName,
                        const std::string& processBinaryPath,
                        const std::string& processHostName,
                        const std::string& processArguments,
                        const std::string& partitionName,
                        const bool processOwnedByRequester) :
	pResource(new am::PMGResource(processBinaryPath,
	                              processHostName,
	                              processArguments,
	                              processOwnedByRequester,
	                              partitionName)),
	        pInterrogator(new am::ServerInterrogator()),
	        pRequestor(new am::RequestorInfo(requestUserName, requestHostName))
{
}

pmg::AMBridge::~AMBridge() {
}

bool pmg::AMBridge::allowStart() {
	bool allowed = false;

	pResource->setStartAction();

	unsigned int retry = 0;

	while(retry < AM_MAX_RETRY) {
		try {
			++retry;

			if(pRequestor.get() != 0) {
				allowed = pInterrogator->isAuthorizationGranted(*pResource, *pRequestor);
			} else {
				allowed = pInterrogator->isAuthorizationGranted(*pResource);
			}

			break;
		}
		catch(...) {
			if(retry == AM_MAX_RETRY) {
				throw;
			} else {
				try {
					throw;
				}
				catch(am::Exception& ex) {
					ers::warning(daq::pmg::Exception(ERS_HERE,
					                                 std::string("Exception while calling the AccessManager: "
					                                		 	 + ex.message()
					                                		 	 + ". Trying again..."),
					                                		 	 ex));
				}
				catch(...) {
					ers::warning(daq::pmg::Exception(ERS_HERE, std::string("Unexpected exception while calling the AccessManager, trying again...")));
				}
			}
		}

		usleep(100000);
	}

	return allowed;
}

bool pmg::AMBridge::allowSignal(const int /** signal **/) {
	bool allowed = false;

	pResource->setTerminateAction();

	unsigned int retry = 0;

	while(retry < AM_MAX_RETRY) {
		try {
			++retry;

			if(pRequestor.get() != 0) {
				allowed = pInterrogator->isAuthorizationGranted(*pResource, *pRequestor);
			} else {
				allowed = pInterrogator->isAuthorizationGranted(*pResource);
			}

			break;
		}
		catch(...) {
			if(retry == AM_MAX_RETRY) {
				throw;
			} else {
				try {
					throw;
				}
				catch(am::Exception& ex) {
					ers::warning(daq::pmg::Exception(ERS_HERE,
					                                 std::string("Exception while calling the AccessManager: "
					                                		 	 + ex.message()
					                                		 	 + ". Trying again..."),
					                                		 	 ex));
				}
				catch(...) {
					ers::warning(daq::pmg::Exception(ERS_HERE, std::string("Unexpected exception while calling the AccessManager, trying again...")));
				}
			}
		}

		usleep(100000);
	}

	return allowed;
}

