/*
 *  ApplicationList.cxx
 *
 *  Created by Matthias Wiesmann on 07.04.05.
 *
 */

#include <limits>

#include <ers/ers.h>
#include <system/exceptions.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#include "ProcessManager/Application.h"
#include "ProcessManager/Partition.h"
#include "ProcessManager/ApplicationList.h"
#include "ProcessManager/Daemon.h"
#include "ProcessManager/Exceptions.h"
#include "ProcessManager/private_defs.h"

using boost::posix_time::ptime;
using boost::posix_time::from_time_t;
using boost::posix_time::second_clock;
using boost::posix_time::time_duration;
using boost::posix_time::seconds;
using boost::posix_time::to_simple_string;
using boost::date_time::c_local_adjustor;

namespace daq {
namespace pmg {

void ApplicationList::removeAndDelete(pmg_id_t appId) {

  boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

  // Remove and delete the requested Application object

  applist_t::iterator pos = m_application_list.find(appId);
  if(pos != m_application_list.end()) {
    delete(pos->second);
    pos->second = 0;
    m_application_list.erase(pos);
  }

  // Now scan the list and look for invalid Application objects
  
  for(applist_t::iterator invAppIt = m_application_list.begin(); invAppIt != m_application_list.end(); ) {
    if(invAppIt->second->invalid()) {
      delete(invAppIt->second);
      invAppIt->second = 0;
      m_application_list.erase(invAppIt++);
    } else {
      ++invAppIt;
    }
  }

}

System::File ApplicationList::build_directory(const std::string &name, const Partition *parent) {
    ERS_PRECONDITION(parent);
    return parent->directory().child(name) ; 
} // build_directory


ApplicationList::ApplicationList(const std::string &name, Partition *parent, pmg_id_t identity) : m_directory(build_directory(name,parent)) {

    m_name = name ; 
    m_last_id = identity; 
    m_parent = parent ;
    
} // ApplicationList

ApplicationList::~ApplicationList() {

  ERS_DEBUG(1, "Removing ApplicationList " << m_name);

  for(applist_t::iterator invAppIt = m_application_list.begin(); invAppIt != m_application_list.end(); ) {
    ERS_DEBUG(1, "Deleting Application object " << (invAppIt->second)->toString() << " whitin the ApplicationList " << m_name);
    delete(invAppIt->second);
    invAppIt->second = 0;
    m_application_list.erase(invAppIt++);
  }

} // ~ApplicationList

Application* ApplicationList::application(pmg_id_t identity) const {

  boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);
  
  Application* app_ptr = 0;

  applist_t::const_iterator pos = m_application_list.find(identity);
  if (pos!=m_application_list.end()) {
    app_ptr = pos->second; 
  } // if

  return app_ptr; 

} // application

application_collection_t &ApplicationList::all_applications(application_collection_t & collection) const {
 
    boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

    applist_t::const_iterator pos; 

    for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {
        Application *app = pos->second ; 
        collection.push_back(app);
    } // for
    
    return collection ; 
    
} // all_applications


Application *ApplicationList::application_factory(pmg_id_t identity) {

  boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

  Application* app_ptr = 0;

  if(identity != 0) {

    if(identity > m_last_id) { // Needed when creating the Application to reconnect to already running launchers
      m_last_id = identity;    // m_last_id must be the greatest one.
    }

    app_ptr = application(identity);
 
    if(app_ptr == 0) {
      app_ptr = new Application(identity,this,true,true);
      m_application_list[identity] = app_ptr;
    }

  } else {

    pmg_id_t n = next_identity();
    app_ptr = new Application(n,this);
    
    m_application_list[n] = app_ptr;

    ERS_DEBUG(3, "Application list size is " << m_application_list.size());

  }

  return app_ptr;
  
} // application_factory
  
const std::string &ApplicationList::name() const {
    return m_name ; 
} // name

const System::File &ApplicationList::directory() const {
    return m_directory ; 
} // directory

const Partition* ApplicationList::parent() const {
    return m_parent ; 
} // parent

unsigned int ApplicationList::active_processes() const {

    boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);
 
    unsigned int count = 0; 
    applist_t::const_iterator pos; 

    for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {

      Application *app = pos->second;
      bool flagInvalid = false;

      if(app && !(app->invalid())) {

	// Increase the valid application counter.

	ERS_DEBUG(4, " " << app->toString() << " is active"); 
	++count;
 
	// Check if the Application is in the NOTAV state for more than APP_NOTAV_STATE_TIMEOUT seconds.

	{
	  boost::recursive_mutex::scoped_lock app_lock(app->mutex());
	  const Manifest* const appManifest = app->manifest();
	  if(appManifest != 0) {
	    PMGProcessState state = appManifest->process_state();
	    if(state == PMGProcessState_NOTAV) {
	      ptime startTime(c_local_adjustor<ptime>::utc_to_local(from_time_t(*appManifest->start_time_ptr())));
	      ptime now(second_clock::local_time());
	      time_duration timeout = seconds(APP_NOTAV_STATE_TIMEOUT);
	      time_duration timeDiff = now - startTime;
	      if(timeDiff > timeout) {
		ERS_DEBUG(4, app->toString() << " in NOTAV state for more than " << APP_NOTAV_STATE_TIMEOUT << " seconds. Start time: " 
			  << to_simple_string(startTime) << " Now: " << to_simple_string(now) << " Difference: " << to_simple_string(timeDiff));
		flagInvalid = true;
		try {
		  // Unmap and unlink the manifest and the directory containing it.
		  if(app->manifest()->is_loaded() && app->manifest()->is_mapped()) {
		    std::string dir = app->manifest()->parent_name();
		    app->manifest()->unlink();
		    app->manifest()->unmap();
		    System::File dir_to_remove(dir);
		    dir_to_remove.remove();
		  }
		}
		catch(...) {
		}
	      }
	    }
	  }
	}
	
	// Flag the Application as invalid if needed.

	if(flagInvalid == true) {
	  app->invalid(true);
	}

      } // if(app && !(app->invalid()))

    } // for
    
    return count ; 

} // running_nbr


void ApplicationList::stop(const std::string& userName, const std::string& hostName) const {

    boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

    applist_t::const_iterator pos; 
    unsigned int failures = 0;
    std::string handles;
    
    for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {
      const Application *app = pos->second ; 
      if(app && !(app->invalid())) {
	std::string applicationHandle =  app->toHandle()->toString();
	try {
	  app->stop(userName, hostName);
	}
	catch(pmg::Exception &ex) {
	  ERS_DEBUG(1,ex);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
	catch(...) {
	  ERS_DEBUG(1, "Unknown exception while sending signal to " << applicationHandle);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
      } 
    } // for
    
    if(failures > 0) {
      throw pmg::ApplicationList_Signal_Exception(ERS_HERE, this->name(), handles);
    }

} // stop 
    
void ApplicationList::signal(int signum, const std::string& userName, const std::string& hostName) const {

    boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

    applist_t::const_iterator pos; 
    unsigned int failures = 0;
    std::string handles;

    for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {
      const Application *app = pos->second; 
      if(app && !(app->invalid())) {
	std::string applicationHandle =  app->toHandle()->toString();	
	try {
	  app->signal(signum, userName, hostName);
	}
	catch(pmg::Exception &ex) {
	  ERS_DEBUG(1,ex);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
	catch(...) {
	  ERS_DEBUG(1, "Unknown exception while sending signal to " << applicationHandle);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
      }      
    } // for
    
    if(failures > 0) {
      throw pmg::ApplicationList_Signal_Exception(ERS_HERE, this->name(), handles);
    }

} // signal

void ApplicationList::kill(const std::string& userName, const std::string& hostName) const {

    boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

    applist_t::const_iterator pos;
    unsigned int failures = 0;
    std::string handles;

    for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {
      const Application *app = pos->second ;
      if(app && !(app->invalid())) {
	std::string applicationHandle =  app->toHandle()->toString();
	try {
	  app->kill(userName,hostName);
	}
	catch(pmg::Exception &ex) {
	  ERS_DEBUG(1,ex);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
	catch(...) {
	  ERS_DEBUG(1, "Unknown exception while sending signal to " << applicationHandle);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
      }
    } // for
    
    if(failures > 0) {
      throw pmg::ApplicationList_Signal_Exception(ERS_HERE, this->name(), handles);
    }

} // kill

void ApplicationList::kill_soft(int timeout, const std::string& userName, const std::string& hostName) const {

    boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

    applist_t::const_iterator pos;
    unsigned int failures = 0;
    std::string handles;

    for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {
      const Application *app = pos->second ;
      if(app && !(app->invalid())) {
	std::string applicationHandle =  app->toHandle()->toString();
	try {
	  app->kill_soft(timeout, userName, hostName);
	}
	catch(pmg::Exception &ex) {
	  ERS_DEBUG(1,ex);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
	catch(...) {
	  ERS_DEBUG(1, "Unknown exception while sending signal to " << applicationHandle);
	  handles += applicationHandle;
	  handles += pmg::ProcessManagerSeparator;
	  ++failures;
	}
      }
    } // for

    if(failures > 0) {
      throw pmg::ApplicationList_Signal_Exception(ERS_HERE, this->name(), handles);
    }

} // kill_soft

Application* ApplicationList::lookup(const std::string& app_name) const {

  boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);
  
  Application* app_ptr = 0;

  if (m_name == app_name) {
    applist_t::const_reverse_iterator pos = m_application_list.rbegin();
    if (pos != m_application_list.rend()) {
      if(pos->second) {
	app_ptr = pos->second;
      }
    }
  }
  
  return app_ptr;
}

pmg_id_t ApplicationList::next_identity() {
    return ++m_last_id ;
} // next_identity

void ApplicationList::print_to(std::ostream& stream) const {

  boost::recursive_mutex::scoped_lock scoped_lock(Daemon::application_table_mutex);

  applist_t::const_iterator pos; 
  
  stream << "ApplicationList \"" << m_name << "\"" << std::endl; ; 
  for(pos=m_application_list.begin();pos!=m_application_list.end();++pos) {
    const long identity = pos->first ; 
    const Application *app = pos->second ; 
    if(app) stream << identity << ")\t" << (*app) << std::endl ; 
  } // for

} // print_to
  
} } // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::ApplicationList& list) {
    list.print_to(stream) ;
    return stream ;
}  // operator<<
