/*
 *  BinaryPath.cxx
 *
 *  Created by Marc Dobson on 27.06.05.
 *
 */


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <ers/ers.h>

#include "ProcessManager/BinaryPath.h"

namespace daq {

const std::string pmg::BinaryPath::m_path_seperation=":";

pmg::BinaryPath::BinaryPath(const std::string& path_list) {
  create(path_list);
}


pmg::BinaryPath::~BinaryPath() {
}


//**********************************************
//********** Private Member Functions **********
//**********************************************
bool pmg::BinaryPath::is_absolute(const std::string& exe) const
{
  if (exe[0]=='/') return true;
  return false;
}


bool pmg::BinaryPath::is_accesible(const std::string& exe) const
{
  if (access( exe.c_str(), F_OK ) == 0 ) return true;
  return false;
}


bool pmg::BinaryPath::is_executable(const std::string& exe) const
{
  if (access( exe.c_str(), X_OK ) == 0 ) return true;
  return false;
}


bool pmg::BinaryPath::is_file(const std::string& exe) const
{
  struct stat stat_buf;
  int ret_val = 0;

  ret_val = stat(exe.c_str(), &stat_buf);
  if (ret_val == 0) {
    if (S_ISREG(stat_buf.st_mode)) {
      return true;
    }
  }

  return false;
}


bool pmg::BinaryPath::check_binary(const std::string& exe) const
{
  if (!this->is_file(exe)) return false;
  if (!this->is_accesible(exe)) return false;
  if (!this->is_executable(exe)) return false;
  return true;
}


//**********************************************
//********** Public Member Functions  **********
//**********************************************
std::string pmg::BinaryPath::get_binary()
{
  if (m_binary_path_list.empty()) return m_null_path;

  std::list<std::string>::iterator iter;
  std::string path;

  for (iter=m_binary_path_list.begin();
       iter!=m_binary_path_list.end();
       iter++)
    {
      path = (*iter);
      if (this->check_binary(path)) return path;
    }

  ERS_DEBUG(1,"No correct binaries found.");
  return m_null_path;
}


void pmg::BinaryPath::create(const std::string& path)
{
  if (path.length() == 0) {
    ERS_DEBUG(1,"Empty input string. Binary path remains as it is.");
    return;
  }

  if (!m_binary_path_list.empty()) {
    m_binary_path_list.erase(m_binary_path_list.begin(),m_binary_path_list.end());
  }

  size_t index = (size_t) 0;
  std::string segment = "";
  std::string in_path = path;
  while ((index = in_path.find(pmg::BinaryPath::m_path_seperation,(size_t)0))!= std::string::npos) {
    segment = in_path;

    in_path.erase((size_t)0,(index+(size_t)1));
    segment.erase((index));

    if (!segment.empty()) {
	  m_binary_path_list.push_back(segment);
	  ERS_DEBUG(3,"Binary Path entry: "<< segment);
	}
  }

  if (!in_path.empty()) {
    m_binary_path_list.push_back(in_path);
    ERS_DEBUG(3,"Binary Path entry: "<< path);
  }
}


void pmg::BinaryPath::print() const
{
  std::list<std::string>::const_iterator iter;
  std::string path;

  for (iter=m_binary_path_list.begin();
       iter!=m_binary_path_list.end();
       iter++)
    {
        path = (*iter);
        if (iter != m_binary_path_list.end()) {
          std::cout << path << pmg::BinaryPath::m_path_seperation;
        }
    }
  std::cout << std::endl;
}
} //daq
