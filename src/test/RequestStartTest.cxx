#include <map>

#include <ipc/core.h>

#include <ers/ers.h>
#include <system/Host.h>

#include <boost/program_options.hpp>

#include "ProcessManager/client_simple.h"
#include "ProcessManager/Exceptions.h"

namespace po = boost::program_options;
using namespace daq::pmg;
using namespace std;

int main(int argc, char** argv) {

	try {

		IPCCore::init(argc, argv);
		Singleton::init();

		int procs;

		try {
			po::options_description desc("Test application starting multiple times the /bin/sleep 0 process");

			desc.add_options()
			("procs,N", po::value<int>(&procs), "Number of processes")
			("help,h", "Print help message");

			po::variables_map vm;
			po::store(po::parse_command_line(argc, argv, desc), vm);
			po::notify(vm);

			if(vm.count("help")) {
				std::cout << desc << std::endl;
				return EXIT_SUCCESS;
			}

			if(!vm.count("procs")) {
				ERS_INFO("ERROR: please specify the number of processes");
				std::cout << desc << std::endl;
				return EXIT_FAILURE;
			}
		}
		catch(std::exception& ex) {
			ERS_INFO("Bad command line options: " << ex.what());
			return EXIT_FAILURE;
		}

		System::Host* local_host = new System::LocalHost();
		std::string libpath(getenv("LD_LIBRARY_PATH"));
		std::map<std::string, std::string> env;
		env["LD_LIBRARY_PATH"] = libpath;

		vector<string> args;
		args.push_back("100");

		ProcessDescription* pd = new ProcessDescription(local_host->full_name(), // hostname
		                                                "pippo", // appname
		                                                "am_test", // partition
		                                                "/bin/sleep", // exec
		                                                "/tmp", // wd
		                                                args, // start args
		                                                env, // env
		                                                "/tmp", // logpath
		                                                "/dev/null", // stdin
		                                                "", // use rm
		                                                true, // null log
		                                                0); // timeout

		for(int i = 0; i < procs; ++i) {

			try {
				pd->start();
			}
			catch(Exception &ex) {
				ers::error(ex);
				//	return(EXIT_FAILURE);
			}

		}

		delete pd;

	}
	catch(ers::Issue &ex) {
		ers::error(ex);
		return (EXIT_FAILURE);
	}

	return (EXIT_SUCCESS);

}
