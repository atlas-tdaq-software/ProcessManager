#include <unistd.h>

#include <iostream>
#include <string>

#include <cmdl/cmdargs.h>
#include <ers/ers.h>
#include <ipc/partition.h>
#include <ipc/core.h>
#include <system/Host.h>

#include "pmgpriv/pmgpriv.hh"
#include "ProcessManager/Singleton.h"

int main(int argc, char** argv) {

  try {
  
    IPCCore::init(argc,argv);
   
    std::cout << "PIPPO: " << omniORB::giopMaxMsgSize() << std::endl; 

    CmdArgStr host('H', "host", "hostname", "host name", CmdArg::isREQ);
    CmdArgStr file('f', "file", "file", "name of the file to fetch", CmdArg::isREQ);
    CmdLine   cmd(*argv, &host, &file, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);
    cmd.parse(arg_iter);
    
    const std::string fileName(file);

    const std::string hostname(host);
    System::Host h(hostname);
 
    daq::pmg::Singleton::init();
    std::string pmgServerName = daq::pmg::ProcessManagerServerPrefix + h.full_name();
    pmgpriv::SERVER_var server = daq::pmg::Singleton::instance()->get_server(pmgServerName);

    pmgpriv::File_var fileContent = 0; 
    server->procFiles(fileName.c_str(), omniORB::giopMaxMsgSize(), fileContent);
    
    //    std::string fileAsAString((const char*)fileContent->get_buffer());

    ::write(1, fileContent->get_buffer(), fileContent->length());

    //    std::cout << fileAsAString << std::endl;

  }
  catch(ers::Issue &i) {
    ers::error(i);
  }
  catch(CORBA::Exception &ex) {
    std::cerr << "CORBA exception " << ex._name() << std::endl;
  }

}
