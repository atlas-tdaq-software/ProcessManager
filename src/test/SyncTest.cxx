#include <time.h>

#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>

#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <memory>

#include <ers/ers.h>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <owl/semaphore.h>

#include "ProcessManager/client_simple.h"

#include <system/Host.h>

namespace po = boost::program_options;
using namespace std;
using namespace daq;
using namespace pmg;

OWLSemaphore my_sem(0);

void wait(int);

void testcallback(std::shared_ptr<Process> process, void* test) {
	if(test != NULL) {
	} // avoids the unused param warning

	ERS_DEBUG(0,"Received callback for handle " << process->handle().toString()<< " with state "<<p_state_strings[(unsigned int) process->status().state] );
	if(process->exited()) {
		ERS_DEBUG(0,"Stop the test");
		my_sem.post();
	}
}

int main(int argc, char** argv) {
	try {
		IPCCore::init(argc, argv);
		pmg::Singleton::init();

		std::string script_name;
		int before_sync, after_sync;

		try {
			po::options_description
			        desc("Test application for the ProcessDescription and ProcessDescriptionDB classes");

			desc.add_options()("script,s", po::value<std::string>(&script_name), "Name of test script")("before,b",
			                                                                                            po::value<int>(&before_sync),
			                                                                                            "Time in seconds before sync is produced")("after,a",
			                                                                                                                                       po::value<
			                                                                                                                                               int>(&after_sync),
			                                                                                                                                       "Time in seconds to run after sync is produced")("help,h",
			                                                                                                                                                                                        "Print help message");

			po::variables_map vm;
			po::store(po::parse_command_line(argc, argv, desc), vm);
			po::notify(vm);

			if(vm.count("help")) {
				std::cout << desc << std::endl;
				return EXIT_SUCCESS;
			}

			if((!vm.count("script")) || (!vm.count("before")) || (!vm.count("after"))) {
				ERS_INFO("ERROR: please specify all the command line options");
				std::cout << desc << std::endl;
				return EXIT_FAILURE;
			}
		}
		catch(std::exception& ex) {
			ERS_INFO("Bad command line options: " << ex.what());
			return EXIT_FAILURE;
		}

		ERS_INFO( "Test will start " << script_name << ", wait for sync ("
				<< before_sync << " secs) and then kill it." << endl
				<< "2 callbacks should be obtained: RUNNING->EXITED.");

		std::ostringstream before_sync_str_stream;
		std::ostringstream after_sync_str_stream;
		before_sync_str_stream << "-W " << before_sync;
		after_sync_str_stream << "-w " << after_sync;

		vector<string> args;
		args.push_back(before_sync_str_stream.str());
		args.push_back(after_sync_str_stream.str());

		// Get the local host name
		System::Host* local_host = new System::LocalHost();

		std::string logpath = "/tmp/pmg_test_logs";

		std::string libpath(getenv("LD_LIBRARY_PATH"));
		libpath += ":/lib;/usr/lib;/usr/local/lib";
		std::map<std::string, std::string> env;
		env["LD_LIBRARY_PATH"] = libpath;

		// Create a ProcessDescription Object
		pmg::ProcessDescription* description = new pmg::ProcessDescription(local_host->full_name(), // hostname
		                                                                   "sync_test", // appname
		                                                                   "MyPartition", // partition
		                                                                   script_name, // execlist
		                                                                   "/tmp", // wd
		                                                                   args, // start args
		                                                                   env, // env
		                                                                   logpath, // logpath
		                                                                   "/dev/null", // stdin
		                                                                   "", // use rm
		                                                                   false, // null log
		                                                                   5); // timeout

		// Print the ProcessDescription Object
		ERS_DEBUG(3,"ProcessDescription:\n" << (*description));
		// Call the start method:
		description->start(testcallback);

		my_sem.wait();

		// Delete the object
		delete description;
		//delete startedList;


	}
	catch(ers::Issue &i) {
		ers::error(i);
	}
	exit(0);
}
