#!/bin/bash

# Test script for the sync mechanism on Process Manager

if [ $# -ne 2 ]; then
        echo "Input Error: needs two parameters to work."
        echo "******************************************"
        echo "produce_sync.sh <time_before_sync> <time_after_sync_before_exiting>"
        echo "Exiting with error code 1."
        exit 1
fi

time_before_sync=$1
time_after_sync=$2

# Delay a bit before producing the sync file
date
echo "Sleeping for ${time_before_sync} seconds before sync."
sleep ${time_before_sync}

# Produce the sync file (file name given by environment variable PMG_SYNC_FILE)
date
echo "Producing sync file ${PMG_SYNC_FILE}"
PMG_SYNC_DIR=`echo ${PMG_SYNC_FILE} | sed 's/\(.*\/\).*/\1/'`
echo "Writting dir: ${PMG_SYNC_DIR}"
mkdir -p ${PMG_SYNC_DIR}
echo "Writting file: ${PMG_SYNC_FILE}"
touch ${PMG_SYNC_FILE}

# Delay before exiting
date
echo "Sleeping for ${time_after_sync} seconds after sync before exiting."
sleep ${time_after_sync}

# Exiting
date
echo "Exiting ..."
