#include <sstream>

#include <ers/ers.h>

#include "ProcessManager/Utils.h"
#include "ProcessManager/Handle.h"

using namespace daq;

const char * const pmg::Handle::PREFIX = "pmg://";
const char * const pmg::Handle::SEPARATOR = "/";
const char * const pmg::Handle::PORT_SEPARATOR = ":";
const pmg::pmg_port_t pmg::Handle::DEFAULT_PORT_MARK = 0;

pmg::Handle::Handle(const System::Host &server,
                    const std::string &part_name,
                    const std::string &app_name,
                    pmg_id_t id,
                    pmg_port_t port) :
        m_server(new System::Host(server)), m_port(port), m_part_name(part_name), m_app_name(app_name), m_unique_id(id)
{
    if(part_name.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty partition name");
    }

    if(app_name.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty application name");
    }
} // Handle

pmg::Handle::Handle(const std::string &server_name,
                    const std::string &part_name,
                    const std::string &app_name,
                    pmg_id_t id,
                    pmg_port_t port) :
        m_server(new System::Host(server_name)), m_port(port), m_part_name(part_name), m_app_name(app_name), m_unique_id(id)
{
    // Check the input as much as I can
    if(part_name.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty partition name");
    }

    if(app_name.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty application name");
    }

    if(server_name.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty server name");
    }
} // Handle

pmg::Handle::Handle(const std::string & handle) {
    // Check the input conditions
    // handle is not empty
    if(handle.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty handle");
    }

    // protocol is correct
    const int prefix_len = strlen(pmg::Handle::PREFIX);
    const std::string protocol = handle.substr(0, prefix_len);
    if(protocol.compare(pmg::Handle::PREFIX) != 0) {
        const std::string errStr(std::string("Wrong protocol: ") + std::string(pmg::Handle::PREFIX));
        throw pmg::Invalid_Handle(ERS_HERE, errStr);
    }

    // Remove prefix
    const std::string data = handle.substr(prefix_len);

    // Break string up at level of "/" character
    std::vector<std::string> elements;
    daq::pmg::utils::tokenize(data, "/", elements);

    try {
        // assign all elements and check they are not empty
        std::string host_name = elements.at(0);
        if(host_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty host name in ") + handle);
        }

        m_part_name = elements.at(1);
        if(m_part_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty partition name in ") + handle);
        }

        m_app_name = elements.at(2);
        if(m_app_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty application name in ") + handle);
        }

        const std::string id_name = elements.at(3);
        if(id_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty ID in ") + handle);
        }
        m_unique_id = atol(id_name.c_str());

        // Find port number if it exists
        const std::string::size_type port_colon = host_name.find(pmg::Handle::PORT_SEPARATOR);
        m_port = pmg::Handle::DEFAULT_PORT_MARK;
        if(port_colon != std::string::npos) {
            const std::string port_string = host_name.substr(port_colon + 1);
            host_name = host_name.substr(0, port_colon);
            m_port = atoi(port_string.c_str());
        } // if semi-colon present

        // Create the host and assign it
        m_server.reset(new System::Host(host_name));
    }
    catch(std::out_of_range& ex) {
        throw pmg::Invalid_Handle(ERS_HERE,
                                  std::string("Bad format for ") + handle + ". It should be \""
                                          + std::string(pmg::Handle::PREFIX)
                                          + "host_name/partition_name/application_name/id\"");
    }
} // Handle

pmg::Handle::Handle(const Handle &handle) {
    // Set the internal members
    const System::Host *other_host = handle.m_server.get();
    if(other_host == nullptr) {
        throw pmg::Invalid_Handle(ERS_HERE, "Invalid host");
    }
    m_server.reset(new System::Host(*other_host));
    m_part_name = handle.m_part_name;
    m_app_name = handle.m_app_name;
    m_unique_id = handle.m_unique_id;
    m_port = handle.m_port;
} // Handle

pmg::Handle::Handle(System::Host &host, const std::string &local_handle) {
    if(local_handle.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty handle");
    }
    m_server.reset(new System::Host(host));
    m_port = pmg::Handle::DEFAULT_PORT_MARK;
    if(m_server == nullptr) {
        throw pmg::Invalid_Handle(ERS_HERE, std::string("Invalid host in ") + local_handle);
    }
    std::vector<std::string> elements;
    daq::pmg::utils::tokenize(local_handle, "/", elements);

    try {
        m_part_name = elements.at(0);
        if(m_part_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty partition name in ") + local_handle);
        }
        m_app_name = elements.at(1);
        if(m_app_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty application name in ") + local_handle);
        }
        const std::string id_name = elements.at(2);
        if(id_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty ID in ") + local_handle);
        }
        m_unique_id = atol(id_name.c_str());
    }
    catch(std::out_of_range& ex) {
        throw pmg::Invalid_Handle(ERS_HERE,
                                  std::string("Bad format for ") + local_handle
                                          + ". It should be \"partition_name/application_name/id\"");
    }
} // Handle

pmg::Handle::Handle(const std::string &host, const std::string &local_handle) {
    if(local_handle.empty()) {
        throw pmg::Invalid_Handle(ERS_HERE, "Empty handle");
    }
    m_server.reset(new System::Host(host));
    m_port = pmg::Handle::DEFAULT_PORT_MARK;
    if(m_server == nullptr) {
        throw pmg::Invalid_Handle(ERS_HERE, "Invalid host");
    }
    std::vector<std::string> elements;
    daq::pmg::utils::tokenize(local_handle, "/", elements);

    try {
        m_part_name = elements.at(0);
        if(m_part_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty partition name in ") + local_handle);
        }
        m_app_name = elements.at(1);
        if(m_app_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty application name in ") + local_handle);
        }
        const std::string id_name = elements.at(2);
        if(id_name.empty()) {
            throw pmg::Invalid_Handle(ERS_HERE, std::string("Empty ID in ") + local_handle);
        }
        m_unique_id = atol(id_name.c_str());
    }
    catch(std::out_of_range& ex) {
        throw pmg::Invalid_Handle(ERS_HERE,
                                  std::string("Bad format for ") + local_handle
                                          + ". It should be \"partition_name/application_name/id\"");
    }
} // Handle

pmg::Handle::~Handle() {
} // ~Handle

pmg::Handle::operator std::string() const {
    return toString();
} //  

const std::string pmg::Handle::toString() const {
    std::ostringstream stream;
    stream << *this;
    return stream.str();
} // toString

const std::string pmg::Handle::full_path() const {
    std::ostringstream stream;
    stream << *this;
    return stream.str();
} // full_path

bool pmg::Handle::equals(const pmg::Handle & other) const {
    if(!other.m_server->equals(*m_server))
        return false;
    if(other.m_port != m_port)
        return false;
    if(other.m_part_name != m_part_name)
        return false;
    if(other.m_app_name != m_app_name)
        return false;
    if(other.m_unique_id != m_unique_id)
        return false;
    return true;
} // equals

const std::string & pmg::Handle::server() const {
    return m_server->full_name();
} // server

unsigned int pmg::Handle::port() const {
    return m_port;
} // port

const std::string & pmg::Handle::partitionName() const {
    return m_part_name;
} // partitionName

const std::string & pmg::Handle::applicationName() const {
    return m_app_name;
} // applicationName

unsigned long pmg::Handle::id() const {
    return m_unique_id;
} // id

void pmg::Handle::printTo(std::ostream &stream) const {
    stream << pmg::Handle::PREFIX << m_server->full_name();
    if(m_port != pmg::Handle::DEFAULT_PORT_MARK) {
        stream << pmg::Handle::PORT_SEPARATOR << m_port;
    }
    stream << pmg::Handle::SEPARATOR << m_part_name;
    stream << pmg::Handle::SEPARATOR << m_app_name;
    stream << pmg::Handle::SEPARATOR << m_unique_id;
} // print_to 

std::ostream& operator<<(std::ostream& stream, const pmg::Handle& handle) {
    handle.printTo(stream);
    return stream;
} // operator<<

