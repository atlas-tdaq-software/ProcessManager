#include <unistd.h>

#include <ipc/partition.h>

#include "ProcessManager/Singleton.h"
#include "ProcessManager/Proxy.h"
#include "ProcessManager/CallBack.h"

namespace daq {
namespace pmg {

Proxy::Proxy(const Handle &handle) :
        m_handle(handle)
{
} // Proxy

Proxy::~Proxy() {
} // ~Proxy

void Proxy::info(const PMGProcessStatusInfo& info, bool updateAll) {
    boost::unique_lock<boost::shared_mutex> lk(m_info_mutex);

    if(updateAll) {
        m_process_info = info;
    } else {
        // Do not update env and start args
        m_process_info.state = info.state;
        m_process_info.start_time = info.start_time;
        m_process_info.stop_time = info.stop_time;
        m_process_info.failure_str = info.failure_str;
        m_process_info.failure_str_hr = info.failure_str_hr;
        m_process_info.start_info.client_host = info.start_info.client_host;
        m_process_info.start_info.host = info.start_info.host;
        m_process_info.start_info.app_name = info.start_info.app_name;
        m_process_info.start_info.partition = info.start_info.partition;
        m_process_info.start_info.executable = info.start_info.executable;
        m_process_info.start_info.wd = info.start_info.wd;
        m_process_info.start_info.rm_token = info.start_info.rm_token;
        m_process_info.start_info.streams = info.start_info.streams;
        //    m_process_info.start_info.start_args = request_info.start_args;
        //    m_process_info.start_info.envs = request_info.envs;
        m_process_info.start_info.init_time_out = info.start_info.init_time_out;
        m_process_info.start_info.auto_kill_time_out = info.start_info.auto_kill_time_out;
        m_process_info.start_info.user = info.start_info.user;
        m_process_info.resource_usage = info.resource_usage;
        m_process_info.info = info.info;
        m_process_info.out_is_available = info.out_is_available;
        m_process_info.err_is_available = info.err_is_available;
    }
} // info

void Proxy::updateState(PMGProcessState newState) {
    boost::unique_lock<boost::shared_mutex> lk(m_info_mutex);

    m_process_info.state = newState;
}

const Handle& Proxy::handle() const {
    return m_handle;
} // handle

PMGProcessStatusInfo Proxy::info() const {
    boost::shared_lock<boost::shared_mutex> lk(m_info_mutex);

    return m_process_info;
} // info

void Proxy::executeCallbacks() {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    ERS_DEBUG(3, "Proxy::executeCallbacks call for handle "<<m_handle.toString());

    // Call the User callbacks
    for(CallBackIterator iter = m_callbacks.begin(); iter != m_callbacks.end();) {
        if(*iter) {
            (*(iter++))->call();
        } else {
            ++iter;
        }
    }

} // executeCallbacks

bool Proxy::isDeadEndState(const PMGProcessState state) const {
    // ASSUMPTION: these are the only end states
    return (state == PMGProcessState_FAILED || state == PMGProcessState_SYNCERROR || state == PMGProcessState_EXITED
            || state == PMGProcessState_SIGNALED);
}

void Proxy::link(const CallBack* callback) {
    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);
    m_callbacks.push_back(const_cast<CallBack*>(callback));
} // link

void Proxy::unlink(const CallBack* callback) {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    // Only remove the pointer from the list w/o deleting it
    // It is up to ProcessImpl to take care of that

    for(CallBackIterator iter = m_callbacks.begin(); iter != m_callbacks.end();) {
        if((*iter) == callback) {
            m_callbacks.erase(iter++);
            break;
        } else {
            ++iter;
        }
    }

} // unlink

bool Proxy::exited() const {
    boost::shared_lock<boost::shared_mutex> lk(m_info_mutex);

    return isDeadEndState(m_process_info.state);
}

pmgpriv::SERVER_var Proxy::getServer() const {
    std::string pmgserverName = ProcessManagerServerPrefix + m_handle.server();

    pmgpriv::SERVER_var server = Singleton::instance()->get_server(pmgserverName);

    return server;

} // getServer()

void Proxy::handleSignalFailure() {
    boost::shared_lock<boost::shared_mutex> lk(m_info_mutex);
    const PMGProcessState currState = m_process_info.state;
    lk.unlock();

    try {
        if(isDeadEndState(currState) == false) {
            pmgpriv::SERVER_var pServer = getServer();

            pmgpub::ProcessStatusInfo_var pInfo = 0;
            const bool done = pServer->get_info(m_handle.toString().c_str(), pInfo);

            boost::unique_lock<boost::shared_mutex> lk(m_info_mutex);
            // Just check that in the while the information has not been updated
            if(m_process_info.state == currState) {
                if(done == true) {
                    m_process_info = pInfo;
                } else {
                    m_process_info.state = PMGProcessState_EXITED;
                }
            }
        }
    }
    catch(pmg::No_PMG_Server& ex) {
        ers::log(ex);
    }
    catch(pmgpriv::get_info_ex& ex) {
        ERS_LOG("Failed getting information about process " << m_handle.applicationName() << ": " << std::string(ex.error_string));
    }
    catch(CORBA::Exception& ex) {
        ERS_LOG("Failed getting information about process " << m_handle.applicationName() << ": " << std::string(ex._name()));
    }
}

void Proxy::stop() {
    pmgpriv::SERVER_var server = getServer(); // throw No_PMG_Server
    try { // Send the process request to the agent
        server->stop(m_handle.toString().c_str(),
                     Singleton::instance()->credentials().c_str(),
                     Singleton::instance()->local_host().c_str());

    }
    catch(pmgpriv::signal_ex &ex) {
        switch(ex.reason) {
            case PMGSignalException_APPLICATION_NOTFOUND_INVALID:
            case PMGSignalException_MANIFEST_UNMAPPED:
            case PMGSignalException_LAUNCHER_NOT_RUNNING: {
                handleSignalFailure();
                throw pmg::Process_Already_Exited(ERS_HERE,
                                                  m_handle.toString(),
                                                  m_handle.applicationName(),
                                                  std::string(ex.error_string));
                break;
            }
            case PMGSignalException_NOT_ALLOWED_BY_AM:
                throw pmg::Signal_Not_Allowed(ERS_HERE,
                                              m_handle.toString(),
                                              m_handle.applicationName(),
                                              std::string(ex.error_string));
                break;
            case PMGSignalException_FIFO_ERROR:
            case PMGSignalException_UNEXPECTED:
            case PMGSignalException_UNKNOWN:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
                break;
            default:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
        }
    }
    catch(CORBA::TRANSIENT& ex) {
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + m_handle.server(), m_handle.server(), ex._name());
    }
    catch(CORBA::Exception &ex) {
        throw pmg::Bad_PMG_Server(ERS_HERE, m_handle.server(), ex._name());
    }
} // stop()

void Proxy::kill() {
    pmgpriv::SERVER_var server = getServer(); // throw No_PMG_Server
    try { // Send the process request to the agent
        server->kill(m_handle.toString().c_str(),
                     Singleton::instance()->credentials().c_str(),
                     Singleton::instance()->local_host().c_str());
    }
    catch(pmgpriv::signal_ex &ex) {
        switch(ex.reason) {
            case PMGSignalException_APPLICATION_NOTFOUND_INVALID:
            case PMGSignalException_MANIFEST_UNMAPPED:
            case PMGSignalException_LAUNCHER_NOT_RUNNING: {
                handleSignalFailure();
                throw pmg::Process_Already_Exited(ERS_HERE,
                                                  m_handle.toString(),
                                                  m_handle.applicationName(),
                                                  std::string(ex.error_string));
                break;
            }
            case PMGSignalException_NOT_ALLOWED_BY_AM:
                throw pmg::Signal_Not_Allowed(ERS_HERE,
                                              m_handle.toString(),
                                              m_handle.applicationName(),
                                              std::string(ex.error_string));
                break;
            case PMGSignalException_FIFO_ERROR:
            case PMGSignalException_UNEXPECTED:
            case PMGSignalException_UNKNOWN:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
                break;
            default:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
        }
    }
    catch(CORBA::TRANSIENT& ex) {
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + m_handle.server(), m_handle.server(), ex._name());
    }
    catch(CORBA::Exception& ex) {
        throw pmg::Bad_PMG_Server(ERS_HERE, m_handle.server(), ex._name());
    }
} // kill()

void Proxy::kill_soft(const int timeout) {
    pmgpriv::SERVER_var server = getServer(); // throw No_PMG_Server
    try { // Send the process request to the agent
        server->kill_soft(m_handle.toString().c_str(),
                          timeout,
                          Singleton::instance()->credentials().c_str(),
                          Singleton::instance()->local_host().c_str());
    }
    catch(pmgpriv::signal_ex &ex) {
        switch(ex.reason) {
            case PMGSignalException_APPLICATION_NOTFOUND_INVALID:
            case PMGSignalException_MANIFEST_UNMAPPED:
            case PMGSignalException_LAUNCHER_NOT_RUNNING: {
                handleSignalFailure();
                throw pmg::Process_Already_Exited(ERS_HERE,
                                                  m_handle.toString(),
                                                  m_handle.applicationName(),
                                                  std::string(ex.error_string));
                break;
            }
            case PMGSignalException_NOT_ALLOWED_BY_AM:
                throw pmg::Signal_Not_Allowed(ERS_HERE,
                                              m_handle.toString(),
                                              m_handle.applicationName(),
                                              std::string(ex.error_string));
                break;
            case PMGSignalException_FIFO_ERROR:
            case PMGSignalException_UNEXPECTED:
            case PMGSignalException_UNKNOWN:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
                break;
            default:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
        }
    }
    catch(CORBA::TRANSIENT& ex) {
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + m_handle.server(), m_handle.server(), ex._name());
    }
    catch(CORBA::Exception &ex) {
        throw pmg::Bad_PMG_Server(ERS_HERE, m_handle.server(), ex._name());
    }
} // kill_soft()

void Proxy::signal(const int signum) {
    pmgpriv::SERVER_var server = getServer(); // throw No_PMG_Server
    try { // Send the process request to the agent
        server->signal(m_handle.toString().c_str(),
                       signum,
                       Singleton::instance()->credentials().c_str(),
                       Singleton::instance()->local_host().c_str());
    }
    catch(pmgpriv::signal_ex& ex) {
        switch(ex.reason) {
            case PMGSignalException_APPLICATION_NOTFOUND_INVALID:
            case PMGSignalException_MANIFEST_UNMAPPED:
            case PMGSignalException_LAUNCHER_NOT_RUNNING: {
                handleSignalFailure();
                throw pmg::Process_Already_Exited(ERS_HERE,
                                                  m_handle.toString(),
                                                  m_handle.applicationName(),
                                                  std::string(ex.error_string));
                break;
            }
            case PMGSignalException_NOT_ALLOWED_BY_AM:
                throw pmg::Signal_Not_Allowed(ERS_HERE,
                                              m_handle.toString(),
                                              m_handle.applicationName(),
                                              std::string(ex.error_string));
                break;
            case PMGSignalException_FIFO_ERROR:
            case PMGSignalException_UNEXPECTED:
            case PMGSignalException_UNKNOWN:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
                break;
            default:
                throw pmg::Server_Internal_Error(ERS_HERE,
                                                 m_handle.toString(),
                                                 m_handle.applicationName(),
                                                 std::string(ex.error_string));
        }
    }
    catch(CORBA::TRANSIENT& ex) {
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + m_handle.server(), m_handle.server(), ex._name());
    }
    catch(CORBA::Exception &ex) {
        throw pmg::Bad_PMG_Server(ERS_HERE, m_handle.server(), ex._name());
    }
} // signal()

void Proxy::errFile(pmgpriv::File_var& fileContent) const {
    pmgpriv::SERVER_var server = getServer(); // throw No_PMG_Server

    boost::shared_lock<boost::shared_mutex> lk(m_info_mutex);
    const std::string errFileName(m_process_info.start_info.streams.err_path);
    lk.unlock();

    try {
        server->procFiles(errFileName.c_str(), omniORB::giopMaxMsgSize(), fileContent);
    }
    catch(pmgpriv::procFiles_ex &ex) {
        throw pmg::Cannot_Get_File(ERS_HERE, errFileName, std::string(ex.error_string), ex.code);
    }
    catch(CORBA::TRANSIENT& ex) {
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + m_handle.server(), m_handle.server(), ex._name());
    }
    catch(CORBA::Exception &ex) {
        throw pmg::Bad_PMG_Server(ERS_HERE, m_handle.server(), ex._name());
    }
}

void Proxy::outFile(pmgpriv::File_var& fileContent) const {
    pmgpriv::SERVER_var server = getServer(); // throw No_PMG_Server

    boost::shared_lock<boost::shared_mutex> lk(m_info_mutex);
    const std::string outFileName(m_process_info.start_info.streams.out_path);
    lk.unlock();

    try {
        server->procFiles(outFileName.c_str(), omniORB::giopMaxMsgSize(), fileContent);
    }
    catch(pmgpriv::procFiles_ex &ex) {
        throw pmg::Cannot_Get_File(ERS_HERE, outFileName, std::string(ex.error_string), ex.code);
    }
    catch(CORBA::TRANSIENT& ex) {
        throw pmg::No_PMG_Server(ERS_HERE, "No PMG server on host " + m_handle.server(), m_handle.server(), ex._name());
    }
    catch(CORBA::Exception &ex) {
        throw pmg::Bad_PMG_Server(ERS_HERE, m_handle.server(), ex._name());
    }
}

}
} // daq::pmg
