#include <ers/ers.h>

#include "ProcessManager/CallBack.h"
#include "ProcessManager/Process.h"

using namespace daq;

pmg::CallBack::CallBack(const std::shared_ptr<Process>& callbackowner, CallBackFncPtr callback, void* callbackparameter) :
        m_callBackOwner(callbackowner), m_callBackParameter(callbackparameter), m_callBackFcnPtr(callback)
{

    ERS_ASSERT(callback);
    ERS_ASSERT(callbackowner);

    // callbackowner is a Process* passed to all callbacks;
    // it allows queries on the process state/info
} // CallBack

pmg::CallBack::~CallBack() {
}

void pmg::CallBack::call() const {

    ERS_DEBUG(3, "state is " << m_callBackOwner->status().failure_str);

    // call the user callback function with the Process* reference and the optional parameter
    m_callBackFcnPtr(m_callBackOwner, m_callBackParameter);

} // call
