#include <ers/ers.h>

#include "ProcessManager/ProxyTable.h"
#include "ProcessManager/Proxy.h"

namespace daq {
namespace pmg {

ProxyTable::ProxyTable() {
} // ProxyTable

ProxyTable::~ProxyTable() {
} // ~ProxyTable

std::shared_ptr<Proxy> ProxyTable::factory(const pmg::Handle& handle) {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    std::shared_ptr<Proxy> proxy_ptr;

    const std::string hdlStr(handle.toString());
    proxyMapIterator iter = m_proxyMap.find(hdlStr);
    if(iter == m_proxyMap.end()) {
        proxy_ptr.reset(new Proxy(handle));
        m_proxyMap.insert(std::make_pair(hdlStr, proxy_ptr));
    } else {
        proxy_ptr = iter->second;
    }

    return proxy_ptr;

} // factory

void ProxyTable::callProxyCallbacks(const std::string& handle, PMGProcessState newState) {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    proxyMapIterator iter = m_proxyMap.find(handle);
    if(iter != m_proxyMap.end()) {
        // The Proxy exists and the callbacks can be executed (if any)
        iter->second->updateState(newState);
        iter->second->executeCallbacks();
    }
}

void ProxyTable::callProxyCallbacks(const std::string& handle, const PMGProcessStatusInfo& info) {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    proxyMapIterator iter = m_proxyMap.find(handle);
    if(iter != m_proxyMap.end()) {
        // The Proxy exists and the callbacks can be executed (if any)
        iter->second->info(info, false);
        iter->second->executeCallbacks();
    } else {
        // The Proxy does not exist yet and no callbacks can be executed.
        // Create the Proxy.
        try {
            Handle hdl(handle);
            std::shared_ptr<Proxy> proxy_ptr(std::make_shared<Proxy>(hdl));
            m_proxyMap.insert(std::make_pair(handle, proxy_ptr));
            proxy_ptr->info(info, true);
        }
        catch(pmg::Invalid_Handle& ex) {
            ers::error(ex);
        }
    }

} // factory

void ProxyTable::remove(const std::string& handle) {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    // look for the wanted proxy and remove it from the map
    proxyMapConstIterator iter = m_proxyMap.find(handle);
    if(iter != m_proxyMap.end()) {
        m_proxyMap.erase(iter);
    }

} // remove

std::shared_ptr<Proxy> ProxyTable::find(const std::string& handle) const {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    std::shared_ptr<Proxy> proxy_ptr;
    proxyMapConstIterator iter = m_proxyMap.find(handle);
    if(iter != m_proxyMap.end()) {
        proxy_ptr = iter->second;
    }
    return proxy_ptr;

} // remove

std::list<std::shared_ptr<Proxy>> ProxyTable::find_all() const {

    boost::recursive_mutex::scoped_lock scoped_lock(m_mutex);

    std::list<std::shared_ptr<Proxy>> proxy_list;
    proxyMapConstIterator iter = m_proxyMap.begin();
    while(iter != m_proxyMap.end()) {
        proxy_list.push_back(iter->second);
        iter++;
    }
    return proxy_list;
}

}
} // daq::pmg
