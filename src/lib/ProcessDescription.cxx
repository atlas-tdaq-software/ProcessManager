#include "ProcessManager/ProcessDescription.h"
#include "ProcessManager/ProcessDescriptionImpl.h"

namespace daq {
namespace pmg {

// Static member definition
const std::vector<std::string> ProcessDescription::emptyArgs;
const std::map<std::string, std::string> ProcessDescription::emptyEnv;

ProcessDescription::ProcessDescription(const std::string& host_name,
                                       const std::string& app_name,
                                       const std::string& partition,
                                       const std::string& exec_list,
                                       const std::string& wd,
                                       const std::vector<std::string>& start_args,
                                       const std::map<std::string, std::string>& env,
                                       const std::string& log_path,
                                       const std::string& input_dev,
                                       const std::string& rm_swobject,
                                       bool null_logs,
                                       unsigned long init_time_out,
                                       unsigned long auto_kill_time_out) :
        m_impl(new ProcessDescriptionImpl(host_name,
                                          app_name,
                                          partition,
                                          exec_list,
                                          wd,
                                          start_args,
                                          env,
                                          log_path,
                                          input_dev,
                                          rm_swobject,
                                          null_logs,
                                          init_time_out,
                                          auto_kill_time_out))
{
} // ProcessDescription

ProcessDescription::ProcessDescription(const System::Host& host,
                                       const std::string& app_name,
                                       const std::string& partition,
                                       const std::string& exec_list,
                                       const std::string& wd,
                                       const std::vector<std::string>& start_args,
                                       const std::map<std::string, std::string>& env,
                                       const std::string& log_path,
                                       const std::string& input_dev,
                                       const std::string& rm_swobject,
                                       bool null_logs,
                                       unsigned long init_time_out,
                                       unsigned long auto_kill_time_out) :
        m_impl(new ProcessDescriptionImpl(host,
                                          app_name,
                                          partition,
                                          exec_list,
                                          wd,
                                          start_args,
                                          env,
                                          log_path,
                                          input_dev,
                                          rm_swobject,
                                          null_logs,
                                          init_time_out,
                                          auto_kill_time_out))
{
} // ProcessDescription

ProcessDescription::ProcessDescription(const std::string& host_name,
                                       const std::string& app_name,
                                       const std::string& partition,
                                       const std::vector<std::string>& exec_list,
                                       const std::string& wd,
                                       const std::string& start_args,
                                       const std::map<std::string, std::string>& env,
                                       const std::string& log_path,
                                       const std::string& input_dev,
                                       const std::string& rm_swobject,
                                       bool null_logs,
                                       unsigned long init_time_out,
                                       unsigned long auto_kill_time_out) :
        m_impl(new ProcessDescriptionImpl(host_name,
                                          app_name,
                                          partition,
                                          exec_list,
                                          wd,
                                          start_args,
                                          env,
                                          log_path,
                                          input_dev,
                                          rm_swobject,
                                          null_logs,
                                          init_time_out,
                                          auto_kill_time_out))
{
} // ProcessDescription

ProcessDescription::ProcessDescription(const System::Host& host,
                                       const std::string& app_name,
                                       const std::string& partition,
                                       const std::vector<std::string>& exec_list,
                                       const std::string& wd,
                                       const std::string& start_args,
                                       const std::map<std::string, std::string>& env,
                                       const std::string& log_path,
                                       const std::string& input_dev,
                                       const std::string& rm_swobject,
                                       bool null_logs,
                                       unsigned long init_time_out,
                                       unsigned long auto_kill_time_out) :
        m_impl(new ProcessDescriptionImpl(host.full_name(),
                                          app_name,
                                          partition,
                                          exec_list,
                                          wd,
                                          start_args,
                                          env,
                                          log_path,
                                          input_dev,
                                          rm_swobject,
                                          null_logs,
                                          init_time_out,
                                          auto_kill_time_out))
{
} // ProcessDescription

ProcessDescription::ProcessDescription(const ProcessDescription &other) :
        m_impl(new ProcessDescriptionImpl(*(other.m_impl)))
{
} // ProcessDescription

ProcessDescription::~ProcessDescription() throw () {
} // ~ProcessDescription

bool ProcessDescription::operator==(const ProcessDescription &other) const {
    return ((*m_impl) == (*(other.m_impl)));
}

bool ProcessDescription::operator!=(const ProcessDescription &other) const {
    return ((*m_impl) != (*(other.m_impl)));
}

std::shared_ptr<Process> ProcessDescription::start(CallBackFncPtr callback, void* callbackparameter) {
    return m_impl->start(callback, callbackparameter);
}

const std::string ProcessDescription::toString() const {
    return m_impl->toString();
} // toString

const std::string ProcessDescription::app_name() const {
    return m_impl->app_name();
}

void ProcessDescription::printTo(std::ostream &stream, const bool prettyPrint) const {
    m_impl->printTo(stream, prettyPrint);
} // printTo

}
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::ProcessDescription& description) {
    description.printTo(stream, true);
    return stream;
} // operator<<
