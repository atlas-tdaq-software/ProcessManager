#include <unistd.h>
#include <pwd.h>
#include <sstream>
#include <sys/stat.h>

#include <ipc/partition.h>
#include <ipc/util.h>

#include <omniORB4/minorCode.h>

#include "ProcessManager/ProcessDescriptionImpl.h"
#include "ProcessManager/Utils.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Process.h"

#include "pmgpub/pmgpub.hh"
#include "pmgpriv/pmgpriv.hh"

#ifdef minor
#undef minor
#endif

using namespace daq;

pmg::ProcessDescriptionImpl::ProcessDescriptionImpl(const std::string& host_name,
                                                    const std::string& app_name,
                                                    const std::string& partition,
                                                    const std::string& exec_list,
                                                    const std::string& wd,
                                                    const std::vector<std::string>& start_args,
                                                    const std::map<std::string, std::string>& env,
                                                    const std::string& log_path,
                                                    const std::string& input_dev,
                                                    const std::string& rm_swobject,
                                                    bool null_logs,
                                                    unsigned long init_time_out,
                                                    unsigned long auto_kill_time_out) :
        m_app_name(app_name), m_partition(partition), m_exec_list(exec_list), m_wd(wd), m_start_args(start_args),
        m_env(env), m_log_path(log_path), m_input_dev(input_dev), m_rm_swobject(rm_swobject), m_null_logs(null_logs),
        m_init_time_out(init_time_out), m_auto_kill_time_out(auto_kill_time_out), m_host(new System::Host(host_name)),
        m_local_host(pmg::Singleton::instance()->local_host()), m_login_name(pmg::Singleton::instance()->credentials())

{
    // ****** Do the initial checking ******
    assert(app_name != "");
    assert(exec_list != "");
}

pmg::ProcessDescriptionImpl::ProcessDescriptionImpl(const System::Host& host,
                                                    const std::string& app_name,
                                                    const std::string& partition,
                                                    const std::string& exec_list,
                                                    const std::string& wd,
                                                    const std::vector<std::string>& start_args,
                                                    const std::map<std::string, std::string>& env,
                                                    const std::string& log_path,
                                                    const std::string& input_dev,
                                                    const std::string& rm_swobject,
                                                    bool null_logs,
                                                    unsigned long init_time_out,
                                                    unsigned long auto_kill_time_out)

:
        m_app_name(app_name), m_partition(partition), m_exec_list(exec_list), m_wd(wd), m_start_args(start_args), m_env(env),
        m_log_path(log_path), m_input_dev(input_dev), m_rm_swobject(rm_swobject), m_null_logs(null_logs),
        m_init_time_out(init_time_out), m_auto_kill_time_out(auto_kill_time_out), m_host(new System::Host(host)),
        m_local_host(pmg::Singleton::instance()->local_host()), m_login_name(pmg::Singleton::instance()->credentials())
{

    // ****** Do the initial checking ******
    assert(app_name != "");
    assert(exec_list != "");
}

pmg::ProcessDescriptionImpl::ProcessDescriptionImpl(const std::string& host_name,
                                                    const std::string& app_name,
                                                    const std::string& partition,
                                                    const std::vector<std::string>& exec_list,
                                                    const std::string& wd,
                                                    const std::string& start_args,
                                                    const std::map<std::string, std::string>& env,
                                                    const std::string& log_path,
                                                    const std::string& input_dev,
                                                    const std::string& rm_swobject,
                                                    bool null_logs,
                                                    unsigned long init_time_out,
                                                    unsigned long auto_kill_time_out)

:
        m_app_name(app_name), m_partition(partition), m_wd(wd), m_env(env), m_log_path(log_path), m_input_dev(input_dev),
        m_rm_swobject(rm_swobject), m_null_logs(null_logs), m_init_time_out(init_time_out), m_auto_kill_time_out(auto_kill_time_out),
        m_host(new System::Host(host_name)), m_local_host(pmg::Singleton::instance()->local_host()),
        m_login_name(pmg::Singleton::instance()->credentials())
{
    std::vector<std::string>::const_iterator iter;

    // ****** Do the initial checking ******
    assert(app_name != "");

    // Convert the exec_list to a string and check it is not empty
    for(iter = exec_list.begin(); iter != exec_list.end(); iter++) {
        if((*iter) != "") {
            // Add the item if not empty
            if(m_exec_list.length() != 0) {
                m_exec_list = m_exec_list + ":" + (*iter);
            } else {
                m_exec_list = (*iter);
            }
        }
    }
    assert(m_exec_list != "");

    // Split the start arguments into a vector
    daq::pmg::utils::tokenize(start_args, " ", m_start_args);
}

pmg::ProcessDescriptionImpl::ProcessDescriptionImpl(const ProcessDescriptionImpl &other) :
        m_app_name(other.m_app_name), m_partition(other.m_partition), m_exec_list(other.m_exec_list), m_wd(other.m_wd),
        m_start_args(other.m_start_args), m_env(other.m_env), m_log_path(other.m_log_path), m_input_dev(other.m_input_dev),
        m_rm_swobject(other.m_rm_swobject), m_null_logs(other.m_null_logs), m_init_time_out(other.m_init_time_out),
        m_auto_kill_time_out(other.m_auto_kill_time_out), m_host(new System::Host(other.host())),
        m_local_host(pmg::Singleton::instance()->local_host()), m_login_name(pmg::Singleton::instance()->credentials())

{
}

pmg::ProcessDescriptionImpl::ProcessDescriptionImpl() :
        m_null_logs(false), m_init_time_out(0), m_auto_kill_time_out(0), m_host(nullptr),
        m_local_host(pmg::Singleton::instance()->local_host()), m_login_name(pmg::Singleton::instance()->credentials())
{
} // ProcessDescriptionImpl

pmg::ProcessDescriptionImpl::~ProcessDescriptionImpl() {
} // ~ProcessDescriptionImpl

bool pmg::ProcessDescriptionImpl::operator==(const ProcessDescriptionImpl &other) const {
    if(!((*m_host) == (other.host())))
        return false;
    if(!(m_app_name == other.m_app_name))
        return false;
    if(!(m_partition == other.m_partition))
        return false;
    if(!(m_exec_list == other.m_exec_list))
        return false;
    if(!(m_wd == other.m_wd))
        return false;
    if(!(m_start_args == other.m_start_args))
        return false;
    if(!(m_env == other.m_env))
        return false;
    if(!(m_log_path == other.m_log_path))
        return false;
    if(!(m_input_dev == other.m_input_dev))
        return false;
    if(!(m_null_logs == other.m_null_logs))
        return false;
    if(!(m_init_time_out == other.m_init_time_out))
        return false;

    return true;
} // operator==

bool pmg::ProcessDescriptionImpl::operator!=(const ProcessDescriptionImpl &other) const {
    return (!((*this) == other));
} // operator!=

const System::Host pmg::ProcessDescriptionImpl::host() const {
    return (*m_host);
} // host

std::shared_ptr<pmg::Process> pmg::ProcessDescriptionImpl::start(CallBackFncPtr callback, void* callbackparameter) {
    std::shared_ptr<pmg::Process> process_ptr;

    // Fill the Corba structure

    std::string empty_info = "";
    pmgpriv::CLIENT_var client_var = pmg::Singleton::instance()->reference();

    pmgpriv::ProcessRequestInfo request_info;
    request_info.client_ref = client_var;
    request_info.client_host = CORBA::string_dup(m_local_host.c_str());

    request_info.host = CORBA::string_dup((m_host->full_name()).c_str());
    request_info.app_name = CORBA::string_dup(m_app_name.c_str());
    request_info.partition = CORBA::string_dup(m_partition.c_str());
    request_info.exec_list = CORBA::string_dup(m_exec_list.c_str());
    request_info.wd = CORBA::string_dup(m_wd.c_str());

    request_info.rm_swobject = CORBA::string_dup(m_rm_swobject.c_str());

    request_info.streams.log_path = CORBA::string_dup(m_log_path.c_str());
    request_info.streams.out_path = CORBA::string_dup(empty_info.c_str());
    request_info.streams.err_path = CORBA::string_dup(empty_info.c_str());
    request_info.streams.in_path = CORBA::string_dup(m_input_dev.c_str());
    request_info.streams.perms = (mode_t) ((S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH));
    request_info.streams.out_is_null = m_null_logs;

    request_info.init_time_out = m_init_time_out;
    request_info.auto_kill_time_out = m_auto_kill_time_out;

    request_info.user.name = CORBA::string_dup(m_login_name.c_str());

    // Fill the arguments
    pmg_arg_type_iterator arg_iter;
    int pos = 0;
    request_info.start_args.length(m_start_args.size());
    for(arg_iter = m_start_args.begin(); arg_iter != m_start_args.end(); ++arg_iter) {
        request_info.start_args[pos] = CORBA::string_dup((*arg_iter).c_str());
        ++pos;
    }

    // Fill the env
    request_info.envs.length(m_env.size());
    pos = 0;
    pmg_env_type_const_iterator env_iter;
    for(env_iter = m_env.begin(); env_iter != m_env.end(); ++env_iter) {
        request_info.envs[pos].name = CORBA::string_dup((env_iter->first).c_str());
        request_info.envs[pos].value = CORBA::string_dup((env_iter->second).c_str());
        ++pos;
    }

    // Call the remote function
    std::string serverName = ProcessManagerServerPrefix + m_host->full_name();
    ERS_DEBUG(2, "Ask Server = " << serverName << " to start " << this->app_name() << " on host " << this->host());

    try {
        // get the agent reference (it may throw pmg::No_PMG_Server)
        pmgpriv::SERVER_var server = pmg::Singleton::instance()->get_server(serverName);

        // Send the process request to the agent
        CORBA::String_var Hdl = server->request_start(request_info);

        const std::string str_handle(Hdl);
        Handle hh(str_handle);

        ERS_DEBUG(3, "Received process handle from server "<< str_handle);

        // Register this process with Singleton (creates proxy if not yet)
        pmgpub::ProcessStatusInfo status_info;
        status_info.state = pmgpub::REQUESTED;
        status_info.start_time = ::time(NULL);
        status_info.stop_time = ::time(NULL);
        status_info.failure_str = CORBA::string_dup("requested");
        status_info.failure_str_hr = CORBA::string_dup("None");
        status_info.start_info.client_host = request_info.client_host;
        status_info.start_info.host = request_info.host;
        status_info.start_info.app_name = request_info.app_name;
        status_info.start_info.partition = request_info.partition;
        status_info.start_info.executable = CORBA::string_dup("None");
        status_info.start_info.wd = request_info.wd;
        status_info.start_info.rm_token = -999;
        status_info.start_info.streams.log_path = request_info.streams.log_path;
        status_info.start_info.streams.out_path = request_info.streams.out_path;
        status_info.start_info.streams.err_path = request_info.streams.err_path;
        status_info.start_info.streams.in_path = request_info.streams.in_path;
        status_info.start_info.streams.perms = request_info.streams.perms;
        status_info.start_info.streams.out_is_null = request_info.streams.out_is_null;
        status_info.start_info.start_args = request_info.start_args;
        status_info.start_info.envs = request_info.envs;
        status_info.start_info.init_time_out = request_info.init_time_out;
        status_info.start_info.user.name = request_info.user.name;
        status_info.resource_usage.total_user_time = -999;
        status_info.resource_usage.total_system_time = -999;
        status_info.resource_usage.memory_usage = -999;
        status_info.info.process_id = -99999;
        status_info.info.signal_value = -999;
        status_info.info.exit_value = -999;
        status_info.out_is_available = false;
        status_info.err_is_available = false;

        // Create the Proxy and get a Process instance
        process_ptr = pmg::Singleton::instance()->start_finished(hh, status_info);

        // Register callback
        if(callback != 0) {
            ERS_DEBUG(5, "Registering callback for handle " << str_handle);
            process_ptr->link(callback, callbackparameter);
        }

        // Start the process

        ERS_DEBUG(3, "Starting process with handle "<< str_handle);

        try {
            server->really_start(str_handle.c_str());
        }
        catch(CORBA::TIMEOUT& ex) {
            ERS_LOG("CORBA TIMEOUT while trying to start " << this->app_name() << ": " << ex);
        }
        catch(CORBA::TRANSIENT &ex) {
            if(ex.minor() != omni::TRANSIENT_CallTimedout) {
                throw;
            }

            ERS_LOG("CORBA TRANSIENT_CallTimedout while trying to start " << this->app_name() << ": " << ex);
        }

        ERS_DEBUG(3, "Started process with handle " << str_handle);
    }
    catch(pmgpriv::start_ex& ex) {
        ERS_DEBUG(0, "Exception while trying to start " << this->app_name() << ": " << std::string(ex.error_string));
        throw pmg::Failed_Start(ERS_HERE, this->app_name(), m_host->full_name(), std::string(ex.error_string));
    }
    catch(daq::pmg::No_PMG_Server& ex) {
        ERS_DEBUG(0, "Exception while trying to start " << this->app_name() << ": " << ex.message());
        throw ex;
    }
    catch(daq::pmg::Bad_PMG_Server& ex) {
        ERS_DEBUG(0, "Exception while trying to start " << this->app_name() << ": " << ex.message());
        throw ex;
    }
    catch(daq::pmg::Exception& ex) {
        ERS_DEBUG(0, "Exception while trying to start " << this->app_name() << ": " << ex.message());
        throw pmg::Failed_Start(ERS_HERE, this->app_name(), m_host->full_name(), ex.message(), ex);
    }
    catch(daq::ipc::Exception& ex) {
        ERS_DEBUG(0, "Exception while trying to start " << this->app_name() << ": " << ex.message());
        throw pmg::No_PMG_Server(ERS_HERE, m_host->full_name(), ex.message(), ex);
    }
    catch(CORBA::TRANSIENT& ex) { // failed in remote call,
        ERS_DEBUG(0, "CORBA exception while trying to start " << this->app_name() << ": " << ex._name());
        throw pmg::No_PMG_Server(ERS_HERE,
                                 "No PMG server on host " + m_host->full_name(),
                                 m_host->full_name(),
                                 ex._name());
    }
    catch(CORBA::Exception& ex) { // failed in remote call,
        ERS_DEBUG(0, "CORBA exception while trying to start " << this->app_name() << ": " << ex._name());
        throw pmg::Bad_PMG_Server(ERS_HERE, m_host->full_name(), ex._name());
    }

    return process_ptr;
} // start

const std::string pmg::ProcessDescriptionImpl::toString() const {
    std::ostringstream stream;
    //stream << *this;
    printTo(stream);
    return stream.str();
} // toString

const std::string pmg::ProcessDescriptionImpl::app_name() const {
    return m_app_name;
}

void pmg::ProcessDescriptionImpl::printTo(std::ostream &stream, const bool prettyPrint) const {
    if(prettyPrint) {
        print(stream, "\n", "\t");
    } else {
        print(stream, "", "");
    }
}

void pmg::ProcessDescriptionImpl::print(std::ostream &stream,
                                        const std::string& line_sep,
                                        const std::string& line_indent) const
{

    std::string two_indent = line_indent + line_indent;
    stream << "ProcessDescription [" << line_sep;
    stream << line_indent << "Username: \"" << m_login_name << "\"; " << line_sep;
    stream << line_indent << "Host: \"" << (*m_host) << "\"; " << line_sep;
    stream << line_indent << "Application Name: \"" << m_app_name << "\"; " << line_sep;
    stream << line_indent << "Partition: \"" << m_partition << "\"; " << line_sep;
    stream << line_indent << "Executable List: \"" << m_exec_list << "\"; " << line_sep;
    stream << line_indent << "Working Dir: \"" << m_wd << "\"; " << line_sep;

    std::vector<std::string>::const_iterator arg;

    // Command line arguments for Start
    stream << line_indent << "Start Arguments: [" << line_sep;
    for(arg = m_start_args.begin(); arg != m_start_args.end();) {
        stream << two_indent << "\"" << (*arg) << "\"";
        if(++arg != m_start_args.end())
            stream << ", ";
        stream << line_sep;
    }
    stream << line_indent << "]; " << line_sep;

    // Environment
    std::map<std::string, std::string>::const_iterator env_iter;
    stream << line_indent << "Environment: [" << line_sep;
    for(env_iter = m_env.begin(); env_iter != m_env.end();) {
        stream << two_indent << "[" << env_iter->first << ", \"" << env_iter->second << "\"]";
        if(++env_iter != m_env.end())
            stream << ", ";
        stream << line_sep;
    }
    stream << line_indent << "]; " << line_sep;

    stream << line_indent << "Log Root: \"" << m_log_path << "\"; " << line_sep;
    stream << line_indent << "Input: \"" << m_input_dev << "\"; " << line_sep;
    stream << line_indent << "Init Timeout: " << m_init_time_out << "; " << line_sep;
    stream << line_indent << "Auto Kill Timeout: " << m_auto_kill_time_out << "; " << line_sep;

    bool use_RM = false;
    if(m_rm_swobject.size() > 0)
        use_RM = true;
    stream << line_indent << "Use Resource Manager: " << use_RM << "; " << line_sep;

    stream << line_indent << "Write Logs: " << m_null_logs << "; " << line_sep;
    stream << "]" << line_sep;

} // printTo

std::ostream& operator<<(std::ostream& stream, const pmg::ProcessDescriptionImpl& description) {
    description.printTo(stream, true);
    return stream;
} // operator<<
