#include <iostream>
#include <vector>
#include <pwd.h>
#include <sys/types.h>

#include <ipc/core.h>
#include <system/Host.h>

#include <pmgpriv/pmgpriv.hh>

#include <boost/algorithm/minmax.hpp>
#include <boost/algorithm/minmax_element.hpp>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

#include "ProcessManager/defs.h"
#include "ProcessManager/Singleton.h"
#include "ProcessManager/Utils.h"

namespace po = boost::program_options;
using boost::posix_time::ptime;
using boost::posix_time::from_time_t;
using boost::posix_time::to_simple_string;
using boost::date_time::c_local_adjustor;
using namespace daq;
using namespace pmg;

void printProcessTable(const std::vector<std::string>& header, const std::vector<std::vector<std::string> >& content) {
	const unsigned int columns(header.size());

	// Vector containing the column sizes
	std::vector<std::vector<unsigned int> > columnSizes(columns);

	// Add the header label sizes
	for(unsigned int i = 0; i < columns; ++i) {
		columnSizes[i].push_back(header[i].size());
	}

	// Add the column content sizes
	const std::vector<std::vector<std::string> >::const_iterator contentStartIt = content.begin();
	const std::vector<std::vector<std::string> >::const_iterator contentStopIt = content.end();
	std::vector<std::vector<std::string> >::const_iterator it;
	for(it = contentStartIt; it != contentStopIt; ++it) {
		for(unsigned int i = 0; i < columns; ++i) {
			columnSizes[i].push_back((*it)[i].size());
		}
	}

	// Compute column sizes
	std::vector<unsigned int> maxValues(columns);
	for(unsigned int i = 0; i < columns; ++i) {
		maxValues[i] = *(boost::minmax_element(columnSizes[i].begin(), columnSizes[i].end())).second;
	}

	// Print headers now
	for(unsigned int i = 0; i < columns; ++i) {
		std::cout << std::setw(maxValues[i] + 3) << std::left << std::setfill(' ') << header[i];
	}
	std::cout << std::endl;

	// Print content now
	for(it = contentStartIt; it != contentStopIt; ++it) {
		for(unsigned int i = 0; i < columns; ++i) {
			std::cout << std::setw(maxValues[i] + 3) << std::left << std::setfill(' ') << (*it)[i];
		}
		std::cout << std::endl;
	}
}

int main(int argc, char** argv) {

	try {

		IPCCore::init(argc, argv);

		std::string hostName;
		bool printTime = false;

		try {
			po::options_description desc("Lists all the process running on a host");

			desc.add_options()("Host,H", po::value<std::string>(&hostName), "Host name")("startTime,v",
			                                                                             po::bool_switch(&printTime),
			                                                                             "Whether to print the processes start time (default is FALSE)")("help,h",
			                                                                                                                                             "Print help message");

			po::variables_map vm;
			po::store(po::parse_command_line(argc, argv, desc), vm);
			po::notify(vm);

			if(vm.count("help")) {
				std::cout << desc << std::endl;
				return EXIT_SUCCESS;
			}

			if(!vm.count("Host")) {
				ERS_LOG("ERROR: please specify the host names");
				std::cout << desc << std::endl;
				return EXIT_FAILURE;
			}
		}
		catch(std::exception& ex) {
			ERS_LOG("Bad command line options: " << ex.what());
			return EXIT_FAILURE;
		}

		System::Host h(hostName);
		std::string hostFullName = h.full_name();

		Singleton::init();
		Singleton* pSing = Singleton::instance();

		std::string agentName = ProcessManagerServerPrefix + hostFullName;
		pmgpriv::SERVER_var server = pSing->get_server(agentName);

		std::string procList;
		pmgpriv::proc_info_list_var procMap = 0;

		try {
			std::cout << "Asking agent " << agentName << " about all the running processes..." << std::endl;

			if(printTime == false) {
				CORBA::String_var allProcs = server->all_processes();
				procList = std::string(allProcs);
			} else {
				server->all_processes_info(procMap);
			}
		}
		catch(pmgpriv::all_processes_ex &ex) {
			ERS_LOG("Agent " << agentName << " failure: " << ex.error_string);
			return (EXIT_FAILURE);
		}
		catch(CORBA::Exception &ex) {
			ERS_LOG("Communication error while contacting agent " << agentName << ": " << ex._name());
			return (EXIT_FAILURE);
		}

		// Print results
		std::vector<std::string> header;
		std::vector<std::vector<std::string> > contents;

		if(printTime == false) {
			header.push_back("APPLICATION");
			header.push_back("PARTITION");
			header.push_back("HANDLE");

			std::vector<std::string> handleVector;
			daq::pmg::utils::tokenize(procList, pmg::ProcessManagerSeparator, handleVector);
			contents.resize(handleVector.size());

			unsigned int headerSize = header.size();
			for(unsigned int i = 0; i < contents.size(); ++i) {
				const std::string& handle(handleVector[i]);
				try {
					Handle h(handle);
					contents[i].push_back(h.applicationName());
					contents[i].push_back(h.partitionName());
					contents[i].push_back(handle);
				}
				catch(pmg::Invalid_Handle& ex) {
					contents[i].resize(headerSize);
					ers::error(ex);
				}
			}
		} else {
			header.push_back("APPLICATION");
			header.push_back("PARTITION");
			header.push_back("START TIME");
			header.push_back("HANDLE");

			contents.resize(procMap->length());

			unsigned int headerSize(header.size());
			for(unsigned int i = 0; i < procMap->length(); ++i) {
				const std::string handle(procMap[i].app_handle);
				try {
					Handle h(handle);
					contents[i].push_back(h.applicationName());
					contents[i].push_back(h.partitionName());

					const pmgpub::ProcessStatusInfo& procInfo(procMap[i].proc_info);
					const ptime t(c_local_adjustor<ptime>::utc_to_local(from_time_t(procInfo.start_time)));
					contents[i].push_back(to_simple_string(t));

					contents[i].push_back(handle);
				}
				catch(pmg::Invalid_Handle& ex) {
					contents[i].resize(headerSize);
					ers::error(ex);
				}
			}
		}

		if(contents.size() == 0) {
			std::cout << "No process found" << std::endl;
		} else {
			printProcessTable(header, contents);
		}
	}
	catch(ers::Issue &ex) {
		ers::error(ex);
		return (EXIT_FAILURE);
	}

	return (EXIT_SUCCESS);

}
