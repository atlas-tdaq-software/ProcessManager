#include <ers/ers.h>

#include <boost/program_options.hpp>
#include <iostream>

#include "ProcessManager/Manifest.h"

namespace po = boost::program_options;
using namespace daq::pmg;

int main(int argc, char** argv) {

  std::string manifestFile;

  try {
    po::options_description desc("List the contents of the specified manifest file");
    desc.add_options()
      ("manifest,M", po::value<std::string>(&manifestFile), "The manifest file")
      ("help,h", "Print help message")
      ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);    

    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS; 
    }
    
    if(!vm.count("manifest")) {
      ERS_LOG("ERROR: please specify the manifest file name");
      std::cout << desc << std::endl;
      return EXIT_FAILURE;
    }
  }
  catch(std::exception& ex) {
    ERS_LOG("Bad command line options: " << ex.what());
    return EXIT_FAILURE; 
  }
 
  try {
    Manifest* pManifest = new Manifest(manifestFile, true, false);
    pManifest->map();
    ERS_LOG("Dumping manifest " << manifestFile << ":\n" << *pManifest);
    pManifest->unmap();
    delete pManifest;
  }
  catch(ers::Issue &ex) {
    ers::error(ex);
  }

}
