package pmgClient;


/**
 * Thrown when the given PMG server was not found in the initial partition lookup
 *
 */
@SuppressWarnings("serial")
public class NoSuchPmgServerException extends PmgClientException {
	
	NoSuchPmgServerException(final String message, final Exception cause) {
		super(message, cause);
	}
}
