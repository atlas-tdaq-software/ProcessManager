package pmgClient;


/**
 * Thrown when server exceptions happen and the actual IDL server exception will be set as a cause for this exception
 *
 */
@SuppressWarnings("serial")
public class ServerException extends PmgClientException {	

	ServerException(final Exception cause) {
		super(cause);
	}

	ServerException(final String message, final Exception cause) {
		super(message, cause);
	}
}
