package pmgClient;


/**
 * Thrown by the PmgClient constructor in case of an error with the port configuration file
 */
@SuppressWarnings("serial")
public class ConfigurationException extends PmgClientException {

	ConfigurationException(final String message) {
		super(message);
	}


	ConfigurationException(final String message, final Exception cause) {
		super(message, cause);
	}

}
