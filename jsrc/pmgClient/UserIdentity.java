package pmgClient;

interface UserIdentity {
    public String getUserName();
    public String getAuthToken();
}
