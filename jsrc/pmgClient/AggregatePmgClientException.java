package pmgClient;

import java.util.LinkedList;

/**
 * An Exception class containing a List with possibly multiple {@link PmgClientException} 
 * that is thrown from methods that may need to suppress a number of Exceptions. 
 * <p>
 *  The message of this Exception will only contain a general description regarding the exceptions contained in the aforementioned List.
 * <p>
 *  The exception can be retrieved from the List that is return by the getPmgClientExceptionList method
 * 
 */
@SuppressWarnings("serial")
public class AggregatePmgClientException extends PmgClientException {
	
	private final LinkedList<PmgClientException> exceptionsList = new LinkedList<PmgClientException>();
	

	AggregatePmgClientException(final String message) {
		super(message + " One should use the getPmgClientExceptionList() method to retrieve a List with all the thrown PmgClientExceptions.");
	}

	/**
	 * Method to be used by the pmg client library, that adds a PmgClientException in the List exceptionsList
	 * @param ex The exception to be added in the exceptionsList List
	 */
	void addPmgClientException(final PmgClientException ex) {
		this.exceptionsList.addLast(ex);
	}
	
	/**
	 * 
	 * @return the exceptionsList List that has the thrown exceptions 
	 */
	public LinkedList<PmgClientException> getPmgClientExceptionList() {
		return this.exceptionsList;
	}
}
