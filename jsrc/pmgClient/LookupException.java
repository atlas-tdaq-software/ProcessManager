package pmgClient;


/**
 * Thrown by the lookup functions of the {@link PmgClient}. The cause will be either an IDL lookup_ex exception or some PmgClientException
 *
 */
@SuppressWarnings("serial")
public class LookupException extends PmgClientException {	

	LookupException(final Exception cause) {
		super(cause);
	}

	LookupException(final String message, final Exception cause) {
		super(message, cause);
	}
}
