package pmgClient;

/**
 * Thrown by the signal methods (signal, stop, killsoft, kill) when Access Manager does not allow the requested action.
 * It is actually encapsulating the IDL signal_ex exception, which is set as a cause for SignalNotAllowedException.
 * Note that signal_ex can also be thrown for other reasons @see {@link Process#handleSignalExceptions}
 *
 */
@SuppressWarnings("serial")
public class SignalNotAllowedException extends PmgClientException {
	
	SignalNotAllowedException(final String message, final Exception cause) {
		super(message, cause);
	}

	SignalNotAllowedException(final String message) {
		super(message);
	}

	SignalNotAllowedException(final Exception cause) {
		super(cause);
	}
}
