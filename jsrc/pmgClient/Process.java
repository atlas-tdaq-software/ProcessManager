package pmgClient;

import java.util.concurrent.atomic.AtomicReference;

import pmgpriv.SERVER;
import pmgpriv.SignalException;
import pmgpriv.get_info_ex;
import pmgpriv.procFiles_ex;
import pmgpriv.signal_ex;
import pmgpub.ProcessStatusInfo;
import pmgpriv.FileHolder;

/**
 * Instances of this class are representing the actual processes.
 * <p>
 * Methods of this class are used to 'interact' with with the process. 
 */
public final class Process {
	private final Handle handle;
	private final PmgClient pmgClient;
	private ProcessStatusInfo processStatusInfo;
	private final AtomicReference<Callback> callback = new AtomicReference<Callback>();
	private final static int MAX_MSG_SIZE = 1024*1024*10;		// 10 MByte
	
	/**
	 * Constructor of Process objects. To be used from the library and NOT directly from user code
	 * 
	 * @param handle the handle of the process that was returned from the server.
	 * @param procStatusInfo Status information about the process.
	 * @param pmgClient a reference to the PmgClient used to launch the process.
	 */
	Process(final Handle handle, final ProcessStatusInfo procStatusInfo, final PmgClient pmgClient){
		this.handle = handle;
		this.pmgClient = pmgClient;
		this.processStatusInfo = procStatusInfo;
	}

	
	/**
	 * Getter for the Process status info of the process, which includes all the information about the process
	 * 
	 * @return a ProcessStatusInfo object with all the information
	 */
	public synchronized ProcessStatusInfo getProcessStatusInfo(){
		return this.processStatusInfo;
	}
	
	
	/**
	 * Getter for the Handle object of the process
	 * 
	 * @return the Handle object of the process
	 */
	public Handle getHandle() {
		return this.handle;
	}
	
	
	/**
	 * Links a given Callback object with the process.
	 *  When a callback is received from the server the {@link Callback#callbackFunction(Process)}
	 *   method of the Callback object will be called. 
	 *   <p>The Process must not be linked with some other callback.
	 *   If it is, client code should first use {@link Process#unlink()} or else link will fail and return false.
	 * 
	 * @param cb an object implementing the Callback interface
	 * @return true if linking was successful or false if this Process was already linked.
	 */
	public boolean link(final Callback cb) {	
		return this.callback.compareAndSet(null, cb);
	}
	
	
	/**
	 * Unlinks the current Process from the linked Callback (if any).
	 */
	public void unlink() {
		this.callback.set(null);
	}
	
	
	/**
	 * Checks whether this Process is linked with some Callback or not.
	 * 
	 * @return true if Process is linked, false otherwise.
	 */
	public boolean isLinked() {
		return this.callback.get() != null;
	}
	
	
	/**
	 * Sends the given signal to the process.
	 * 
	 * @param signum the signal to be sent.
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws SignalNotAllowedException if Access Manager does not allow the requested action.
	 * @throws ProcessAlreadyExitedException if the process has already exited.
	 * @throws CorbaException if there was a CORBA layer exception. The CORBA exception will be the cause of this one.
	 * @throws ServerException if signaling failed for some unknown reason or there was some other server exception.
	 * @throws BadUserException Cannot get user identity
	 */
	public void signal(final int signum) throws NoSuchPmgServerException, SignalNotAllowedException,
			ProcessAlreadyExitedException, CorbaException, ServerException, BadUserException {
		try {	// throws NoSuchPmgServerException
			this.getServer().signal(this.handle.toString(), signum, UserIdentityFactory.getUserIdentity().getAuthToken(), this.pmgClient.getLocalhost());
		} catch (signal_ex e) {
			handleSignalExceptions(e);	// throws various Exceptions
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during stop call", e);
		}
	}
	

	/**
	 * Terminates the process by sending SIGTERM.
	 * 
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws SignalNotAllowedException if Access Manager does not allow the requested action.
	 * @throws ProcessAlreadyExitedException if the process has already exited.
	 * @throws CorbaException if there was a CORBA layer exception. The CORBA exception will be the cause of this one.
	 * @throws ServerException if signaling failed for some unknown reason or there was some other server exception.
	 * @throws BadUserException Cannot get user identity
	 */
	public void stop() throws NoSuchPmgServerException, SignalNotAllowedException,
			ProcessAlreadyExitedException, CorbaException, ServerException, BadUserException {
		try {	// throws NoSuchPmgServerException
			this.getServer().stop(this.handle.toString(), UserIdentityFactory.getUserIdentity().getAuthToken(), this.pmgClient.getLocalhost());
		} catch (signal_ex e) {
			handleSignalExceptions(e);	// throws various Exceptions
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during stop call", e);
		}
	}
	

	/**
	 * Kills the process via SIGKILL.
	 * 
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws SignalNotAllowedException if Access Manager does not allow the requested action.
	 * @throws ProcessAlreadyExitedException if the process has already exited.
	 * @throws CorbaException if there was a CORBA layer exception. The CORBA exception will be the cause of this one.
	 * @throws ServerException if signaling failed for some unknown reason or there was some other server exception.
	 * @throws BadUserException Cannot get user identity
	 */
	public void kill() throws NoSuchPmgServerException, SignalNotAllowedException,
			ProcessAlreadyExitedException, CorbaException, ServerException, BadUserException {
		try {	// throws NoSuchPmgServerException
			this.getServer().kill(this.handle.toString(), UserIdentityFactory.getUserIdentity().getAuthToken(), this.pmgClient.getLocalhost());
		} catch (signal_ex e) {
			handleSignalExceptions(e);	// throws various Exceptions
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during kill call", e);
		}
	}
	

	/**
	 * Softly kills the process.
	 * Current implementation at the server-side is as follows:
	 *  SIGTERM signal is send. If the process is not exited within the given amount of time, then SIGKILL signal is send.
	 * 
	 * @param timeout time in seconds of how long the server should wait before sending SIGKILL
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws SignalNotAllowedException if Access Manager does not allow the requested action.
	 * @throws ProcessAlreadyExitedException if the process has already exited.
	 * @throws CorbaException if there was a CORBA layer exception. The CORBA exception will be the cause of this one.
	 * @throws ServerException if signaling failed for some unknown reason or there was some other server exception.
	 * @throws BadUserException Cannot get user identity
	 */
	public void killSoft(final int timeout) throws NoSuchPmgServerException, SignalNotAllowedException,
			ProcessAlreadyExitedException, CorbaException, ServerException, BadUserException {
		try {	// throws NoSuchPmgServerException
			this.getServer().kill_soft(this.handle.toString(), timeout, UserIdentityFactory.getUserIdentity().getAuthToken(), this.pmgClient.getLocalhost());
		} catch (signal_ex e) {
			handleSignalExceptions(e);	// throws various Exceptions
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during killSoft call", e);
		}
	}
	
	
	/**
	 * Returns the err stream of the process as a String.
	 * 
	 * @return A String object corresponding to the stder stream output
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws NonExistingProcessFile  if the process produced nothing in the out stream or the file could not be retrieved
	 * @throws CorbaException if there was a CORBA layer exception. The CORBA exception will be the cause of this one.
	 */
	public String errFileStr() throws NoSuchPmgServerException, NonExistingProcessFile, CorbaException {
		FileHolder fileContent = new FileHolder();
		
		try {
			this.getServer().procFiles(this.getProcessStatusInfo().start_info.streams.err_path, MAX_MSG_SIZE, fileContent);
		} catch (procFiles_ex e) {
			throw new NonExistingProcessFile("ErrFile of process " + this.handle.toString() + " does not exist or could not be retrieved" , e );
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during procFiles call for stderr", e);
		}
		
		byte[] errBytes = fileContent.value;
		return new String(errBytes, 0, errBytes.length-1); // convert to String and also remove the trailing null
	}
	
	
	/**
	 * Return the out stream of the process as a String.
	 * 
	 * @return A String object corresponding to the stdout stream output
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws NonExistingProcessFile if the process produced nothing in the out stream or the file could not be retrieved
	 * @throws CorbaException if there was a CORBA layer exception. The CORBA exception will be the cause of this one.
	 */
	public String outFileStr() throws NoSuchPmgServerException, NonExistingProcessFile, CorbaException {
		FileHolder fileContent = new FileHolder();
		
		try {
			this.getServer().procFiles(this.getProcessStatusInfo().start_info.streams.out_path, MAX_MSG_SIZE, fileContent);
		} catch (procFiles_ex e) {
			throw new NonExistingProcessFile("OutFile of process " + this.handle.toString() + " does not exist or could not be retrieved" , e );
		} catch (org.omg.CORBA.SystemException e) {
			throw new CorbaException("CORBA Exception during procFiles call for stdout", e);
		}
		
		byte[] outBytes = fileContent.value;
		return new String(outBytes, 0, outBytes.length-1); // convert to String and also remove the trailing null
	}


    /**
     * Returns the err stream of the process.
     * One can get the byte[] of with the stream by using the value attribute of the FileHolder.
     * Notice that this array will be null-terminated and users will probably need to trim that last byte.
     *
     * @return A FileHolder object corresponding to the err stream
     * @throws NoSuchPmgServerException if the server of the Process is not found.
     * @throws NonExistingProcessFile if the process produced nothing in the out stream, the file could not be retrieved
     * 			or there was a CORBA Exception.
     */
    public FileHolder errFile() throws NoSuchPmgServerException, NonExistingProcessFile {
        FileHolder fileContent = new FileHolder();

        try {
        	this.getServer().procFiles(this.getProcessStatusInfo().start_info.streams.err_path, MAX_MSG_SIZE, fileContent);
        } catch (procFiles_ex | org.omg.CORBA.SystemException e) {
        	throw new NonExistingProcessFile("ErrFile of process " + this.handle.toString()
        			+ " does not exist, could not be retrieved or there was a CORBA Exception" , e );
        }
        return fileContent;
    }
   
   
    /**
     * Return the out stream of the process.
     * One can get the byte[] of with the stream by using the value attribute of the FileHolder.
     * Notice that this array will be null-terminated and users will probably need to trim that last byte.
     *
     * @return A FileHolder object corresponding to the out stream
     * @throws NoSuchPmgServerException if the server of the Process is not found.
     * @throws NonExistingProcessFile if the process produced nothing in the out stream, the file could not be retrieved
     * 			or there was a CORBA Exception.
     */
    public FileHolder outFile() throws NoSuchPmgServerException, NonExistingProcessFile {
        FileHolder fileContent = new FileHolder();

        try {
        	this.getServer().procFiles(this.getProcessStatusInfo().start_info.streams.out_path, MAX_MSG_SIZE, fileContent);
        } catch (procFiles_ex | org.omg.CORBA.SystemException e) {
        	throw new NonExistingProcessFile("OutFile of process " + this.handle.toString()
        			+ " does not exist, could not be retrieved or there was a CORBA Exception" , e );
        }
        return fileContent;
    }
    
	
	
	/**
	 * Returns a String with most of the information regarding the process; will <b>not</b> return a Handle
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(768);
		builder.append("Process [\n handle="); builder.append(this.handle);
		builder.append(",\n callback="); builder.append(this.callback.get());
		builder.append(",\n pmgClient= PmgClient [localhost=");
		builder.append(this.pmgClient.getLocalhost());
		builder.append(", corbaRef=");
		builder.append(this.pmgClient.getCorbaRef());
		builder.append("]");
		builder.append(",\n processStatusInfo= ProcessStatusInfo[ state= ProcessState[");
		final ProcessStatusInfo info = this.getProcessStatusInfo();
		builder.append(info.state.toString());
		builder.append("], info= ProcessInfo[ process_id="); builder.append(info.info.process_id);
		builder.append(", signal_value="); builder.append(info.info.signal_value);
		builder.append(", exit_value="); builder.append(info.info.exit_value);
		builder.append("] ]\n]");
		return builder.toString();
	}

	
	
	
	/***   package-private and private methods   ****/

	
	
	/**
	 * Internal method used by the library to set the Process status information
	 * 
	 * @param info the new status information
	 */
	synchronized void setProcessStatusInfo(final ProcessStatusInfo info ){	
		this.processStatusInfo = info;
	}


	/**
	 * The internal method that actually calls the callbackFunction of the Callback if this Process is linked.
	 * Also @see {@link Callback}
	 * 
	 */
	void callback() {
		ers.Logger.debug(2, "Executing callback of proccess : " + this.handle.toString());
		final Callback cb = this.callback.get();
		if(cb != null ) {
			cb.callbackFunction(this);
		}
	}

	/**
	 * Internal function to facilitate the handling of exceptions from the signaling methods.
	 * 
	 * @param ex the exception to be handled.
	 * @throws SignalNotAllowedException if Access Manager does not allow the requested action.
	 * @throws ProcessAlreadyExitedException if the process has already exited.
	 * @throws NoSuchPmgServerException if the server of the Process is not found.
	 * @throws ServerException if signaling failed for some unknown reason or there was some other server exception.
	 */
	private void handleSignalExceptions(signal_ex ex) throws SignalNotAllowedException, ProcessAlreadyExitedException,
			NoSuchPmgServerException, ServerException {

		switch (ex.reason.value()) {
		case SignalException._APPLICATION_NOTFOUND_INVALID:
		case SignalException._MANIFEST_UNMAPPED:
		case SignalException._LAUNCHER_NOT_RUNNING:
			
			pmgpub.ProcessState currState = this.getProcessStatusInfo().state;
			
			synchronized (this) {	
				if( ! PmgClient.isEndState(this) ) {
					try {
						SERVER server = getServer();		// throws NoSuchPmgServerException
						pmgpub.ProcessStatusInfoHolder statusInfoHolder = new pmgpub.ProcessStatusInfoHolder();
						boolean success  = server.get_info(this.handle.toString(), statusInfoHolder);
						
						// Just check that in the meanwhile, the information has not been updated
						if(currState == this.getProcessStatusInfo().state) {
							if(success) {
								this.setProcessStatusInfo(statusInfoHolder.value);
							} else {
								this.getProcessStatusInfo().state = pmgpub.ProcessState.EXITED;
							}
						}
					} catch (get_info_ex e) {
						throw new ServerException(e);
					}
				}
			}
				
			throw new ProcessAlreadyExitedException(ex.error_string, ex);

		case SignalException._NOT_ALLOWED_BY_AM:
			throw new SignalNotAllowedException(ex.error_string, ex);

		case SignalException._FIFO_ERROR:
		case SignalException._UNEXPECTED:
		case SignalException._UNKNOWN:
			throw new ServerException(ex.error_string, ex);

		default:
			throw new ServerException(ex.error_string, ex);
		}
	}
	
	
	/**
	 * Internal method to facilitate the acquisition of the SERVER in which the process is running
	 * 
	 * @return the SERVER in which the process is running.
	 * @throws NoSuchPmgServerException if the server does not exist.
	 */
	private SERVER getServer() throws NoSuchPmgServerException {
		return this.pmgClient.pmgServerFromName(this.handle.getServer());
	}	
}
