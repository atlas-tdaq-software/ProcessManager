package pmgClient;

/**
 * Thrown by the signaling methods (signal, stop, killsoft, kill), when the process to be signaled has already exited.
 *
 */
@SuppressWarnings("serial")
public class ProcessAlreadyExitedException extends PmgClientException {	

	ProcessAlreadyExitedException(final String message, final Exception cause) {
		super(message, cause);
	}
}
