/**
@author lpapaevg (Lykourgos Papaevgeniou)

Java client library for communicating with the ProcessManager server through CORBA.
<p>
Other libraries needed by this package are: ipc.jar, external.jar and ers.jar

<p>
To launch a new process client codes needs to :
<ol>
<li><b>Create a PmgClient instance.</b> This is the representation of the pmg client and there should be only one such instance</li>
<li><b>Create a ProcessDescription using ProcessDescription.ProcessDescriptionBuilder.</b> See JavaDoc of that class and example.</li>
<li><b>Invoke the start method on the ProcessDescription object that was created.</b> This will return a Process Object.</li>
</ol>

Every other action for the created process is then performed by invoking methods of the Process Object created.
<p>
Every other action with the pmgClient in general, will be done by invoking methods of the PmgClient instance.<br><br>

<p>
Example (without excpetions handling): <br><br>
<pre>
final class MyCallback implements Callback {

	@Override
	public void callbackFunction(pmg.client.Process linkedProcess) {	// this methods will be invoked upon callback receipt
		System.out.print("-----  Callback from Process :::::  " + linkedProcess.getHandle() + "  -------");
		System.out.println("State : " + linkedProcess.getProcessStatusInfo().state.toString());
	}
}


public class Test {

	public static void main( String [] arg) throws Exception {		<b>// One SHOULD properly handle exceptions!</b>

		PmgClient client = new PmgClient();
		
		HashMap<String, String> env = new HashMap<String, String>();  
		env.put("TDAQ_SOMETHING_ENV_VAR", "0");
		env.put("TDAQ_SOMETHING_ELSE", "2");
		
		ArrayList<String> startArgs = new ArrayList<String>();
		startArgs.add("some_start_argument");

		ProcessDescription descr51 = new ProcessDescription.
				ProcessDescriptionBuilder(client, "pc-tbed-pub-05.cern.ch", "lyk",  "MyAppName_51_", 
				"/afs/cern.ch/user/l/lpapaevg/public/lyk1.sh", env)		// arguments following this line can be ommitted and default values will be used 
					.wd("/afs/cern.ch/user/l/lpapaevg/public/")
					.startArgs(startArgs)
					.logPath("/afs/cern.ch/user/l/lpapaevg/public/")
					.inputDev("/dev/null")
					.rmSwobject("")
					.initTimeout(0)
					.autoKillTimeout(0)
					.nullLogs(false)
					.build();
					
		MyCallback myCallback = new MyCallback();
		
		pmg.client.Process proc51 = descr51.start(myCallback);
		
		// More code here ...
		
	}
}
</pre>

*/
package pmgClient;