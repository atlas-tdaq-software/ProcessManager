package pmgClient;

/**
 * Thrown when when a process ( given Handle) was not found. 
 *
 */
@SuppressWarnings("serial")
public class ProcessNotFoundException extends PmgClientException {
	
	ProcessNotFoundException(final String message){
		super(message);
	}

	ProcessNotFoundException(final String message, final Exception cause) {
		super(message, cause);
	}
}
