package test;

import java.util.ArrayList;
import java.util.HashMap;

import pmgClient.*;
import pmgpub.ProcessState;





/**
 * 
 * For this example you need the following setup:
 *	- 2 PMG servers running on "pc-tbed-pub-06.cern.ch", "pc-tbed-pub-04.cern.ch" (or on other according to host1,host2 variables)
 *	- 2 partitions with names: "lyk", "lyk2"
 *	- Change the executablePath1 and executablePath2 to whatever executables you want (preferably ones that will run forever unless killed)
 *	- Change the testWd variable to your preferred directory
 */
public class Example {
	
	final class ExampleCallback implements Callback {

		@Override
		public void callbackFunction(pmgClient.Process linkedProcess) {
			System.out.print("-----  Callback from Process :::::  " + linkedProcess.getHandle() + "  -------");
			System.out.println("State : " + linkedProcess.getProcessStatusInfo().state.toString());
			if( linkedProcess.getProcessStatusInfo().state == ProcessState.SIGNALED) {
				try {
					System.out.println(" out:" + linkedProcess.outFileStr() + "end_of_out");
					System.err.println(" err:" + linkedProcess.errFileStr()  +"end_of_err");
				} catch (NoSuchPmgServerException e) {
					System.err.println(e.getMessage());
				} catch (NonExistingProcessFile e) {
					System.err.println(e.getMessage());
				} catch (CorbaException e) {
					System.err.println(e.getMessage());
				}
			}
		}
	}

	
	public static void main( String [] arg) throws InterruptedException {
	
		PmgClient client;
		try {
			client = new PmgClient();
		} catch (ConfigurationException e) {
			System.err.println("CorbaException during testing start");
			e.printStackTrace();
			throw new RuntimeException();
		}
		
		final String host1 = "pc-tbed-pub-06.cern.ch";
		final String host2 = "pc-tbed-pub-04.cern.ch";
		final String executablePath1 = "/afs/cern.ch/user/l/lpapaevg/public/lykTest/lyk1.sh";
		final String executablePath2 = "/afs/cern.ch/user/l/lpapaevg/public/lykTest/lyk2.sh";
		final String testWd = "/tmp/lyk";
		final String testlogPath = testWd;

		
		final HashMap<String, String> env = new HashMap<String, String>();  
		env.put("TDAQ_SOMETHING_ENV_VAR", "0");
		env.put("TDAQ_SOMETHING_ELSE", "2");
		final ArrayList<String> startArgs = new ArrayList<String>();
		startArgs.add("3");//startArgs.add("11");

		final ProcessDescription descr51 = new ProcessDescription
				.ProcessDescriptionBuilder(client, host1, "lyk",  "MyAppName_host1_app1_", "/badpath/lyk.sh:"+executablePath1, env)
					.wd(testWd)
					.startArgs(startArgs)
					.logPath(testlogPath)
					.inputDev("/dev/null")
					.rmSwobject("")
					.initTimeout(0)
					.autoKillTimeout(0)
					.nullLogs(false)
					.build();
		
		final ProcessDescription descr52 = new ProcessDescription.ProcessDescriptionBuilder(
				client, host1, "lyk2",  "MyAppName_host1_app2_", executablePath2, env )
					.logPath(testlogPath)
					.build();
		
		final ProcessDescription descr41 = new ProcessDescription.ProcessDescriptionBuilder(
				client, host2, "lyk",  "MyAppName_host2_app1_", executablePath1, env )
					.logPath(testlogPath)
					.build();
		
		final ProcessDescription descr42 = new ProcessDescription.ProcessDescriptionBuilder(
				client, host2, "lyk2",  "MyAppName_host2_app2_", executablePath2, env )
					.logPath(testlogPath)
					.build();

		
		
		  
		final ExampleCallback myCallback = (new Example()).new ExampleCallback();
//		
//		try {
//			System.out.println("Host 05 before: " + client.askRunningProcesses("pc-tbed-pub-05.cern.ch", "lyk"));
////			System.out.println("Host 04 before: " + client.askRunningProcesses("pc-tbed-pub-04.cern.ch", "lyk") + "\n");	
//		} catch (NoSuchPmgServerException e1) {
//			e1.printStackTrace();
//		} catch (ServerException e) {
//			e.printStackTrace();
//		}
		
		System.out.println("Starting process...");

		pmgClient.Process proc51 = null;
		pmgClient.Process proc52 = null;
		pmgClient.Process proc41 = null;
		pmgClient.Process proc42 = null;
		
		try {
			proc51 = descr51.start(myCallback);
			proc52 = descr52.start(myCallback);
			proc41 = descr41.start(myCallback);
			proc42 = descr42.start(myCallback);
		} catch(BadUserException e) {
		        System.err.println("BadUserException during testing start");
                        e.printStackTrace();
                        throw new RuntimeException();
		} catch (NoSuchPmgServerException e) {
			System.err.println("NoSuchPmgServerException during testing start");
			e.printStackTrace();
			throw new RuntimeException();
		} catch (ServerException e) {
			System.err.println("ServerException during testing start");
			e.printStackTrace();
			throw new RuntimeException();
		} catch (CorbaException e) {
			System.err.println("CorbaException during testing start");
			e.printStackTrace();
			throw new RuntimeException();
		}
		System.out.println("Started!");
		
		
		System.out.println("Sleeping for 5 sec...");
		Thread.sleep(5000);
		
		
//		try {			
//			ArrayList<String> arl = client.runningProcessesForHost("pc-tbed-pub-04.cern.ch");
//			ArrayList<String> arl2 = client.runningProcessesForHost("pc-tbed-pub-05.cern.ch");
//		} catch (NoSuchPmgServerException e) {
//			System.err.println("NoSuchPmgServerException during testing ");
//		} catch (ServerException e) {
//			System.err.println("ServerException during testing ");
//		} catch (CorbaException e) {
//			System.err.println("CorbaException during testing ");
//		}
			
//			System.out.println("ask 05: " + client.askRunningProcesses("pc-tbed-pub-05.cern.ch", "lyk"));
//			System.out.println("ask 04: " + client.askRunningProcesses("pc-tbed-pub-04.cern.ch", "lyk") + "\n");
//			System.out.println("Partition lyk after start: " + client.askRunningProcesses("lyk") + "\n");
//			System.out.println("Partition lyk1 after start: " + client.askRunningProcesses("lyk1") + "\n");

//			try {
//				client.askRunningProcessesMapOfHost("pc-tbed-pub-04.cern.ch");
//				client.askRunningProcessesMapOfHost("pc-tbed-pub-05.cern.ch");
//			} catch (NoSuchPmgServerException e) {
//				System.err.println("NoSuchPmgServerException during testing askRunningProcessesMapOfHost");
//			} catch (ServerException e) {
//				System.err.println("ServerException during testing askRunningProcessesMapOfHost");
//			} catch (CorbaException e) {
//				System.err.println("CorbaException during testing askRunningProcessesMapOfHost");
//			}
			
//			try {
//				List<String> l = new LinkedList<String>();
//				
//				l.add("pc-tbed-pub-05.cern.ch");
//				l.add("pc-tbed-pub-04.cern.ch");
//				
//				Handle h = client.lookup("pc-tbed-pub-05.cern.ch", "MyAppName_51_", "lyk");
//				System.err.println("handle: " + h.toString());
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
			

			
		System.out.println("Stopping...");
		try {
			if (proc51 != null) {
				proc51.killSoft(2);
			}
		} catch (NoSuchPmgServerException | SignalNotAllowedException | ProcessAlreadyExitedException | CorbaException | ServerException | BadUserException e) {
			System.err.println(e.getClassName() + "Exception during testing kill");
		} 
		
		try {
			if (proc52 != null) {
				proc52.killSoft(2);
			}
		} catch (NoSuchPmgServerException | SignalNotAllowedException | ProcessAlreadyExitedException | CorbaException | ServerException | BadUserException e) {
			System.err.println(e.getClassName() + "Exception during testing kill");
		} 
		
		try {
			if (proc41 != null) {
				proc41.killSoft(2);
			}
		} catch (NoSuchPmgServerException | SignalNotAllowedException | ProcessAlreadyExitedException | CorbaException | ServerException | BadUserException e) {
			System.err.println(e.getClassName() + "Exception during testing kill");
		} 
		
		try {
			if (proc42 != null) {
				proc42.killSoft(2);
			}
		} catch (NoSuchPmgServerException | SignalNotAllowedException | ProcessAlreadyExitedException | CorbaException | ServerException | BadUserException e) {
			System.err.println(e.getClassName() + "Exception during testing kill");
		} 
		
		System.out.println("Stopped!");
					
		
		System.out.println("Sleeping for 3 secs and done!");

		Thread.sleep(3000);
			
//			System.out.println("Host 05 after stop: " + client.askRunningProcesses("pc-tbed-pub-05.cern.ch", "lyk"));
//			System.out.println("Host 04 after stop: " + client.askRunningProcesses("pc-tbed-pub-04.cern.ch", "lyk") + "\n");		
//			System.out.println("Partition lyk after stop: " + client.askRunningProcesses("lyk") + "\n");
					
		
//		HashMap <String, pmgpub.ProcessStatusInfo> m = (HashMap <String, pmgpub.ProcessStatusInfo>) client.askRunningProcessesMap("lyk");
//		for( Entry<String, ProcessStatusInfo> e : m.entrySet() ) {
//			
//			client.getProcess(new Handle(e.getKey())).killSoft(2);
//
//		}
				
		System.out.println("----------------------------- End of test -----------------------------");
	}
}
