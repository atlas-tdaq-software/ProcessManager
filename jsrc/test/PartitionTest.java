package test;

import ipc.InvalidPartitionException;
import pmgClient.Callback;
import pmgClient.ConfigurationException;
import pmgClient.CorbaException;
import pmgClient.Handle;
import pmgClient.LookupException;
import pmgClient.PmgClient;
import pmgClient.ProcessNotFoundException;
import pmgClient.ServerException;
import config.Configuration;
import config.NotFoundException;
import config.SystemException;
import dal.Partition;
import rc.RCStateInfoNamed;




/**
 * Run this test with the start_PartitionTest.sh that exists in the bin folder
 * 
 *  If it is run manually, one might get some ers errors. In that case:   export TDAQ_ERS_NO_SIGNAL_HANDLERS=1
 *
 */
public class PartitionTest {
	
	static int cbReceived = 0;
	static int runningProccess = 0;
	static int dbProccess = 0;
	static int linkedProcesses = 0;
	public final static Object SHARED_LOCK = new Object();

	/**
	 * Our callback class for this test
	 *
	 */
	private final class TestCallback implements Callback {
		@Override
		public void callbackFunction(pmgClient.Process linkedProcess) {
			System.out.print("   Callback from Process :  " + linkedProcess.getHandle());
			System.out.println("\t State : " + linkedProcess.getProcessStatusInfo().state.toString());
			synchronized (SHARED_LOCK) {
				cbReceived++;
				System.out.println("Callbacks received up to now : " + cbReceived + " \n");
				SHARED_LOCK.notifyAll();
			}
		}
	}
	
	

	/**
	 * 
	 * @param arg
	 */
	public static void main(String[] argv) {
		
		if( argv.length != 4 ) {
			System.err.println("Wrong usage! Arguments: -d database_file -p partition_name");
			return;
		}
		String db = null;
		String partitionName = null;
		
		for(int i=0; i<argv.length ; i++ ) {
			switch(argv[i]) {
			case "-p" :
				partitionName = argv[++i];
				break;
			case "-d":
				db = argv[++i];
				break;
			default:
				System.err.println("Wrong usage! Arguments: -d database_file -p partition_name");
				return;
			}
		}
		
		System.out.println("\n Starting test for partition: " + partitionName + " of db file: " + db);
		

		//System.setProperty("tdaq.ipc.partition.name", "part_hlt_lpapaevg");
		Configuration config = null;
		try {
			config = new Configuration("oksconfig:" + db);
		} catch (config.SystemException ex) {
			System.err.println("Caught \'config.SystemException\':\n" + ex.getMessage());
			return;
		}
		System.out.println("Got the Configuration object from the file: " + db);

		
		Partition partition = null;
		try {
			partition = dal.Algorithms.get_partition(config, partitionName);
			if (partition != null) {
				if( ! partitionName.equals(partition.UID()) ) {
					throw new IllegalStateException("Given partition name is defferent ffrom the db name");
				}
				System.out.println("\nGot the Partition: " + partitionName + " from the Configuration Object.\n");
				
				PmgClient pmgClient = new PmgClient();
				TestCallback  testCallback = (new PartitionTest()).new TestCallback();
				dal.BaseApplication[] apps = partition.get_all_applications(null, null, null);
			
				
				/*
				 *		Check the status of the RC to make sure it is up and running 
				 */
				final RCStateInfoNamed rc = new RCStateInfoNamed(new ipc.Partition(partitionName), "RunCtrl.RootController" );
				
				System.out.println("\nChecking RC status...");
				boolean infrastructureIsUp = false;
				while ( ! infrastructureIsUp ) {
					try {
						rc.checkout();
					} catch(Exception e) {
						System.out.println("Exception: " + e.toString() + ". Infrastructure is not up. Will retry in 1 secs...");
						try { Thread.sleep(1000); } catch (InterruptedException e1) { continue; }						
						continue;
					}
					infrastructureIsUp = true;
				}
				String state = rc.state;
				
				while(state.equals("NONE") || state.equals("ABSENT")) {
					System.out.println("Got RC state: " + state + ". Will retry in 2 secs...");
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						continue;
					}
					rc.checkout();
					state = rc.state;
				}
				
				System.out.println("\nRC state is " + state + " . Lookup and linking of apps in database:\n");
				
				

				for( dal.BaseApplication app : apps ) {
					String appID = app.UID();
					String host = app.get_host().UID();
					dbProccess++;
					
					try {
						System.out.print("lookup of:   " + appID + "    at:   " + host + "  ... ");
						Handle handle = pmgClient.lookup(appID, partitionName, host);
						if ( handle != null ) {
							pmgClient.Process proc = pmgClient.getProcess( handle );
							proc.link(testCallback);
							linkedProcesses++;
							System.out.println("\t linked (current total linked: " + linkedProcesses + ")");
						} else {
							System.out.println("\t not running (current total inked: " + linkedProcesses + ")");
						}
					} catch (LookupException e) {
						System.err.println("Caught \'PmgClient.LookupException\':\n" + e.getMessage());
						continue;
					} catch (ProcessNotFoundException e) {
						System.err.println("Caught \'PmgClient.ProcessNotFoundException\':\n" + e.getMessage());
						continue;
					}
				}
				System.out.println("\nApplications found in database file: " + dbProccess
						+ " in total! Linked with : " + linkedProcesses + " of them\n");
				
				
//				for( String s : pmgClient.runningProcessesForPartition(partitionName) ) {
//					runningProccess++;
//				}
				runningProccess = pmgClient.runningProcessesForPartition(partitionName).size();
				System.out.println("Running processes: " + runningProccess + " in total!");
				
				
				/*
				 *		Wait for the callbacks 
				 */
				synchronized (SHARED_LOCK) {
					while( linkedProcesses != 0 && cbReceived < linkedProcesses ) {
						System.out.println("Received: " + cbReceived + " callbacks, out of the expected: " + linkedProcesses );
						try {
							SHARED_LOCK.wait(5000);
						} catch (InterruptedException e) {
							continue;
						}
					}
				}
				System.out.println("DONE! Received: " + cbReceived + " callbacks, out of the expected: " + linkedProcesses + "\n");								
			}
		} catch (SystemException ex) {
			System.err.println("Caught \'config.SystemException\':\n" + ex.getMessage());
		} catch (NotFoundException ex) {
			System.err.println("Caught \'config.NotFoundException\':\n" + ex.getMessage());
		} catch (ConfigurationException ex) {
			System.err.println("Caught \'PmgClient.ConfigurationException\':\n" + ex.getMessage());
		} catch (ServerException e) {
			System.err.println("Caught \'PmgClient.ServerException\':\n" + e.getMessage());
		} catch (CorbaException e) {
			System.err.println("Caught \'PmgClient.CorbaException\':\n" + e.getMessage());
		} catch (InvalidPartitionException e) {
			System.err.println("Caught \'PmgClient.InvalidPartitionException\':\n" + e.getMessage());
		} catch (Exception e) {
		        System.err.println("Caught generic exception:\n" + e.getMessage());
		}
	}
}
