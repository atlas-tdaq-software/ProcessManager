package test;

import java.util.ArrayList;
import java.util.HashMap;

import pmgClient.*;



public class TestBig {

	public static int procLaunchedInRound = 0;
	public static int expectedCallbacksInRound = 0;
	public static int callBacksReceivedInRound = 0;
	public static int callBacksMissedInRound = 0;

	public static int totalCallbacksReceived = 0;
	public static int totalCallbacksMissed = 0;
	public static int procLaunchedTotal = 0;
	public static int procFailedToLaunchTotal = 0;

	public final static int CALLBACKS_PER_PROCESS = 2;

	public final static int roundsNum = 100;						// test parameters to be EDITTED
	public final static int proccessToTryPerRound = 20;				// test parameters to be EDITTED
	
	public final static Object sharedLock = new Object();
	
	final class MyCallbackBig implements Callback {

		@Override
		public void callbackFunction(pmgClient.Process linkedProcess) {
//			System.out.print("-----  Callback from Process :::::  " + linkedProcess.getHandle() + "  -------");
//			System.out.println("State : " + linkedProcess.getProcessStatusInfo().state.toString());
			
			synchronized(TestBig.sharedLock) {
				TestBig.callBacksReceivedInRound++;
				TestBig.totalCallbacksReceived++;
				TestBig.sharedLock.notifyAll();
			}
		}
	}
	
	

	public static void main(String[] arg) {
		
		(new TestBig()).test();
		
	}
	
	
	public void test() {

		PmgClient client;
		try {
			client = new PmgClient();
		} catch (ConfigurationException e) {
			System.err.println("CorbaException during testing start");
			e.printStackTrace();
			throw new RuntimeException();
		}

		HashMap<String, String> env = new HashMap<String, String>();
		env.put("TDAQ_SOMETHING", "0");
		env.put("TDAQ_SOMETHING_ELSE", "2");
		ArrayList<String> startArgs = new ArrayList<String>();
		startArgs.add("0");

		ProcessDescription descr51 = new ProcessDescription.ProcessDescriptionBuilder(client, "pc-tbed-pub-05.cern.ch",
				"lyk", "MyAppName_51_", "/bin/sleep", env).wd("/afs/cern.ch/user/l/lpapaevg/public/lykTest/")
				.startArgs(startArgs).logPath("/afs/cern.ch/user/l/lpapaevg/public/lykTest").inputDev("/dev/null")
				.rmSwobject("").initTimeout(0).autoKillTimeout(0).nullLogs(false).build();

		MyCallbackBig myCallbackBig = new MyCallbackBig();
		
		for (int i = 1; i <= roundsNum; i++) {
//			System.out.println("Starting processes...");


			synchronized(sharedLock){
				for (int j = 0; j < proccessToTryPerRound; j++) {
					try {
						descr51.start(myCallbackBig);
						
						procLaunchedInRound++;
						
					} catch(BadUserException e) {
                                                System.err.println("BadUserException during testing start: " + e.getMessage());
                                                procFailedToLaunchTotal++;
                                                continue;					    
					} catch (NoSuchPmgServerException e) {
						System.err.println("NoSuchPmgServerException during testing start: " + e.getMessage());
						procFailedToLaunchTotal++;
						continue;
					} catch (ServerException e) {
						System.err.println("ServerException during testing start: " + e.getMessage());
						procFailedToLaunchTotal++;
						continue;
					} catch (CorbaException e) {
						System.err.println("CorbaException during testing start: " + e.getMessage() 
								+ " " + e.getLocalizedMessage() + " getCause:"
								+ ((e.getCause()!=null)? e.getCause().getMessage() : "no getcause"));
						procFailedToLaunchTotal++;
						continue;
					}
				}
			
			
				while(callBacksReceivedInRound < procLaunchedInRound * CALLBACKS_PER_PROCESS) {
					try {						
						sharedLock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
			
//				System.out.println("All started!");
		
		
//				System.out.println("Comparing number of received callbacks with number of processess run!");
				System.out.print(" Round: " + i);
				
				expectedCallbacksInRound = procLaunchedInRound * CALLBACKS_PER_PROCESS;
				callBacksMissedInRound = expectedCallbacksInRound - callBacksReceivedInRound;
		
				if (callBacksMissedInRound != 0) {
					System.err.println("FAILED! Received: " + callBacksReceivedInRound + " out of: "
							+ expectedCallbacksInRound + " callbacks.\n");
					totalCallbacksMissed += callBacksMissedInRound;
				} else {
//					System.out.println("SUCCESS! Received: " + callBacksReceivedInRound + " out of: "
//							+ expectedCallbacksInRound + " callbacks.\n");
				}
		
				procLaunchedTotal += procLaunchedInRound;
				
				procLaunchedInRound = 0;		// reset round
				callBacksReceivedInRound = 0; 
				callBacksMissedInRound = 0;
			}

		}


		System.out.println("\n\nSleeping for 3 secs and and then show the final report!");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\n----------------------------------------------------------");
		System.out.println("           Final report: \n");
		System.out.println("Total Callbacks expected: " + procLaunchedTotal * CALLBACKS_PER_PROCESS);
		System.out.println("Total Callbacks received: " + totalCallbacksReceived);
		System.out.println("Total Callbacks missed: " + totalCallbacksMissed);
		System.out.println("\nTotal Processes launched: " + procLaunchedTotal);
		System.out.println("\nTotal Processes failed to launch: " + procFailedToLaunchTotal);
		System.out.println("----------------------------------------------------------\n");

		System.out.println("End of test");
	}
}
