#ifndef PMG_PROCESS_H
#define PMG_PROCESS_H

#include <map>
#include <vector>
#include <memory>

#include "ProcessManager/defs.h"
#include "ProcessManager/Handle.h"
#include "ProcessManager/Exceptions.h"

namespace daq {
  namespace pmg {

    class ProcessImpl;
    class Proxy;

    /**
     * \class Process Process.h "ProcessManager/Process.h"
     *
     * \brief The Process class is the object which represents a process
     *        which has been started. It allows to send commands and get information.
     *
     *        It is instantiated either following a start request on a
     *        ProcessDescription object or by constructing it with a valid Proxy.
     *        It can also be used to register callbacks on a running process, in which case
     *        the callbacks will be issued until this object is deleted (or the corresponding
     *        process exits). In particular, the callback optionally passed to the start
     *        function of a ProcessDescription object is stored in 
     *        the returned Process object. For consistency reasons because of CallBack objects, 
     *        no copy constructor is provided. The preferred way to get new instances is to query 
     *        Singleton to get fresh copies.
     *
     *        This class does not contain the real code to execute all the actions. The implementation
     *        is performed by the ProcessImpl class. Every time a Process object is created, a new 
     *        ProcessImpl instance is created too. 
     */

    class Process : public std::enable_shared_from_this<Process> {

    private:
      /**
       * \brief Constructor taking a Proxy parameter. Normally not used by user
       *        code; the user can get a Process instance from the result of the start method
       *        of a ProcessDescription object or by querying the Singleton for a fresh instance.
       *
       * \sa    Singleton::get_process(), ProcessDescription::start().
       */

      Process(const std::shared_ptr<Proxy>& proxy);

      /**
       * \brief The destructor: it deletes the ProcessImpl object.
       */

      ~Process();

      friend class ProcessDeleter;

    public:

      /**
       * \brief Replaces the standard constructor in order to guarantee that the object
       *        is always packed in a sharted pointer. Normally not used by user
       *        code; the user can get a Process instance from the result of the start method
       *        of a ProcessDescription object or by querying the Singleton for a fresh instance.
       */
      static std::shared_ptr<Process> create(const std::shared_ptr<Proxy>&);

      bool operator==(const Process &other) const;   ///< Just operator overloading provided for convenience.
      bool operator!=(const Process &other) const;   ///< Just operator overloading provided for convenience.

      /**
       * \brief It gets the name of the process.
       *
       * \return The process name.
       *
       * \sa ProcessImpl::name().
       */

      const std::string name() const;

      /**
       * \brief It gets the process handle.
       *
       * \return The process handle.
       *
       * \sa ProcessImpl::handle().
       */

      const Handle& handle() const;

      /**
       * \brief It gets process status information. This information is updated
       *        every time the process status changes.
       *
       * \return A daq::pmg::PMGProcessStatusInfo structure containing process status information.
       *
       * \sa ProcessImpl::status().
       */

      PMGProcessStatusInfo status() const;

      /**
       * \brief It gets the executable path.
       *
       * \return The executable path.
       *
       * \sa ProcessImpl::path().
       */

      const std::string path() const;

      /**
       * \brief It gets the process environment.
       *
       * \return Map containing the process environment (variable->value).
       *
       * \sa ProcessImpl::env().
       */

      std::map<std::string,std::string> env() const;

      /**
       * \brief It gets the parameters passed to the process.
       *
       * \return String vector containing all the process parameters.
       *
       * \sa ProcessImpl::params().
       */

      std::vector<std::string> params() const;

      /**
       * \brief It gets the process resource usage.
       *
       * \return A daq::pmg::PMGResourceInfo containing the process resource usage information.
       *
       * \sa ProcessImpl::resource().
       */

      PMGResourceInfo resource() const;

      /**
       * \brief It checks if the process is linked or not (i.e., a callback for it has been registered).
       * 
       * \return \c TRUE If the process is linked.
       *
       * \sa ProcessImpl::is_linked().
       */

      bool is_linked() const;

      /**
       * \brief It checks if the process exited.
       *
       * \return \c TRUE if the process exited.
       *
       * \sa ProcessImpl::is_running().
       */

      bool exited() const;

      /**
       * \brief It checks if the process exists.
       *
       * \return \c TRUE if the process exists.
       */

      bool is_valid() const;

      /**
       * \brief It registers a callback on this Process object. Only possible if
       *        it has not been done before on this object. The passed function
       *        will be called each time the watched process changes its status;
       *        it will stop being called either when this Process object is
       *        deleted or when the watched process exits or when unlink() is
       *        called on this Process object.
       *
       * \param callback The callback user function. Must be non-null.
       * \param callbackparameter Optional callback parameter to be
       *                          passed to the callback function during
       *                          the actual callback.
       *
       * \sa ProcessImpl::link().
       */

      void link(CallBackFncPtr callback, void* callbackparameter = 0);

      /**
       * \brief It clears the callback function. Only possible if a callback
       *        has previouly been registered on this Process.
       *
       * \sa ProcessImpl::unlink().
       */

      void unlink();

      /**
       * \brief It asks the ProcessManager server to terminate the process via a \e SIGTERM.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \note The ProcessManager server is called via \e CORBA.
       *
       * \sa ProcessImpl::stop(), Server::stop(), Daemon::stop(), Application::stop().
       */

      void stop();

      /**
       * \brief It asks the ProcessManager server to kill the process via a \e SIGKILL.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \note The ProcessManager server is called via \e CORBA.
       *
       * \sa ProcessImpl::kill(), Server::kill(), Daemon::kill(), Application::kill().
       */

      void kill();

      /**
       * \brief It asks the ProcessManager server to "softly" kill the process. The server will first send the process a \e SIGTERM and then,
       *        if the process has not exited within a certain amount of time, it will send a \e SIGKILL.
       *
       * \param timeout The time (in seconds) to wait before sending the \e SIGKILL.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       *
       * \note The ProcessManager server is called via \e CORBA.
       *
       * \sa ProcessImpl::kill_soft(), Server::kill_soft(), Daemon::kill_soft(), Application::kill_soft().
       */
      
      void kill_soft(const int timeout);

      /**
       * \brief It asks the ProcessManager server to send a \e POSIX signal to the process.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC;
       * \throw pmg::Failed_Signal_Process An error occurred while trying to send the signal to the process.
       *
       * \param signum \e POSIX signal number.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Process_Already_Exited The process already exited
       * \throw pmg::Signal_Not_Allowed Signal not allowed by the AccessManager
       * \throw pmg::Server_Internal_Error Server error while processing the request
       *
       * \note The ProcessManager server is called via \e CORBA.
       *
       * \sa ProcessImpl::signal(), Server::signal(), Daemon::signal(), Application::signal().
       */
      
      void signal(const int signum);

      /**
       * \brief It asks the ProcessManager server who started the process to retrieve the content of the process error file.
       *
       * \param fileContent CORBA sequence whose buffer will contain the file contents.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Cannot_Get_File The ProcessManager server is not able to retreive the file
       *
       * \note The buffer held by \a fileContent can be retreived using the get_buffer() method offered by the
       *       CORBA sequence interface (i.e., const char* fileAsAString = (const char*) fileContent->get_buffer()). 
       *
       * \sa Server::errFile()
       */      
      
      void errFile(pmgpriv::File_var& fileContent) const;
      
      /**
       * \brief It asks the ProcessManager server who started the process to retrieve the content of the process output file.
       *
       * \param fileContent CORBA sequence whose buffer will contain the file contents.
       *
       * \throw pmg::No_PMG_Server The ProcessManager server is not found in \e IPC
       * \throw pmg::Bad_PMG_Server The ProcessManager server is not able to answer
       * \throw pmg::Cannot_Get_File The ProcessManager server is not able to retreive the file
       *
       * \note The buffer held by \a fileContent can be retreived using the get_buffer() method offered by the
       *       CORBA sequence interface (i.e., const char* fileAsAString = (const char*) fileContent->get_buffer()). 
       *
       * \sa Server::outFile()
       */
      
      void outFile(pmgpriv::File_var& fileContent) const;

    protected:

      /**
       * \brief It gets a pointer to the ProcessImpl object.
       *
       * \return A pointer to Process::m_impl.
       */

      ProcessImpl* impl() const;

    private:

      std::unique_ptr<ProcessImpl> m_impl;    ///< Pointer to the ProcessImpl instance.

    }; // Process
  } 
} // daq::pmg 

#endif 
