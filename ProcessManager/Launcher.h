/*
 *  Launcher.h
 *
 *  Created by Matthias Wiesmann on 16.02.05.
 *
 */

#ifndef PMG_LAUNCHER_H
#define PMG_LAUNCHER_H

#include "ProcessManager/Manifest.h"

#include <chrono>
#include <mutex>
#include <memory>
#include <thread>
#include <condition_variable>

namespace System {
  class FIFOConnection;
}

namespace daq {
  namespace pmg {


    /**
     * \class Launcher Launcher.h "ProcessManager/Launcher.h"
     *
     * \brief This class is the main component of the ProcessManager Launcher program.
     *        The Launcher takes care of starting the process using the manifest to take all the needed information.
     *        It waits for the process to exit and it notifies the ProcessManager server every time
     *        the process status changes (this is done writing information into the report \e FIFO).
     *        The Launcher starts a thread (the control thread) reading on the control \e FIFO and waiting for requests (from the server)
     *        to send commands to the process (mainly \e POSIX signals).
     *        A second thread (the sync thread) is spawn if the process is configured to notify the ProcessManager server
     *        when it is actively running.
     *
     */

    class Launcher {

    public:

      /**
       * \brief The constructor: it gets the Launcher \e PID and creates a conditional variable.
       *
       * \note The conditional variable is used to notify the control thread that the process has exited.
       *       This is needed when the Launcher is asked to terminate the child process in a "soft" way: first a
       *       \e SIGTERM is sent and then, if the process's not exited within a certain amount of time, a \e SIGKILL is sent.
       *       After the \e SIGTERM has been sent the control thread waits on the conditional variable using a timeout. If this
       *       timeout elapses and the conditional variable has not changed (i.e., the process's not exited) then it sends the
       *       \e SIGKILL.
       */

      Launcher(const System::File &dir);

      /**
       * \brief The destructor: it stops the sync and control threads if still running and deletes some internally created objects.
       */

      ~Launcher();

      /**
       * \brief This is the main Launcher method:
       *
       *        \li It starts the process;
       *        \li It spawns the control thread and the sync thread (if needed);
       *        \li It blocks calling \e wait on the process;
       *        \li It waits for the control thread to exit (it causes the control_loop() to exit writing the \c QUIT_COMMAND on the control \e FIFO).
       */

      void launch();

    private:

      std::thread                     m_control_thread;   ///< \brief Thread that handles control commands.
      std::thread                     m_sync_alarm;       ///< \brief Thread that checks if process has issued the sync.
      std::mutex                      m_cond_mutex;       ///< \brief Mutex paired to the conditional variable used in the case of a \c KILL_SOFT.
      std::condition_variable         m_cond_var;         ///< \brief Conditional variable used in the case of a \c KILL_SOFT.
      Manifest                        m_manifest;         ///< \brief Manifest for the process.
      std::mutex                      m_mutex;            ///< \brief Mutex used to protect manifest access.
      System::Process*                m_process;          ///< \brief Pointer to the process the launcher controls.
      pid_t		              		  launcher_pid;       ///< \brief Launcher \c PID.
      std::string					  m_process_handle;   ///< \brief The process handle as a string.
      bool                            m_process_joined;   ///< \brief Falg used for the \c KILL_SOFT mechanism.

      static System::FIFOConnection*  m_report_fifo;      ///< \brief Pointer to the report \e FIFO (used only in signal_handler()).
      static System::FIFOConnection*  m_control_fifo;     ///< \brief Pointer to the control \e FIFO (used only in signal_handler()).
      static System::Process*         m_process_static;   ///< \brief Pointer to the process the launcher controls (used only in signal_handler()).

    protected:

      friend void signal_handler(int signal);

      /**
       * \brief Method used to unmap the manifest, unlink the control FIFO and
       *        send a signal to the server to stop the report thread.
       */

      void doCleanUp();

      /**
       * \brief This method starts the child process. All launch information is extracted from the manifest file
       *        and the following operations are done:
       * \li manifest is checked;
       * \li current working directory is set;
       * \li launcher pid is stored;
       * \li process is started with redirection to output and error stream;
       * \li process id is recorded in the object and the manifest.
       */

      void start_child();

      /**
       * \brief It waits for the child process to terminate. This is done by calling join repeatedly (join returns at every signal).
       *
       * \sa System::Process::join().
       */

      void wait_child();

      /**
       * \brief It is executed when the process exits. It just updates the process resource usage statistics and
       *        fills the manifest with the process stop time.
       */

      void end_child();

      /**
       * \brief It updates process information. This simply calls \c getrusuage and puts the result into the manifest.
       */

      void update_data();

      /**
       * \brief It starts the control thread.
       */

      void start_control();

      /**
       * \brief It waits until the control thread terminates.
       *        This method first sends command \e q (quit) into the \e FIFO stream
       *        then does a join on the control thread.
       */

      void wait_control();

      /**
       * \brief It checks if:
       * \li \c m_process is defined;
       * \li the process pointed by \c m_process exists;
       * \li the state of the process is either \c p_created or \c p_running;
       *
       * \return \c TRUE If all the checks are successful.
       */

      bool continue_run();

      /**
       * \brief The main control loop (executed in a seperate thread - the so called "control thread").
       *
       *        The loop does a blocking read on the control \e FIFO and then parses the result.
       *        The following commands are understood:
       *
       *        \li \e u It updates the resource usage data in the manifest;
       *        \li \e s <var>number</var> It sends signal <var>number</var> to the process;
       *        \li \e q It stops the control loop;
       *        \li \e k It sends the \e SIGKILL signal to the process;
       *        \li \e t It sends the \e SIGTERM signal to the process;
       *        \li \e K <var>timeout</var> It first sends the \e SIGTERM signal to the process and the the \e SIGKILL one if the process's not exited within
       *                                    the <var>timeout</var>.
       */

      void control_loop();

      /**
       * \brief It contains the code executed by thread. It verifies regularly if the sync file has been created by child.
       *        It is stopped after a timeout (in this case the process status is set to
       *        \c SYNCERROR and the process is terminated) or once the sync has been created (in this case the process status
       *        is set to \c RUNNING).
       */
      void sync_loop();

      /**
       * \brief This method notify the daemon that the state of the process has changed.
       */

      void update_state();

      /**
       *  \brief returns full name of sync file.
       */

      std::string sync_file();

      /**
       * It kills the process when the time point is reached (unless the process is already exited)
       */
      void auto_kill_loop(const std::chrono::steady_clock::time_point& endTime);

    };  // Launcher

    bool propagateGID(std::string& startDir);                ///< \brief It scans the \a startDir directory tree for the GID bit

    bool verify_process_sync(void* launcher);                ///< \brief verifies that the sync file has been created

    /**
     * \brief Signal handler function used in the Launcher. It catches the \e SIGTERM and \e SIGINT signals and:
     *        \li Stops the Launcher control thread;
     *        \li Removes all the \e FIFOs;
     *        \li Kills the child process sending it a \e SIGKILL signal.
     *
     * \sa Launcher.
     */

    void signal_handler(int signal);

  }
}  // daq::pmg

#endif
