#ifndef PMG_DAEMON_H
#define PMG_DAEMON_H

#include "ProcessManager/Handle.h"
#include "ProcessManager/defs.h"
#include "ProcessManager/Application.h"

#include <pmgpriv/pmgpriv.hh>

#include <system/File.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>

class IPCAlarm;

namespace daq {
  namespace pmg {

    class Partition;

    /**
     * \class Daemon Daemon.h "ProcessManager/Daemon.h"
     *
     * \brief This class encapsulates almost all the functionalities of the ProcessManager server.
     *
     *        The Daemon class manages not only all the requests from clients (i.e., to start or terminate a process) but
     *        also the process hierarchy (partition, application name, etc.). It is responsible to regularly update process
     *        and server information in \e IS (this id done using an IPCAlarm). It interacts also
     *        with the \e RM (to check process resources before starting it), the \e AM (to ask about the user
     *        authorization to start a process) and the Application class (to send commands to processes).
     *        For each instance of the ProcessManager server only one instance of the Daemon class exists.
     *
     */

    class Daemon {

    public:

      typedef std::map<std::string, pmg::Partition*> partition_table_t; ///< A definition for map containing a pointer to a Partition object and the partition name.

      /**
       * \brief It creates the IPCAlarm updating information in \e IS.
       *
       * \param directory Reference directory used by the ProcessManager server (i.e., /tmp/ProcessManager).
       */

      Daemon(System::File directory);

      /**
       * \brief It deletes the IPCAlarm object used to update \e IS info.
       */

      ~Daemon();

      /**
       * \brief It asks all the Applications to stop the threads reading on the report FIFO.
       *        This method is only called by the Server destructor and is needed to avoid
       *        segfaults when the pmgserver is killed.
       *
       * \sa    Application::stopReportThread().
       */

      void stopAllReportThreads() const;

      /**
       * \brief It is executed when a request to start a process is received.
       *
       *        This method does not actually start the process but makes all the required steps to do it.
       *        It executes the following actions: aks the AccessManager about
       *        user authorization to start the process, creates the Application object describing
       *        the process to execute, inits and fills the manifest, checks the executable and at the end
       *        adds the client to the ClientList. If all the checks are successful then the process handle is built and returned,
       *        otherwise an exception is raised and the created Application object is set as "invalid".
       *
       * \param requestInfo Information about the process requested to start.
       *
       * \return The application handle (as a string).
       *
       * \throw pmg::Internal_Server_error The server has got an internal error.
       *
       * \throw pmg::AM_Not_Allowed The AccessManager denies the authorization to start the process.
       *
       * \throw pmg::Binary_Not_Found The process executable has not been found.
       *
       * \throw System::PosixIssue, ers::Issue, std::exception or any other exception when a generic error occurred (i.e., while accessing the manifest).
       *
       * \sa RMBridge.h
       */

      char* request_start(const pmgpriv::ProcessRequestInfo& requestInfo);

      /**
       * \brief The process state is set to REQUESTED.
       *        The Resource Manager is asked (if needed) and then process is started calling the Application::start() method.
       *        If some error occurs the Application object corresponding to the \a handle handle is set as "invalid".
       *
       * \param handle The handle describing the process to be started.
       *
       * \throw pmg::Internal_Server_error The server has got an internal error.
       *
       * \throw pmg::No_RM_Resources No resources are present in RM to start the process.
       *
       * \throw Any exception raised by Application::start().
       *
       * \sa RMBridge.h
       */

      void really_start(const pmg::Handle& handle);

      /**
       * \brief It is executed every time a client wants a \e POSIX signal to be sent to a process.
       *        It calls Application::signal().
       *
       * \param handle The handle of the process to be signalled.
       * \param signum The \e POSIX signal number.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Application_NotFound_Invalid The Application object has not been found or is invalid
       * \throw pmg::AM_Not_Allowed  The signal is not allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped The signal has not been sent because the manifest
       *        has already been unmapped.
       * \throw pmg::Launcher_Not_Running The signal has not been sent because the associated launcher
       *        is not running
       * \throw pmg::FIFO_Error The signal has not been sent because of an error writing/opening the control FIFO
       */

      void signal(const pmg::Handle& handle, const CORBA::Long signum, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It is executed every time a client wants a \e SIGTERM signal to be sent to a process.
       *        It calls Application::stop().
       *
       * \param handle The handle of the process to be terminated.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Application_NotFound_Invalid The Application object has not been found or is invalid
       * \throw pmg::AM_Not_Allowed  The signal is not allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped The signal has not been sent because the manifest
       *        has already been unmapped.
       * \throw pmg::Launcher_Not_Running The signal has not been sent because the associated launcher
       *        is not running
       * \throw pmg::FIFO_Error The signal has not been sent because of an error writing/opening the control FIFO
       */

      void stop(const pmg::Handle& handle, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It is executed every time a client wants a \e SIGKILL signal to be sent to a process.
       *        It calls Application::kill().
       *
       * \param handle The handle of the process to be killed.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Application_NotFound_Invalid The Application object has not been found or is invalid
       * \throw pmg::AM_Not_Allowed  The signal is not allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped The signal has not been sent because the manifest
       *        has already been unmapped.
       * \throw pmg::Launcher_Not_Running The signal has not been sent because the associated launcher
       *        is not running
       * \throw pmg::FIFO_Error The signal has not been sent because of an error writing/opening the control FIFO
       */

      void kill(const pmg::Handle& handle, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It is executed every time a client wants a a process to be killed in a "soft" way.
       *        It calls Application::kill_soft().
       *
       * \param handle The handle of the process to be terminated.
       * \param timeout The time (in seconds) to be passed to the Application::kill_soft() method.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Application_NotFound_Invalid The Application object has not been found or is invalid
       * \throw pmg::AM_Not_Allowed  The signal is not allowed by the AccessManager
       * \throw pmg::Application_Manifest_Already_Unmapped The signal has not been sent because the manifest
       *        has already been unmapped.
       * \throw pmg::Launcher_Not_Running The signal has not been sent because the associated launcher
       *        is not running
       * \throw pmg::FIFO_Error The signal has not been sent because of an error writing/opening the control FIFO
       */

      void kill_soft(const pmg::Handle& handle, const CORBA::Long timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It executes the Application::kill_soft() method for all the applications within a partition.
       *
       * \param partition The partition the applications belong to.
       * \param timeout The time (in seconds) to be passed to the Application::kill_soft() method.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Partition_Signal_Exception Some error occurred while killing a process in the partition
       *
       * \return \c TRUE If the command was successfull, otherwise \c FALSE (i.e., the \a partition
       *                 has not been found).
       */

      bool kill_partition(const std::string& partition, const CORBA::Long timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It executes the Application::kill_soft() method for all the applications in all partitions.
       *
       * \param timeout The time (in seconds) to be passed to the Application::kill_soft() method.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Failed_Kill_All Some error occurred while killing a process
       */

      void kill_all(const CORBA::Long timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It looks for the application matching the sarch criteria.
       *
       * \param app_name Name of the application.
       * \param part_name Name of the partition the application belongs to.
       *
       * \return The application handle as a string. The string is empty if the application has not been found.
       */

      char* lookup(const std::string& app_name, const std::string& part_name) const;

      /**
       * \brief It looks for all the running process in a partition
       *
       * \param partition Name of the partition the processes should belong to
       *
       * \return A string containing the handles of all the processes (they are separated by the pmg::ProcessManagerSeparator string - defined in defs.h).
       *         The string is empty if no process has been found or the requested partition does not exists.
       */

      char* processes(const std::string& partition) const;

      /**
       * \brief It looks for all the running process in a partition
       *
       * \param partition Name of the partition the processes should belong to
       * \param procInfoList List of process running in the partition
       */
      void processes_info(const std::string& partition, pmgpriv::proc_info_list* procInfoList) const;

      /**
       * \brief It looks for all the running process in all the partitions
       *
       * \return A string containing the handles of all the processes (they are separated by the pmg::ProcessManagerSeparator string - defined in defs.h).
       *         The string is empty if no process has been found or the requested partition does not exist.
       */

      char* all_processes() const;

      /**
       * \brief It looks for all the running process in all the partitions
       *
       * \param procInfoList List of all the processes running in all the partition
       */
      void all_processes_info(pmgpriv::proc_info_list* procInfoList) const;

      /**
       * \brief It checks if an application is active or not. It internally calls Application::is_active() and Application::invalid().
       *
       * \param handle Application handle.
       *
       * \return \c TRUE if the application is active, \c FALSE if not.
       *
       * \sa Application::is_active(), Application::invalid().
       */

      bool exists(const pmg::Handle& handle) const;

      /**
       * \brief It adds a client to an application ClientList.
       *
       * \param requestInfo Structure containing the handle of the application the client would link to
       *                    and the client \e CORBA reference.
       *
       * \return \c FALSE if the client is already in the ClientList,
       *         \c TRUE if the client is successfully added to the ClientList,
       *
       * \sa Application::is_in_client_list(),
       *     Application::add_client_ref(const pmgpriv::CLIENT_var&).
       */

      bool link_client(const pmgpriv::LinkRequestInfo& requestInfo) const;

      /**
       * \brief It gets all available information about an application.
       *
       * \param handle The application handle.
       * \param info A pointer to the structure to be filled with application information.
       *
       * \return \c TRUE if the \a info structure has been successfully filled, otherwise \c FALSE.
       */

      bool get_info(const pmg::Handle& handle, pmgpub::ProcessStatusInfo* info) const;

      /**
       * \return It returns Daemon::m_directory
       */

      const System::File& directory() const;

      /**
       * \brief It builds an Application object by handle.
       *
       * \param handle The application handle.
       *
       * \return A pointer to the new Application object.
       *
       * \note It calls application_factory(const std::string&,const std::string&,pmg_id_t) with \a identity
       *       different than \e 0. This method is used only if you need to create an Application object
       *       when the process was already started. Basically this is needed to reconnect the ProcessManager
       *       server to an already running application after a failure. This is also the only case in which
       *       the application handle is known before the Application object has been created.
       */

      Application* build_app(const pmg::Handle& handle);

      /**
       * \brief It prints application information to a stream.
       *
       * \param stream The stream information will be sent to.
       */

      void print_to(std::ostream& stream) const;

    protected:

      /**
       * \brief It gets the total number of active processes (in all partitions).
       *
       * \return The number of active processes.
       *
       * \sa Partition::active_processes().
       */

      unsigned int active_processes() const;

      /**
       * \brief It gets a collection containing pointers to all application.
       *
       * \return A collection containing all applications pointers.
       *
       */

      application_collection_t all_applications() const;

      /**
       * \brief It gets all the partitions.
       *
       * \return It returns Daemon::m_partition_table.
       */

      const partition_table_t& partitions() const;

      /**
       * \brief It removes the \a partitionName partition from the partition list.
       *        It is called by update_all_is().
       *
       * \param partitionName The name of the partition to remove.
       */

      void removePartition(const std::string& partitionName);

      /**
       * \brief It finds a partition by name.
       *
       * \param name The name of the partition to find.
       *
       * \return A pointer to the Partition object or \e 0 if the partition has not been found.
       */

      Partition *partition(const std::string& name) const;

      /**
       * \brief It builds a partition from a name.
       *
       * \param name Name of the partition.
       *
       * \return A pointer to the new partition or to the already existing one.
       */

      Partition *partition_factory(const std::string& name);

      /**
       * \brief It finds an application by name, partition name and unique id.
       *
       * \param partition_name The partition name.
       * \param application_name The application name.
       * \param identity The application unique id.
       *
       * \return A pointer to the application, or \e 0 if the application has not been found.
       *
       * \sa Partition::application(const std::string&,long) const.
       */

      Application *application(const std::string& partition_name,
			       const std::string& application_name,
			       pmg_id_t identity) const;

      /**
       * \overload Application * daq::pmg::Daemon::application(const pmg::Handle &handle) const.
       *
       * \param handle The handle of the application.
       *
       * \return A pointer to the application object, or \e 0 if the application has not been found.
       */

      Application *application(const pmg::Handle& handle) const;

      /**
       * \brief It builds a new Application object.
       *
       * \param partition_name Name of the partition the application will belong to.
       * \param application_name Name of the appllication.
       * \param identity Unique id of the application.
       *
       * \return A pointer to the new Application object.
       *
       * \note Partition and ApplicationList object instances are created and registered as needed.
       *
       * \warning The default value of \a identity is \e 0. This is the value it should always be used.
       *          This method is called with an \a identity value different than \e 0 only by
       *          build_app(const pmg::Handle&).
       *
       * \sa build_app(const pmg::Handle&), Partition::application_factory(const std::string&, long),
       *     ApplicationList::application_factory(pmg_id_t).
       */

      Application *application_factory(const std::string& partition_name,
				       const std::string& application_name,
				       pmg_id_t identity = (pmg_id_t)0);

      /**
       * \brief It sends a \e SIGTERM signal to all the processes in all partitions.
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Failed_Kill_All Some error occurred while killing a process
       *
       * \sa Partition::stop().
       */

      void stop(const std::string& userName, const std::string& hostName) const;

      /**
       * \brief Called by kill_all().
       *
       * \throw pmg::Failed_Kill_All Some error occurred while killing a process
       */

      void kill(const int timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief Function executed by an IPCAlarm to update process and agent information in \e IS.
       *        This alarm takes also care to remove the Partition object if the partition is no more
       *        present in IPC and if no processes are still active.
       *        It is executed every IS_UPDATE_PERIOD (defined in private_defs.h) seconds.
       *
       * \param daemon_ptr Pointer to the Daemon object.
       *
       * \return It always returns \c TRUE (i.e., the IPCAlarm is never stopped).
       */

      friend bool update_all_is(void* daemon_ptr);

    private:

      IPCAlarm*                      m_Update_IS;                 ///< Pointer to the IPCAlarm used to update \e IS information.
      partition_table_t              m_partition_table;           ///< Container containing all the partitions for which a process has been started.
      System::File                   m_directory ;                ///< Reference directory used by the ProcessManager server (i.e., /tmp/ProcessManager).
      static boost::recursive_mutex  partition_table_mutex;       ///< Mutex used to protect the access to the Partition table.

    public:

      static boost::recursive_mutex  application_table_mutex;     ///< Mutex used to protect the access to the Application table.

    }; // Daemon

    bool update_all_is(void* daemon_ptr);

  }

} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Daemon& daemon);

#endif
