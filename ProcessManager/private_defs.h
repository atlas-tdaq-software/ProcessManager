#ifndef PMG_PRIV_DEFS_H
#define PMG_PRIV_DEFS_H

#include <time.h>
#include <string>

namespace daq {
  namespace pmg {

    const char UPDATE_CODE = 'u';                            ///< \brief Code used to notify the launcher to update the resource usage statistics. \sa Application::update_data().
    const char SIGNAL_CODE = 's'; 			     ///< \brief Code used to notify the launcher to send a signal to the process. \sa Application::signal().
    const char KILL_CODE = 'k'; 			     ///< \brief Code used to notify the launcher to kill the process. \sa Application::kill().
    const char KILL_SOFT_CODE = 'K';			     ///< \brief Code used to notify the launcher to "softly" kill the process. \sa Application::kill_soft().
    const char TERM_CODE = 't'; 			     ///< \brief Code used to notify the launcher to terminate a process. \sa Application::stop(). 
    const char QUIT_CODE = 'q'; 			     ///< \brief Code used to notify the launcher to quit (mostly the control thread). 

    const char* const UPDATE_COMMAND = "u\n";		     ///< \brief Command to update process data (\c UPDATE_CODE + new line).
    const char* const QUIT_COMMAND = "q\n"; 		     ///< \brief Command to quit (\c QUIT_CODE + new line). 
    const char* const TERM_COMMAND = "t\n"; 		     ///< \brief Command to terminate (\c TERM_CODE + new line).
    const char* const KILL_COMMAND = "k\n"; 		     ///< \brief Command to kill (\c KILL_CODE + new line).
    const char* const LAUNCHER_NAME = "pmglauncher"; 	     ///< \brief Binary name of the PMG launcher.
    const char* const STG1_LAUNCHER_NAME = "PMGLauncher";    ///< \brief Binary name of the first stage of the PMG launcher.

    const unsigned int  AM_MAX_RETRY                  = 3;
    const double        IPC_READY_PERIOD              = 20.0;
    const double        IPC_READY_PERIOD_LONG         = 3600.0;
    const double        IS_UPDATE_PERIOD              = 15.0;
    const double        SINGLETON_SERVER_QUERY_PERIOD = 10.0;
    const long          FIFO_SELECT_PERIOD            = 120;
    const time_t        LAUNCHER_TIMEOUT              = 5;
    const int           APP_NOTAV_STATE_TIMEOUT       = 120;
    const std::string   PROC_DIR                      = "/proc";
    const std::string   STAT_FILE                     = "stat";
    const std::string   STATM_FILE                    = "statm";
    const std::string   SERVER_PORT_FILE              = "/com/pmg_port";
    
    const std::string   IS_SERVER_PROCESS_SEPARATOR   = "|";
  }
}

#endif
