#ifndef PROC_INTERFACE_H
#define PROC_INTERFACE_H

#include <string>
#include <vector>

#include "ProcessManager/ProcStats.h"


namespace daq {  
  namespace pmg {

    /**
     * \class ProcInterface ProcInterface.h "ProcessManager/ProcInterface.h"
     *
     * \brief This class is an interface to the /proc filesystem to get process
     *        resource usage.
     */

    class ProcInterface {
    
    public:
      
      /**
       * \brief The constructor: it gets process information from 'stat' and 'statm' files.
       *
       * \param pid Process \e PID.
       *
       * \throw pmg::Exception Not able to retrieve process information from the proc file system.
       */

      ProcInterface(const unsigned int pid);

      /**
       * \brief The destructor.
       */

      ~ProcInterface();

      /**
       * \brief It returns a reference to the data structure keeping the process information.
       */

      const proc_usage& procInfo() const;
      
    protected:

      /**
       * \brief It gets process information from the 'stat' file.
       *
       * \throw pmg::Exception Not able to retrieve process information from the 'stat' file.
       */

      void readStat();

      /**
       * \brief It gets process information from the 'statm' file.
       *
       * \throw pmg::Exception Not able to retrieve process information from the 'statm' file.
       */

      void readStatm();
      
      /**
       * \brief It sets the full name of the 'stat' file starting from the process PID.
       */

      std::string setStatFile();
      
      /**
       * \brief It sets the full name of the 'statm' file starting from the process PID.
       */

      std::string setStatmFile();

    private:
      
      const unsigned int m_pid;               ///< The process PID.
      const std::string m_statFileName;       ///< The 'stat' file full name.
      const std::string m_statmFileName;      ///< The 'statm' file full name.
      proc_usage m_total_stat;                ///< Data structure keeping process information.
 
    };

  }
}

#endif
