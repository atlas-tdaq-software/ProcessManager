#ifndef PMG_AM_BRIDGE_H
#define PMG_AM_BRIDGE_H

#include <string>
#include <memory>

namespace daq {
namespace am {

class PMGResource;
class ServerInterrogator;
class RequestorInfo;

}
}

namespace daq {
namespace pmg {

/**
 * \class AMBridge AMBridge.h "ProcessManager/AMBridge.h"
 *
 * \brief This class is an interface to the AccessManager.
 *        It takes care of asking the AccessManager about the user rights
 *        to start and/or signal a process.
 */

class AMBridge {

	public:

		/**
		 * \brief Constructor for a resource with unspecified process details.
		 */

		AMBridge();

		/**
		 * \brief Constructor for a resource with process details.
		 *
		 * \param processBinaryPath The process name (or its full path).
		 * \param processHostName The host where the process will be started or is already running.
		 * \param processArguments The arguments passed to the process.
		 * \param partitionName The name of the partition the process belongs to
		 * \param processOwnedByRequester True if the action requester is the same of the process owner.
		 *
		 * \note Requests will be made on behalf of current user running the ProcessManager server.
		 */

		explicit AMBridge(const std::string& processBinaryPath,
		                  const std::string& processHostName,
		                  const std::string& processArguments,
		                  const std::string& partitionName,
		                  const bool processOwnedByRequester);

		/**
		 * \brief Constructor for a resource with process details and information about the user
		 *        who wants to start or send a signal to the process.
		 *
		 * \param requestUserName The name of the user requesting the action.
		 * \param requestHostName The host name the request comes from.
		 * \param processBinaryPath The process name (or its full path).
		 * \param processHostName The host where the process will be started or is already running.
		 * \param processArguments The arguments passed to the process.
		 * \param partitionName The name of the partition the process belongs to
		 * \param processOwnedByRequester True if the action requester is the same of the process owner.
		 */

		explicit AMBridge(const std::string& requestUserName,
		                  const std::string& requestHostName,
		                  const std::string& processBinaryPath,
		                  const std::string& processHostName,
		                  const std::string& processArguments,
		                  const std::string& partitionName,
		                  const bool processOwnedByRequester);

		/**
		 * \brief The destructor.
		 */

		virtual ~AMBridge();

		/**
		 * \brief It asks the AccessManager if the process can be started.
		 *
		 * \return \c TRUE if the action is allowed.
		 *
		 * \throw Any exception thrown by the AM layer
		 */

		bool allowStart();

		/**
		 * \brief It asks the AccessManager if a certain \e POSIX signal can be sent to the process.
		 *
		 * \param signal \e POSIX signal number.
		 *
		 * \return \c TRUE if the action is allowed.
		 *
		 * \throw Any exception thrown by the AM layer
		 */

		bool allowSignal(const int signal = 0);

	protected:

		/**
		 * \brief Avoid object copy and assignment
		 */

		AMBridge(const AMBridge& rhs);
		AMBridge& operator=(const AMBridge& rhs);

	private:

		const std::unique_ptr<am::PMGResource> pResource; ///< It holds the process information.
		const std::unique_ptr<am::ServerInterrogator> pInterrogator; ///< The AccessManager is asked through this object
		const std::unique_ptr<const am::RequestorInfo> pRequestor; ///< It holds information about the user requesting a certain action.

};

}
}

#endif
