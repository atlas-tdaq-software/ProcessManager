#ifndef PMG_CALLBACK_H
#define PMG_CALLBACK_H

#include "ProcessManager/defs.h"

#include <memory>

namespace daq {
  namespace pmg {

    class Process;

    /**
     * \class CallBack CallBack.h "ProcessManager/CallBack.h"
     *
     * \brief Used internally for callbacks. Stores the optional user parameter,
     *        the Process object used to link this callback (which is passed
     *        too during the callbacks) and the user function to use as callback.
     */

    class CallBack {

    public:

      /**
       * \brief Constructor taking all needed parameters. The CallBack then
       *        remains a constant object.
       *
       * \param callBackOwner Pointer to the Process used to create this
       *        callback. It is passed to the user callback function to
       *        allow state/info query about the process. Must be non-null.
       *
       * \param callback The user callback function. Must be non-null.
       *
       * \param callbackparameter Optional parameter to be passed to the
       *        user callback function. May be null; will be passed as such
       *        during callbacks.
       */
      
      CallBack(const std::shared_ptr<Process>& callBackOwner,
               CallBackFncPtr callback,
               void* callbackparameter);
      
      ~CallBack();

      /**
       * \brief Issues a callback using the user callback function.
       *        No parameters needed because only the Process object
       *        is passed to the user callback function, which allows
       *        state/info queries.
       */
      
      void call() const;

    private:

      std::shared_ptr<Process>       m_callBackOwner;      ///< Pointer to the Process the callback refers to.
      void*                          m_callBackParameter;  ///< Parameter to be passed to the user callback function.
      CallBackFncPtr                 m_callBackFcnPtr;     ///< The user callback function.

    }; // CallBack
  } 
} // pmg

#endif
