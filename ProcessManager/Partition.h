/*
 *  Partition.h
 *
 *  Created by Matthias Wiesmann on 07.04.05.
 *
 */


#ifndef PMG_PARTITION_H
#define PMG_PARTITION_H

#include <map>
#include <system/File.h>

#include "ProcessManager/Handle.h"
#include "ProcessManager/Application.h"

namespace daq {
  namespace pmg {

    class ApplicationList; 
    class Daemon; 
   
    /**
     * \class Partition Partition.h "ProcessManager/Partition.h"
     *
     * \brief This class encapsulates all the functionalities needed within the scope of a partition.
     *        It has methods to create new applications and browse the ones belonging to the partition.
     *        The Partition class owns an ApplicationList collecting all the application objects. It also
     *        offers methods to send commands to applications.
     *        The Partition object is removed by the daq::pmg::Daemon::removePartition(const std::string& partitionName) method
     *        called by daq::pmg::update_all_is().
     *
     * \sa    daq::pmg::update_all_is(), daq::pmg::Daemon::removePartition().
     */
 
    class Partition {

    public:

      /**
       * \brief The constructor.
       *
       * \param name The name of the partition.
       * \param parent Pointer to the Daemon.
       */

      Partition(const std::string &name, Daemon *parent);
 
      /**
       * \brief The destructor.
       *        It remove all the ApplicationList objects held by the partition.
       */

      ~Partition();

      /**
       * \brief It gets the partition name.
       *
       * \return The name of the partition.
       */

      const std::string &name() const;

      /**
       * \brief It gets the directory where all applications dir will be created (i.e., /tmp/ProcessManager/partition_name).
       *
       * \return System::File object describing the directory.
       */

      const System::File & directory() const;

      /**
       * \brief It gets the number of active (i.e., running) processes within the scope of the partition.
       *
       * \return The number of active processes.
       *
       * \sa ApplicationList::active_processes().
       */

      unsigned int active_processes(); 

      /**
       * \brief It looks for an Application by name and unique identity.
       *
       * \param name The name of the application.
       * \param identity The application unique id.
       *
       * \return A pointer to the Application matching the search criteria, or \e 0 if the Application
       *         is not found.
       */

      Application *application(const std::string &name, long identity) const;

      /**
       * \brief It gets all the applications conatained in all the ApplicationList objects owned by the Partition.
       *
       * \param collection The \c application_collection_t container where to put the Application objects.
       *
       * \return The \a collection container.
       *
       * \sa ApplicationList::all_applications().
       */
 
      application_collection_t &all_applications(application_collection_t & collection) const; 

      /**
       * \brief It terminates all the applications int the partition.
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       *
       * \throw pmg::Partition_Signal_Exception Some error occurred while sending the signal to a process
       *
       * \sa ApplicationList::stop().
       */
 
      void stop(const std::string& userName, const std::string& hostName) const; 

      /**
       * \brief It sends the \e POSIX signal \a signum to all the applications in the partition.
       *
       * \param signum The \e POSIX signal number.
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       * 
       * \throw pmg::Partition_Signal_Exception Some error occurred while sending the signal to a process
       *
       * \sa ApplicationList::signal().
       */

      void signal(int signum, const std::string& userName, const std::string& hostName) const; 

      /**
       * \brief It kills all the applications int the partition.
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       * 
       * \throw pmg::Partition_Signal_Exception Some error occurred while sending the signal to a process
       * 
       * \sa ApplicationList::kill().
       */

      void kill(const std::string& userName, const std::string& hostName) const;


      /**
       * \brief It "softly" terminates  all the applications int the partition.
       *
       * \param userName The name of the user requesting the action.
       * \param hostName The name of the host the request comes from.
       * \param timeout The time (in seconds) to wait for the process to exit before sending the \e SIGKILL signal.
       * 
       * \throw pmg::Partition_Signal_Exception Some error occurred while sending the signal to a process
       *      
       * \sa ApplicationList::kill_soft().
       */

      void kill_soft(int timeout, const std::string& userName, const std::string& hostName) const;

      /**
       * \brief It looks in the partition for an application by name.
       *
       * \param app_name Name of the application to look for.
       *
       * \return A pointer to the corresponding Application object or \e NULL if the application is not found.
       *
       * \sa ApplicationList::lookup().
       */

      Application* lookup(const std::string& app_name) const;

      /**
       * \brief In the ApplicationList (identified by \a name) it creates an Application object with unique id equal to \a identity.
       *
       * \param name Name of the ApplicationList.
       * \param identity Unique id of the Application object (the default value should always be used).
       *
       * \return A pointer the the new created Application or to an existing one with unique id equal to \a identity.
       *
       * \note It calls application_list_factory() and ApplicationList::application_factory(). This means
       *       that no new objects will be created if some object matching the input criteria already exists.
       *
       * \warning For the usage of this method with \a identity different than zero see ApplicationList::application_factory().
       */
 
      Application *application_factory(const std::string & name, long identity=(long)0);
    
      /**
       * \brief It prints all the application information to a stream.
       *
       * \param stream The stream where to send the information.
       *
       * \sa ApplicationList::print_to().
       */
      
      void print_to(std::ostream& stream) const;
 
    protected:

      /**
       * \brief It gets the pointer to the Daemon.
       *
       * \return The pointer to the Daemon.
       */
 
      const Daemon *parent() const; 

      /**
       * \brief It looks for an ApplicationList by name.
       *
       * \param name Name of the application.
       *
       * \return A pointer to the ApplicationList matching the search criteria, or \e 0 if the
       *         ApplicationList is not found.
       */

      ApplicationList *application_list(const std::string &name) const; 
      
      /**
       * \brief It creates a new ApplicationList (with name \a name) whose Application unique id 
       *        will start from \a identity.
       *
       * \param name Name of the application the ApplicationList will hold.
       * \param identity The starting unique id of the Application objects containd in the ApplicationList.
       *
       * \return A pointer to the new ApplicationList or to an already existing ApplicationList identified 
       *         by \a name. 
       *
       * \warning For the usage of this method with \a identity different than zero see ApplicationList::ApplicationList().
       */

      ApplicationList *application_list_factory(const std::string &name, long identity=(long)0);

      /**
       * \brief It gets the Directory where applications will place their files (i.e., /tmp/ProcessManager/partition_name).
       *
       * \param name The partition name.
       * \param parent Pointer to the Daemon.
       */
  
      static System::File directory_name(const std::string &name, Daemon *parent);

    private:

      typedef std::map<std::string, ApplicationList*> partition_map_t;             ///< \brief Definition for map containing the ApplicationList objects associated to their names. 

      std::string                 m_name;                                          ///< \brief Name of the partition.
      System::File                m_directory;                                     ///< \brief Directory where applications will place their files (i.e., /tmp/ProcessManager/partition_name).
      partition_map_t             m_partition_map;                                 ///< \brief Map containing pointers to all the owned ApplicationList objects.
      Daemon*                     m_parent;                                        ///< \brief Pointer to the Daemon.
    
    }; // Partition
  } 
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Partition& partition);

#endif
