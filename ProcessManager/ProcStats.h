#ifndef PROC_STATS_H
#define PROC_STATS_H

#include <string>

namespace daq {

namespace pmg {

/**
 * \brief Structure to store process info from /proc/PID/stat.
 */

typedef struct {
		int64_t pid;
		std::string comm;
		char state;
		int64_t ppid;
		int64_t pgrp;
		int64_t session;
		int64_t tty_nr;
		int64_t tpgid;
		uint64_t flags;
		uint64_t minflt;
		uint64_t cminflt;
		uint64_t majflt;
		uint64_t cmajflt;
		uint64_t utime;
		uint64_t stime;
		int64_t cutime;
		int64_t cstime;
		int64_t priority;
		int64_t nice;
		int64_t num_threads;
		int64_t itrealvalue;
		uint64_t starttime;
		uint64_t vsize;
		int64_t rss;
		uint64_t rsslim;
		uint64_t startcode;
		uint64_t endcode;
		uint64_t startstack;
		uint64_t kstkesp;
		uint64_t kstkeip;
		uint64_t signal;
		uint64_t blocked;
		uint64_t sigignore;
		uint64_t sigcatch;
		uint64_t wchan;
		uint64_t nswap;
		uint64_t cnswap;
		int64_t exit_signal;
		int64_t processor;
		uint64_t rt_priority;
		uint64_t policy;
} proc_stat;

/**
 * \brief Structure to store process info from /proc/PID/statm.
 */

typedef struct {
		int64_t size;
		int64_t resident;
		int64_t share;
		int64_t text;
		int64_t lib;
		int64_t data;
		int64_t dt;
} proc_statm;

/**
 * \brief Global structure.
 */

typedef struct {
		proc_statm statm_info;
		proc_stat stat_info;
} proc_usage;

}

}

#endif
