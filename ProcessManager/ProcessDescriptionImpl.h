#ifndef PMG_PROCESS_DESCRIPTION_IMPL_H
#define PMG_PROCESS_DESCRIPTION_IMPL_H

#include <iostream>
#include <memory>

#include <system/Host.h>

#include "ProcessManager/Exceptions.h"
#include "ProcessManager/CallBack.h"

class IPCPartition;

namespace daq {
  namespace pmg {

     class Singleton;
     class Process;

    /**
     * \class ProcessDescriptionImpl ProcessDescriptionImpl.h "ProcessManager/ProcessDescriptionImpl.h"
     *
     * \brief This class implements all the functionalities of the ProcessDescription class.
     *        An object of this class is instantiated every time a ProcessDescription object is created.
     *        Every time a ProcessDescriptionImpl object is instantiated an IPCPartition and a Singleton
     *        objects are created.
     *
     * \see ProcessDescription.
     */

    class ProcessDescriptionImpl {

    public:

      /**
       * \brief The constructor: for argument description see ProcessDescription constructors.
       */

      ProcessDescriptionImpl(const std::string& host_name,
			     const std::string& app_name,
			     const std::string& partition,
			     const std::string& exec_list,
			     const std::string& wd,
			     const std::vector<std::string>& start_args,
			     const std::map<std::string,std::string>& env,
			     const std::string& log_path,
			     const std::string& input_dev,
			     const std::string& rm_swobject,
			     bool null_logs,
			     unsigned long init_time_out,
			     unsigned long auto_kill_time_out);

      /**
       * \overload
       */

      ProcessDescriptionImpl(const System::Host& host,
			     const std::string& app_name,
			     const std::string& partition,
			     const std::string& exec_list,
			     const std::string& wd,
			     const std::vector<std::string>& start_args,
			     const std::map<std::string,std::string>& env,
			     const std::string& log_path,
			     const std::string& input_dev,
			     const std::string& rm_swobject,
			     bool null_logs,
			     unsigned long init_time_out,
			     unsigned long auto_kill_time_out);

      /**
       * \overload
       */

      ProcessDescriptionImpl(const std::string& host_name,
			     const std::string& app_name,
			     const std::string& partition,
			     const std::vector<std::string>& exec_list,
			     const std::string& wd,
			     const std::string& start_args,
			     const std::map<std::string,std::string>& env,
			     const std::string& log_path,
			     const std::string& input_dev,
			     const std::string& rm_swobject,
			     bool null_logs,
			     unsigned long init_time_out,
			     unsigned long auto_kill_time_out);

      /**
       * \brief Copy constructor.
       */

      ProcessDescriptionImpl(const ProcessDescriptionImpl &other);

      /**
       * \brief The destructor: it deletes some internally dynamically allocated objects.
       */

      virtual ~ProcessDescriptionImpl();

      bool operator==(const ProcessDescriptionImpl &other) const;   ///< Just operator overloading.
      bool operator!=(const ProcessDescriptionImpl &other) const;   ///< Just operator overloading.

      /**
       * \brief This method asks the server to start a process.
       *
       *        All the parameters passed to the constructor are used to fill a pmgpriv::ProcessRequestInfo
       *        structure containing all information needed by the ProcessManager server to start the process.
       *        Then it calls the Server::request_start() method receiving the Handle of the starting process.
       *        If the handle is not null then a Proxy and a Process instances are created, and the callback
       *        (if any) is registered. At the end the Server::really_start() method is called and the process
       *        is started.
       *
       * \param callback Optional callback function.
       * \param callbackparameter Optional callback parameter to be
       *                          passed to the callback function during
       *                          the actual callback.
       *
       * \return A pointer to the Process object describing the started process. The user should not take care
       *         of deallocating this object. But if the created Process is a linked one (i.e., a callback
       *         is registered) the user should unlink it once the process has exited.
       *
       * \throw daq::pmg::No_PMG_Server The ProcessManager server is not found in \e IPC;
       * \throw daq::pmg::Bad_PMG_Server There is a failure while trying to contact the ProcessManager server;
       * \throw daq::pmg::Failed_start A problem occurs while starting the process.
       *
       * \sa The Process class, Server::request_start(), Server::really_start(), Singleton::start_finished(), Singleton::get_process(),
       *     Process::link().
       */

      std::shared_ptr<Process> start(CallBackFncPtr callback = 0, void* callbackparameter = 0);

      /**
       * \brief It gets some information about the starting process.
       *
       * \return A string containing information about the starting process.
       */

      const std::string toString() const;

      /**
       * \brief It gets the name of the process to be started.
       *
       * \return The name of the process as a string.
       */

      const std::string app_name() const;

      /**
       * \brief It prints starting process information to a stream.
       *
       * \param stream The stream information will be sent to.
       * \param prettyPrint Use an eye-candy string formatting.
       */

      void printTo(std::ostream &stream, const bool prettyPrint=false) const;

    protected:

      /**
       * \brief Default constructor.
       */

      ProcessDescriptionImpl();

      /**
       * \brief It gets the host the process will be started on.
       *
       * \return System::Host object describing the host.
       */

      const System::Host host() const;

      /**
       * \brief It prints the starting process information to a stream using some eye-candy formatting.
       *
       * \param stream The stream information will be sent to.
       * \param line_sep This string will be used as line separator.
       * \param line_indent This string will be used as line indent.
       */

      void print(std::ostream &stream, const std::string& line_sep, const std::string& line_indent) const;

    private:

      const std::string m_app_name;                      ///< Name of the process.
      const std::string m_partition;                     ///< Name of the partition the process will belong to.
      std::string m_exec_list;                           ///< A list of possible fully qualified executables.
      const std::string m_wd;                            ///< The working directory from which the process should be started.
      std::vector<std::string> m_start_args;             ///< The command line arguments used to start the process.
      const std::map<std::string,std::string> m_env;     ///< The environment variables which are needed to start process.
      const std::string m_log_path;                      ///< The log file directory path.
      const std::string m_input_dev;                     ///< The redirection of stdin.
      const std::string m_rm_swobject;                   ///< The swobject describing the process.
      const bool m_null_logs;                            ///< If \c TRUE logs will not be written.
      const unsigned long m_init_time_out;               ///< The initial timeout after which to give up.
      const unsigned long m_auto_kill_time_out;          ///< The timeout after which the process will be killed by the server.
      const std::unique_ptr<const System::Host> m_host;  ///< The host the process will be started on.
      const std::string m_local_host;                    ///< The client local host.
      const std::string m_login_name;                    ///< The name of the user running the client.
    }; // ProcessDescription
  }
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::ProcessDescriptionImpl& description);

#endif
