#ifndef PMG_IS_BRIDGE_H
#define PMG_IS_BRIDGE_H

#include <string>

#include <ipc/partition.h>

#include "ProcessManager/PMGPublishedAgentDataNamed.h"
#include "ProcessManager/PMGPublishedProcessDataNamed.h"
#include "ProcessManager/Exceptions.h"

namespace daq {
namespace pmg {

/**
 * \class ISBridge ISBridge.h "ProcessManager/ISBridge.h"
 *
 * \brief This is a template class providing an interface to the IS system. It offers generic methods to update and
 *        remove information from IS and a pure virtual function which should be overloaded by the user code to collect
 *        specific information about the object to be published in IS.
 *        The template argument T is the user class deriving from the is::ISNamedInfo one (i.e., the one produced by the
 *        is_edit_repository.sh script)
 */

template<typename T> class ISBridge {

    public:

        /**
         * \brief It creates an \e IS named info object of type T.
         *
         * \param partName The name of the partition in which the information has to be updated.
         * \param isServerName Name of the \e IS server to use.
         *
         * \throw It can throw any exception raised by the IS system when creating the T object.
         */

        ISBridge(const std::string& partName, const std::string& isServerName) :
                pISNamed_(new T(partName, isServerName)), partName_(partName), isName_(isServerName), clean_(true), collect_(true)
        {
        }

        /**
         * \brief Overloaded constructor. It takes as an additional argument a pointer to T.
         *        Use this constructor when you can build the full T object outside the class.
         *        In this case the T objest must be deleted by the user.
         *
         * \note When using this constructor the updateInfo() method does not call collectInfo().
         *       This means that the information will be inserted in IS as it is passed to this constructor.
         */

        ISBridge(const std::string& partName, const std::string& isServerName, T* pT) :
                pISNamed_(pT), partName_(partName), isName_(isServerName), clean_(false), collect_(false)
        {
        }

        /**
         * \brief The destructor
         */

        virtual ~ISBridge() {
            if(clean_) {
                delete pISNamed_;
            }
        }

        /**
         * \brief Method to collect and update information in \e IS. It internally
         *        uses the is::ISNamedInfo::checkin() method.
         *
         * \note It calls collectInfo() before sending information to IS. If the
         *       ISBridge(const std::string& partName, const std::string& isServerName, T* pT)
         *       constructor is used then collectInfo() is not called.
         *
         * \throw pmg::Failed_Publish_IS
         *
         */

        void updateInfo(bool boolFlag) {
            try {
                if(collect_) {
                    this->collectInfo(boolFlag);
                }
                pISNamed_->checkin();
            }
            catch(ers::Issue& ex) {
                throw pmg::Failed_Publish_IS(ERS_HERE, this->serverName(), ex.message(), ex);
            }
            catch(CORBA::Exception& ex) {
                throw pmg::Failed_Publish_IS(ERS_HERE, this->serverName(), std::string("CORBA exception ") + std::string(ex._name()));
            }
            catch(std::exception& ex) {
                throw pmg::Failed_Publish_IS(ERS_HERE, this->serverName(), ex.what(), ex);
            }
            catch(...) {
                throw pmg::Failed_Publish_IS(ERS_HERE, this->serverName(), "Unknown exception");
            }
        }

        /**
         * \brief It checks if the information already exists in IS.
         */

        bool infoExists() const {
            bool exists = false;
            try {
                exists = pISNamed_->isExist();
            }
            catch(is::Exception& ex) {
            }
            catch(...) {
                ERS_DEBUG(0, "Got an unexpected exception");
            }
            return exists;
        }

        /**
         * \brief It removes the information from IS.
         *
         * \throw pmg::Failed_Remove_IS
         */

        void removeInfo() {
            try {
                pISNamed_->remove();
            }
            catch(ers::Issue& ex) {
                throw pmg::Failed_Remove_IS(ERS_HERE, this->serverName(), ex.message(), ex);
            }
            catch(CORBA::Exception& ex) {
                throw pmg::Failed_Remove_IS(ERS_HERE, this->serverName(), std::string("CORBA exception ") + std::string(ex._name()));
            }
            catch(std::exception& ex) {
                throw pmg::Failed_Remove_IS(ERS_HERE, this->serverName(), ex.what(), ex);
            }
            catch(...) {
                throw pmg::Failed_Remove_IS(ERS_HERE, this->serverName(), "Unknown exception");
            }
        }

        protected:

        /**
         * \brief Pure virtual method to be overloaded by the user code.
         *        The method shouold contain the procedure to retrieve the information
         *        to be inserted/updated into the IS server.
         *
         * \param boolFlag Simple flag for user needs.
         *
         * \throw pmg::Failed_Collect_Info The user code implementation of this method should rise
         *        this exception when something goes wrong gathering the needed information.
         */

        virtual void collectInfo(bool boolFlag) = 0;

        /**
         * \brief It gives the pointer to the T object. To be used in the user code when
         *        overloading the collectInfo method. It gives the possibility to fill the
         *        T data structure.
         *
         * \return A pointer to T.
         */

        T* data() const {
            return pISNamed_;
        }

        /**
         * \brief It returns the IS server name.
         */

        const std::string& serverName() const {
            return isName_;
        }

        /**
         * \brief It returns the partition name.
         */

        const std::string& partitionName() const {
            return partName_;
        }

        /**
         * \brief Avoid object copy and assignment
         */

        ISBridge(const ISBridge& rhs);
        ISBridge& operator=(const ISBridge& rhs);

        private:

        T* const pISNamed_; ///< Pointer to the T object
        const std::string partName_;///< The partition name
        const std::string isName_;///< The IS server name
        const bool clean_;///< Flag set to delete or not the T object
        const bool collect_;///< Flag set to collect the info to put in IS

    };

    /**
     * \class AgentIS ISBridge.h "ProcessManager/ISBridge.h"
     *
     * \brief This class derives from the ISBridge one and implements the IS structure
     *        containing information related to the ProcessManager Agent.
     *        Agent information in a partition is created when it starts a process for the
     *        first time in that partition and regularly using the same Daemon IPCAlarm.
     *
     * \sa update_all_is().  
     */

class AgentIS: public ISBridge<PMGPublishedAgentDataNamed> {

    public:

        /**
         * \brief The constructor (see ISBridge constructors).
         *
         * \throw pmg::Failed_Create_IS
         */

        AgentIS(const std::string& partName, const std::string& isServerName);

        /**
         * \brief Overloaded constructor.
         */

        AgentIS(const std::string& partName, const std::string& isServerName, PMGPublishedAgentDataNamed* pData);

        /**
         * \brief The destructor
         */

        virtual ~AgentIS();

    protected:

        /**
         * \brief Avoid object copy and assignment
         */

        AgentIS(const AgentIS& rhs);
        AgentIS& operator=(const AgentIS& rhs);

        /**
         * \brief It collects all the needed Agent information.
         *
         * \param lowLevelInfo Include or not low level Agent information.
         *
         * \throw pmg::Failed_Collect_Info Some error occurred while collecting information.
         */

        void collectInfo(bool lowLevelInfo);
};

class Manifest;
// Forward declaration

/**
 * \class ProcessIS ISBridge.h "ProcessManager/ISBridge.h"
 *
 * \brief This class derives from the ISBridge one and implements the IS structure
 *        containing information related to the processes.
 *        Process information is created when the process is started and is updated
 *        everytime its status changes and regularly using an IPCAlarm in the Daemon class.
 *
 * \sa update_all_is().
 */

class ProcessIS: public ISBridge<PMGPublishedProcessDataNamed> {

    public:

        /**
         * \brief The constructor.
         *        With respect to the ISBridge constructor it takes an extra argument (manifest).
         *
         * \param manifest Pointer to the Manifest object used to retrieve process information.
         *
         * \throw pmg::Failed_Create_IS
         */

        ProcessIS(const Manifest* const manifest, const std::string& partName, const std::string& isServerName);

        /**
         * \brief Overloaded constructor.
         */

        ProcessIS(const Manifest* const manifest,
                  const std::string& partName,
                  const std::string& isServerName,
                  PMGPublishedProcessDataNamed* pData);

        /**
         * \brief The destructor.
         */

        virtual ~ProcessIS();

    protected:

        /**
         * \brief Avoid object copy and assignment
         */

        ProcessIS(const ProcessIS& rhs);
        ProcessIS& operator=(const ProcessIS& rhs);

        /**
         * \brief It collects all the needed process information.
         *
         * \param lowLevelInfo Include or not low level Agent information.
         *
         * \throw pmg::Failed_Collect_Info Some error occurred while collecting information.
         */

        void collectInfo(bool lowLevelInfo);

    private:

        const Manifest* const pManifest_; ///< Pointer to the Manifest object.
};

}
}

#endif
