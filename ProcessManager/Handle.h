#ifndef PMG_HANDLE_H
#define PMG_HANDLE_H

#include <iostream>
#include <memory>

#include "ProcessManager/Exceptions.h"

#include <system/Host.h>

namespace daq {
  namespace pmg {

    typedef unsigned int pmg_port_t;   ///< Definition for the port number the server is listening to. 
    typedef unsigned long pmg_id_t;    ///< Definition for the application unique id.
    
    /**
     * \class Handle Handle.h "ProcessManager/Handle.h"
     *
     * \brief This class represents a ProcessManager handle with all the relevant information
     *        It contains no actual code to perform operations, only setter, getter and translation methods. 
     */

    class Handle {

    public:

      static const char* const PREFIX;             ///< The handle prefix (i.e., "pmg://").
      static const char* const SEPARATOR;          ///< The handle section separator (i.e., "/").
      static const char* const PORT_SEPARATOR;     ///< The separator which identifies the port number (i.e., ":").
      static const pmg_port_t  DEFAULT_PORT_MARK;  ///< The default port number (i.e., \e 0).

      /**
       * \brief Constructor.
       *
       * \param server The host of the server where the process runs. 
       * \param part_name The partition name of the process.
       * \param app_name The application name of the process.
       * \param id The unique identifier for the process,
       *           (this id desambiguates multiple processes with the same application and partition name). 
       * \param port The port number for the server connection (optional parameter). 
       *
       * \throw pmg::Invalid_Handle The Handle cannot be built.
       */ 

      Handle(const System::Host &server,
	     const std::string &part_name,
	     const std::string &app_name,
	     pmg_id_t id,
	     pmg_port_t port = DEFAULT_PORT_MARK);

      /**
       * \brief Constructor.
       *
       * \param server_name The host of the server where the process runs. 
       * \param part_name The partition name of the process.
       * \param app_name The application name of the process.
       * \param id The unique identifier for the process,
       *           (this id desambiguates multiple processes with the same application and partition name). 
       * \param port The port number for the server connection (optional parameter).       
       *
       * \throw pmg::Invalid_Handle The Handle cannot be built.
       */

      Handle(const std::string &server_name,
	     const std::string &part_name,
	     const std::string &app_name,
	     pmg_id_t id,
	     pmg_port_t port = DEFAULT_PORT_MARK);

      /**
       * \brief It parses a string as a ProcessManager handle.
       *
       * \param handle The process handle as a string.
       *
       * \throw pmg::Invalid_Handle The Handle cannot be built.
       */

      Handle(const std::string &handle);

      /**
       * \brief Copy constructor.
       *
       * \param handle The original handle.
       *
       * \throw pmg::Invalid_Handle The Handle cannot be built.
       */

      Handle(const Handle &handle);
      
      /**
       * \brief Constructor with host and partial (local) handle.
       *
       * \param host The host.
       * \param local_handle Local handle part (i.e., partition/application/id).
       *
       * \throw pmg::Invalid_Handle The Handle cannot be built.
       */

      Handle(System::Host &host,
	     const std::string &local_handle);

      /** 
       * \brief Constructor with host and partial (local) handle.
       * \param server_name The host as string.
       * \param local_handle Local handle part (i.e., partition/application/id).
       *
       * \throw pmg::Invalid_Handle The Handle cannot be built.
       */

      Handle(const std::string &server_name,
	     const std::string &local_handle);

      /**
       * \brief Destructor: it deletes the Handle::m_server object.
       */

      ~Handle();
      
      /**
       * \brief It calls toString().
       */

      operator std::string() const;

      /**
       * \brief It compares the current handle with another one.
       *
       * \param other The handle to compare with.
       *
       * \return \c TRUE if the handles are the same, otherwise \c FALSE.
       */

      bool equals(const pmg::Handle & other) const;

      /**
       * \brief It converts the handle to a string.
       *
       * \return The handle as a string.
       */

      const std::string toString() const;

      /**
       * \brief It gets the path of the handle as a string (no prefix, useful for files).
       *
       * \return The path of the handle as a string
       */
      
      const std::string full_path() const;

      /**
       * \brief It gets the server name as a string.
       *
       * \return The server name as a string
       */
 
      const std::string& server() const;

      /**
       * \brief It gets the port number used by the server.
       *
       * \return The port number used by the server.
       */

      pmg_port_t port() const;

      /**
       * \brief It gets the partition name.
       *
       * \return The partition name.
       */

      const std::string& partitionName() const;

      /**
       * \brief It gets the application name.
       *
       * \return The application name.
       */

      const std::string& applicationName() const;

      /**
       * \brief It gets the application unique id.
       *
       * \return The application unique id.
       */

      pmg_id_t id() const;

      /**
       * \brief It prints description into stream.
       *
       * \param stream The stream description is sent to.
       */
      
      void printTo(std::ostream &stream) const;

    private:
      
      std::unique_ptr<System::Host> m_server;                 /**< \brief Pointer to server host */
      pmg_port_t    m_port;                                   /**< \brief Connection port on server */
      std::string   m_part_name;                              /**< \brief Partition name */
      std::string   m_app_name;                               /**< \brief Application name */
      pmg_id_t      m_unique_id;                              /**< \brief Application unique identity */
    
    }; // Handle
  } 
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Handle& handle) ;

#endif
