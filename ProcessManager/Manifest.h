/*
 *  Manifest.cxx
 *
 *  Created by Matthias Wiesmann on 15.02.05.
 *
 */

#ifndef PMG_MANIFEST_H
#define PMG_MANIFEST_H

#include "ProcessManager/ProcessData.h"
#include "system/MapFile.h"
#include "system/StringMemoryArea.h"
#include "system/Executable.h"
#include "system/Process.h"
#include "system/User.h"

namespace daq {
  namespace pmg {
    
    /**
     * \class Manifest Manifest.h "ProcessManager/Manifest.h"
     * 
     * \brief Memory map wrapper for \c ProcessData struture.
     *
     *        This class is memory mapped \c ProcessData structure. 
     *        It offers methods to read and write the fields in that structure. 
     *        Before calling any of those method, the object should be mapped in 
     *        memory. 
     *        Scalar types are simply wrapped by setter/getter methods. 
     *        Strings are stored in the \c m_data part of the \c ProcessData
     *        structure. 
     *
     * \sa ProcessData 
     */
    
    class Manifest : public System::MapFile, public System::StringMemoryArea {

    protected:

      static const char* const DEFAULT_PATH;           ///< \brief The default path to find manifest file.
      static const char* const SIGNATURE;              ///< \brief The Signature used to mark the file.
      virtual const char* string_area_read() const;    ///< \brief It reads pointer to string area.
      virtual char* string_area_write();               ///< \brief It write pointer to string area.
      virtual offset_t last_string() const;            ///< \brief It gets the offset of last string.
      virtual void last_string(offset_t offset);       ///< \brief It sets the offset of last string.
      virtual size_t string_area_size() const;         ///< \brief The size of string area.
      const ProcessData* process_read_data() const;    ///< \brief The read pointer to Process Data.
      ProcessData* process_write_data() const;         ///< \brief The write pointer to Process Data.

    public:

      static const char* const FILENAME;               ///< \brief The default filename.
      static const char* const DEFAULT_CONTROL;        ///< \brief The default control \e FIFO name.
      static const char* const DEFAULT_OUT;            ///< \brief The default output stream.
      static const char* const DEFAULT_REPORT;         ///< \brief The default report file \e FIFO.
      static const char* const DEFAULT_ERR_MSG;        ///< \brief The default string to fill error message at manifest init.

      Manifest(const System::File &file, bool read_mode, bool write_mode, mode_t perm = 0777);

      // General usage methods

      /** 
       * \brief It sets the signature in the manifest file.
       *       
       *        The signature consists of the 0 terminated c-string \"pmg\" 
       *        at the begnining of the file and the 0xff character in the first 
       *        position of the character array the actual strings are stored
       *        starting at position 1. 
       */

      void sign() const;                            

      /** 
       * \brief It checks if the manifest file is valid.
       * 
       * \return \c TRUE If the signature is valid.
       */

      bool check_sign() const;

      /**
       * \brief It gives the version of the map.
       *
       * \return The version of the map.
       */
                     
      unsigned int version() const;                

      // Process state

      /** 
       * \brief It finds the process state. 
       *
       * \return The process state. 
       */

      PMGProcessState process_state() const;

      /** 
       * \brief It sets the process state.
       * 
       * \param state The new state. 
       */

      void process_state(PMGProcessState state);

      /** 
       * \brief It finds the process id.
       * 
       * \return The pid of the process.
       */

      System::Process process_id() const;

      /** 
       * It sets the process id.
       * 
       * \param p The new process id. 
       */
  
      void process_id(const System::Process &p);

      /** 
       * \brief It gets the process id of the Launcher process.
       * 
       * \return The Launcher process identifier. 
       */

      System::Process launcher_pid() const;

      /** 
       * \brief It sets the process id for the Launcher process.
       *
       * \param p The Launcher process identifier.
       */

      void launcher_pid(const System::Process &p);

      /** 
       * \brief It gets the owner of the process.
       * 
       * \return The user that owns the process. 
       */

      System::User user() const;

      /** 
       * \brief It sets the owner of the process.
       * 
       * \param user The user that owns the process. 
       */
               
      void user(const System::User &user);

      /** 
       * \brief It gets the process exit code.
       * 
       * \return The process exit code. 
       */

      int exit_code() const;

      /** 
       * \brief It sets the process exit code.
       * 
       * \param code The process exit code.  
       */

      void exit_code(int code);

      /** 
       * \brief It gets the signal associated with the process.
       *
       * \return The signal number.
       */
           
      unsigned int exit_signal() const;

      /**
       * \brief It sets the signal associated with the process.
       *
       * \param code The signal number.
       */

      void exit_signal(unsigned int code);

      /**
       * \brief It gets the \e RM token associated with the process.
       *
       * \return The Resource Manager token.
       */

      long rm_token() const;

      /**
       * \brief It sets the \e RM token associated with the process.
       *
       * \param token The Resource Manager token.
       */

      void rm_token(long token);

      // Resource and time methods

      /** 
       * \brief It gets the resource usage structure writing pointer.
       * 
       * \return Writing pointer to the resource usage structure.
       */

      struct rusage *resource_usage_ptr();

      /** 
       * \brief It gets the resource usage structure reading pointer.
       * 
       * \return Reading pointer to the resource usage structure.
       */

      const struct rusage *resource_usage_ptr() const;

      /**
       * \brief It gets write pointer for start time.
       *
       * \return The write pointer for start time.  
       */

      time_t *start_time_ptr();

      
      /**
       * \brief It gets read pointer for start time.
       *
       * \return The read pointer for start time. 
       */

      const time_t *start_time_ptr() const;

      /**
       * \brief It gets write pointer for stop time.
       *
       * \return The write pointer for stop time. 
       */

      time_t *stop_time_ptr();

      /**
       * \brief It gets read pointer for stop time.
       *
       * \return The read pointer for stop time. 
       */

      const time_t *stop_time_ptr() const;              

      // Program arguments

      /**
       * \brief It resolves the binary path into an executable object.
       * 
       * \return Object representing the executable described in the Manifest.
       */

      System::Executable executable() const;

      /**
       * \brief It sets the path of the executable.
       * 
       * \param exec Executable object.
       *
       * \sa BinaryPath.
       */

      void executable(const System::Executable &exec);

      /**
       * \brief It gets the working directory.
       * 
       * \return File object representing the working directory. 
       */

      System::File working_directory() const;

      /**
       * \brief It sets the working directory.
       *
       * \param dir File object representing the new working directory. 
       */

      void working_directory(const System::File &dir);   

      /**
       * \brief It gets the input file for the process.
       * 
       * \return File object representing the process input file.
       */

      System::File input_file() const;
      
      /**
       * \brief It sets the input file for the process.
       *
       * \param input File object representing the process input file. 
       */

      void input_file(const System::File &input);

      /**
       * \brief It gets the output file for the process.
       * 
       * \return File object representing the process output file.
       */

      System::File output_file() const;

      /**
       * \brief It sets the output file for the process.
       *
       * \param output File object representing the process output file. 
       */
      
      void output_file(const System::File &output);

      /** 
       * \brief It gets the error file for the process.
       * 
       * \return File object representing the process error file.
       */

      System::File error_file() const;

      /** 
       * \brief It sets the error file for the process.
       * 
       * \param error File object representing the process error file.
       */

      void error_file(const System::File &error);

      /**
       * \brief It gets the output file permissions.
       *
       * \return The output file permissions.
       */

      mode_t output_file_permission() const;

       /**
       * \brief It sets the output file permissions.
       *
       * \param perm The output file permissions.
       */

      void output_file_permission(mode_t perm);

      /** 
       * \brief It sets the process parameter list.
       *
       * \param params The collection of parameters.
       */

      void parameters(const System::Executable::param_collection &params); 

      /** 
       * \brief It extracts the process parameter list from the manifest.
       *
       * \return A parameter collection. 
       */

      System::Executable::param_collection parameters() const;

      /** 
       * \brief It sets the environnement collection.
       * 
       * \param envs Collection of all key value-pairs. 
       */

      void environnements(const System::Executable::env_collection &envs);
      
      /**
       * \brief It rReturns the set of environment variables.
       * 
       * \return A collection of environment variables
       */

      System::Executable::env_collection environnements() const;

      /** 
       * \brief It returns the process init timeout.
       * \return The timeout value.
       */

      int init_timeout() const;

      /** 
       * \brief It sets the process init timeout.
       *
       * \param timeout The timeout for getting to the running state. 
       */

      void init_timeout(const int timeout);

      /**
       * \brief It returns the process "automatic kill" timeout.
       * \return The timeout value.
       */

      int auto_kill_timeout() const;

      /**
       * \brief It sets the process "automatic kill" timeout.
       *
       * \param timeout The timeout before the process is automatically killed.
       */

      void auto_kill_timeout(const int timeout);

      // Launcher control

      /** 
       * \brief It gets the control file for the process. 
       *        This should be a path to a \e FIFO file.
       * 
       * \return File object describing the \e FIFO.
       */

      System::File control_file() const;

      /** 
       * \brief It sets the control file for the process. 
       *        This should be a path to a FIFO file.
       * 
       * \param control File object describing the \e FIFO.
       */

      void control_file(const System::File &control);

      /**
       * \brief It gets the permission associated with the control file.
       * 
       * \return Control file permissions. 
       */

      mode_t control_file_permission() const;

      /** 
       * \brief It sets the permission associated with the control file.
       * \param perm The permissons for the control file.
       */

      void control_file_permission(mode_t perm);

      /** 
       * \brief It gets the report \e FIFO.
       *        This file gets written when the state of a process is updated.
       *
       * \return The file object describing the \e FIFO.
       */
        
      System::File report_file() const;

      /** 
       * \brief It sets the report \e FIFO.
       *        This file gets written when the state of a process is updated.
       *
       * \param report The file object describing the \e FIFO.
       */

      void report_file(const System::File &report);

      // Error reporting

      /** 
       * \brief It gets the error message associated with the Launcher.
       *
       * \return Pointer to a c-string in the shared memory map.
       */

      const char* error_msg() const;

      /** 
       * \brief It gets the error message associated with the Launcher.
       *
       * \return Pointer to a c-string in the shared memory map.
       */

      void error_msg(const char* msg);

      // Handle

      /**
       * \brief It sets the process handle.
       *  
       * \param handle The new handle
       *
       * \note The handle is considered an opaque character string.
       */

      void handle(const std::string &handle); 

      /** 
       * \brief It gets the process handle.
       *
       * \return The process handle.
       *
       * \note The handle is considered an opaque character string.
       */

      std::string handle() const;             

      /**
       * \brief It sets the process sw object
       */
      
      void rm_swobject(const std::string &swobject);

      /**
       * \brief It gets the process sw object.
       */

      std::string rm_swobject() const;              
      
      /**
       * \brief It sets the requesting host
       */
      
      void requesting_host(const std::string &hostName);

      /**
       * \brief It gets the requesting host
       */

      std::string requesting_host() const;

      // Pretty print method

      /** 
       * \brief It pretty prints the content of the manifest to a stream.
       *
       * \param stream The destination stream.
       */

      void print(std::ostream &stream) const;

    } ; // Manifest
  } 
} // daq::pmg

std::ostream& operator<<(std::ostream& stream, const daq::pmg::Manifest& manifest);

/** 
 * This macro builds the version number,
 * the high word number is the number
 * the low word is the size of the header section in bytes
 */

#define PMG_MANIFEST_VERSION (0x10000 | sizeof(ProcessDataHeader))

#endif

