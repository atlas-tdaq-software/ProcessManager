#ifndef PMG_DEFS_H
#define PMG_DEFS_H

#include <pmgpub/pmgpub.hh>
#include <pmgpriv/pmgpriv.hh>

#include <string>
#include <map>
#include <vector>
#include <memory>

#include <boost/function.hpp>

typedef pmgpub::ProcessState                                PMGProcessState;          ///< Definition for a structure containing the process state.
typedef pmgpub::ProcessStatusInfo                           PMGProcessStatusInfo;     ///< Definition for a structure containing all the information about the process.
typedef pmgpub::ResourceInfo                                PMGResourceInfo;          ///< Definition for a structure containing process resource usage.
typedef pmgpriv::SignalException                            PMGSignalException;       ///< Definition for a structure describing the exception raised while sending a signal to a process. 

#define PMGProcessState_NOTAV                               pmgpub::NOTAV              ///< Process not available
#define PMGProcessState_REQUESTED                           pmgpub::REQUESTED          ///< The process has been requested to start.
#define PMGProcessState_LAUNCHING                           pmgpub::LAUNCHING          ///< The process is going to be started (i.e., the Launcher has been started).
#define PMGProcessState_CREATED                             pmgpub::CREATED            ///< The System::Process object describing the process has been created.
#define PMGProcessState_RUNNING                             pmgpub::RUNNING            ///< The process is running.
#define PMGProcessState_EXITED                              pmgpub::EXITED             ///< The process has exited normally.
#define PMGProcessState_SIGNALED                            pmgpub::SIGNALED           ///< The process has exited because of a signal.
#define PMGProcessState_SYNCERROR                           pmgpub::SYNCERROR          ///< The timeout waiting for the process to confirm it is actively running is elapsed.
#define PMGProcessState_FAILED                              pmgpub::FAILED             ///< The process has failed to start.

#define PMGSignalException_NOT_ALLOWED_BY_AM                pmgpriv::NOT_ALLOWED_BY_AM                ///< Signal not allowed by the AccessManager
#define PMGSignalException_APPLICATION_NOTFOUND_INVALID     pmgpriv::APPLICATION_NOTFOUND_INVALID     ///< Signal not sent because the Application object is not found or invalid
#define PMGSignalException_MANIFEST_UNMAPPED                pmgpriv::MANIFEST_UNMAPPED                ///< Signal not sent because the Manifest has already been unmapped
#define PMGSignalException_LAUNCHER_NOT_RUNNING             pmgpriv::LAUNCHER_NOT_RUNNING             ///< Signal not sent because the associated Launcher is no more running
#define PMGSignalException_FIFO_ERROR                       pmgpriv::FIFO_ERROR                       ///< Signal not sent because of an error writing/opening a FIFO
#define PMGSignalException_UNEXPECTED                       pmgpriv::UNEXPECTED                       ///< Signal not sent because of an unexpected error
#define PMGSignalException_UNKNOWN                          pmgpriv::UNKNOWN                          ///< Signal not sent because of an unknown error

typedef std::map<std::string, std::string>                  pmg_env_type;                ///< Definition for a map containing environment variables.
typedef std::map<std::string, std::string>::iterator        pmg_env_type_iterator;       ///< Definition for a \c pmg_env_type iterator.
typedef std::map<std::string, std::string>::const_iterator  pmg_env_type_const_iterator; ///< Definition for a const \c pmg_env_type iterator.

typedef std::vector<std::string>                            pmg_arg_type;             ///< Definition for a vector containing the arguments to be passed to the process. 
typedef std::vector<std::string>::iterator                  pmg_arg_type_iterator;    ///< Definition for a \c pmg_arg_type iterator.

#define LIBRARY_VAR "LD_LIBRARY_PATH"

namespace daq {
  namespace pmg {

    class Process;

    typedef boost::function<void (std::shared_ptr<Process>, void*)> CallBackFncPtr; ///< Definition for a pointer to the user callback function.

    // These are used as constants in the pmgClient.java
    // (if something changes here, changes should be made there too)
    const std::string ProcessManagerServerPrefix = "AGENT_";   ///< Prefix for the ProcessManager name in IPC (i.e., AGENT_<host full name>).
    const std::string ProcessManagerSeparator = "|PMGD|";

    /**
     * \brief String array describing all the possible process states.
     *
     * \sa defs.h, daq::pmg::p_state_t
     */

    const char* const p_state_strings[] = { "not defined",
					    "requested", 
					    "launching", 
					    "created", 
					    "running", 
					    "exited", 
					    "signaled", 
					    "sync error", 
					    "failure" };


  } 
  
} // daq::pmg

#endif
