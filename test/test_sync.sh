#!/usr/bin/env bash
#$Id$

echo "*********************************************************************"
echo "*"
echo "* TEST SYNC: PMG SYNC mechanism test"
echo "*"
echo "*********************************************************************"
echo 

export TDAQ_IPC_INIT_REF="file:$HOME/ipc_root.ref"
export TDAQ_LOGS_PATH="/tmp/pmg_test_sync.$$"

export TDAQ_PARTITION="pmg_test"
export TDAQ_DB_DATA="$PWD/temp.data.xml"
export PMG_ROOT="$PWD/.."

export SYNC_PRODUCE_SCRIPT="${PMG_ROOT}/src/test/produce_sync.sh"

export TEST_DB="${PMG_ROOT}/test/test.data.xml"
$PMG_ROOT/test/CS_modify_tag.py

. $PMG_ROOT/test/CS_test_functions.sh

mkdir -p $TDAQ_LOGS_PATH
pushd $TDAQ_LOGS_PATH >/dev/null

export LD_LIBRARY_PATH=${PMG_ROOT}/${CMTCONFIG}:${LD_LIBRARY_PATH}
export PATH=${PMG_ROOT}/${CMTCONFIG}:${PATH}

start_global_ipc_server
start_partition_ipc_server
start_rm_server

ipc_ls -lR
ipc_ls -p pmg_test -l

start_new_pmgserver

popd >/dev/null

SyncTest -s $SYNC_PRODUCE_SCRIPT -b 10 -a 10
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 2 -a 1
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 5 -a 3
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 6 -a 2
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 3 -a 0
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 7 -a 0
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 10 -a 10
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 2 -a 10
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 5 -a 0
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 6 -a 2
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 3 -a 0
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""
SyncTest -s $SYNC_PRODUCE_SCRIPT -b 7 -a 0
echo "*************"
ps -axwwu | grep pmglauncher | grep -v grep
ps -axwwu | grep produce_sync.sh | grep -v grep
echo ""

test_status=$?

if [ ${test_status} -eq 0 ]; then
    echo "Test was successful."
else
    echo "Test was not successful."
fi

stop_test ${test_status}

