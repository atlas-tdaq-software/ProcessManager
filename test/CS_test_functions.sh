# $Id$
# Author: Dietrich.Liko@cern.ch
#
# Functions to support running of tests 
#

trap stop_test INT QUIT HUP

me=`basename $0 .sh`

function stop_test
{
   trap "" INT QUIT HUP
   
   ipc_rm    -p $TDAQ_PARTITION -i "rc/controller" -n ".*"
   sleep 2
   ipc_rm -p $TDAQ_PARTITION -i ".*" -n ".*" 
   sleep 2
   ipc_rm -p $TDAQ_PARTITION -i ".*" -n ".*" -f

   if [ ${rc_trans_pid:-0} -gt 0 ]; then
      kill $rc_trans_pid
   fi

   if [ ${rm_pid:-0} -gt 0 ]; then
      kill $rm_pid
   fi

   if [ ${pmg_pid:-0} -gt 0 ]; then
      kill $pmg_pid
      #pmg_kill_agent
   fi
   
   if [ ${ipc_pid:-0} -gt 0 ]; then
      kill $ipc_pid
   fi

   if [ ! -z $TDAQ_LOGS_PATH ]; then
      here=$PWD
      pushd $TDAQ_LOGS_PATH >/dev/null
      gtar cvzf $here/$me.tar.gz * >/dev/null
      popd >/dev/null
      rm -rf $TDAQ_LOGS_PATH
   fi
      
   echo
   #if [ ${1:-100} -ne 0 ]; then
   #   echo "ERROR: Test failed."
   #else
   #   echo "SUCCESS: Test succeded."
   #fi
   echo

   popd

   #exit ${1:-100}
   

}

function start_global_ipc_server()
{
   test_ipc_server >/dev/null
   if [ $? -ne 0 ]; then
      echo "INFO [$me] Starting global ipc_server ..."
      ipc_server &>ipc_Server.log </dev/null &
      ipc_pid=$!
      local cnt=0
      sleep 1
      test_ipc_server >/dev/null
      while [ $? -ne 0 ]; do
        sleep 1
        let cnt+=1
        if [ $cnt -gt 10 ]; then
           echo "ERROR [$me] Global ipc_server not ready. Abort test."
           stop_test 100
        fi
        test_ipc_server >/dev/null
      done      
   fi
}

function start_partition_ipc_server()
{
   local cnt=0
   test_ipc_server -p $TDAQ_PARTITION >/dev/null
   while [ $? -eq 0 ]; do
      echo "INFO [$me] Partition $TDAQ_PARTITION already running. Waiting ..."
      sleep 1
      let cnt+=1
      if [ $cnt -gt 10 ]; then
         echo "INFO [$me] Give up waiting for partition $TDAQ_PARTITION. Abort test."
         stop_test 100
      fi
      test_ipc_server -p $TDAQ_PARTITION >/dev/null
   done
   echo "INFO [$me] Starting ipc_server for partition $TDAQ_PARTITION ..."
   ipc_server -p $TDAQ_PARTITION &>ipc_server.log </dev/null &
   local cnt=0
   sleep 1
   test_ipc_server -p $TDAQ_PARTITION >/dev/null
   while [ $? -ne 0 ]; do
      sleep 1
      let cnt+=1
      if [ $cnt -gt 10 ]; then
         echo "ERROR [$me] Partition ipc_server not ready. Abort test."
         stop_test 100
      fi
      test_ipc_server -p $TDAQ_PARTITION >/dev/null
   done      
}

function start_rm_server()
{
   test_corba_server -p initial -c "rm/RM_DB" -n "RM_Server" >/dev/null
   if [ $? -ne 0 ]; then
      echo "INFO [$me] Starting rm_server ..."
      rm_server &>rm_Server.log </dev/null &
      rm_pid=$!
      local cnt=0
      sleep 1
      test_corba_server -p initial -c "rm/RM_DB" -n "RM_Server"  >/dev/null
      while [ $? -ne 0 ]; do
        sleep 1
        let cnt+=1
        if [ $cnt -gt 10 ]; then
           echo "ERROR [$me] rm_server not ready. Abort test."
           stop_test 100
        fi
        test_corba_server -p initial -c "rm/RM_DB" -n "RM_Server" >/dev/null
      done      
   fi

   test_rm_server -p $TDAQ_PARTITION >/dev/null
   if [ $? -ne 0 ]; then
      echo "INFO [$me] Register partition $TDAQ_PARTITION into resource manager..."
      rm_register_partition -p $TDAQ_PARTITION -i OKS -d $TDAQ_DB_DATA  >rm_register_partition.log 2>&1 </dev/null
      local cnt=0
      sleep 1
      test_rm_server -p $TDAQ_PARTITION >/dev/null
      while [ $? -ne 0 ]; do
         sleep 1
         let cnt+=1
         if [ $cnt -gt 10 ]; then
            echo "ERROR [$me] impossible to register partition into resource manager. Abort test."
            stop_test 100
         fi
         test_rm_server -p $TDAQ_PARTITION >/dev/null
      done
   fi      
}

function start_mrs_server()
{
   echo "INFO [$me] Starting mrs_server ..."
   mrs_server -p $TDAQ_PARTITION -b mrs_server.backup >mrs_server.log 2>&1 </dev/null &
   local cnt=0
   sleep 1
   test_mrs_server -p $TDAQ_PARTITION >/dev/null
   while [ $? -ne 0 ]; do
      sleep 1
      let cnt+=1
      if [ $cnt -gt 10 ]; then
         echo "ERROR [$me] mrs_server not ready. Abort test."
         stop_test 100
      fi
      test_mrs_server -p $TDAQ_PARTITION >/dev/null
   done      
}

function start_is_server()
{
   echo "INFO [$me] Starting is_server $1 ..."
   is_server -p $TDAQ_PARTITION -n $1 -b is_server_$1.backup -H 10 &>is_server_$1.log </dev/null &
   echo "INFO [$me] is_server $1 launched..."
   local cnt=0
   sleep 1
   test_is_server -p $TDAQ_PARTITION -n $1     
    result=$?
   while [ $result -ne 0 ]; do
    echo $result
   echo "INFO [$me] Waiting test result for is_server $1 ..."
      sleep 1
      let cnt+=1
      if [ $cnt -gt 10 ]; then
         echo "ERROR [$me] is_server $1 not ready. Abort test."
         stop_test 100
      fi
      test_is_server -p $TDAQ_PARTITION -n $1 
      result=$?
   done      
}

function start_pmg_agent()
{
   local host=`hostname`
   test_pmg_agent -H $host >/dev/null
   if [ $? -ne 0 ]; then
      echo "INFO [$me] Starting pmg_agent ..."
      pmg_agent &>pmg_agent.log </dev/null &
      pmg_pid=$!
      local cnt=0
      sleep 1
      test_pmg_agent -H $host >/dev/null
      while [ $? -ne 0 ]; do
         sleep 1
         let cnt+=1
         if [ $cnt -gt 20 ]; then
            echo "ERROR [$me] pmg_agent not ready. Abort test."
            stop_test 100
         fi
         test_pmg_agent -H $host >/dev/null
      done
   fi
}

function start_new_pmgserver()
{
   local host=`hostname -f`
   test_pmg_agent -H $host >/dev/null
   if [ $? -ne 0 ]; then
      echo "INFO [$me] Starting new pmgserver ..."
      pmgserver &>pmgserver.log </dev/null &
      pmg_pid=$!
      local cnt=0
      sleep 1
      test_pmg_agent -H $host >/dev/null
      while [ $? -ne 0 ]; do
         sleep 1
         let cnt+=1
         if [ $cnt -gt 20 ]; then
            echo "ERROR [$me] pmgserver not ready. Abort test."
            stop_test 100
         fi
         test_pmg_agent -H $host >/dev/null
      done
   fi
}


function start_rdb_server()
{
   echo "INFO [$me] Starting $1 rdb_server ..."
   rdb_server -p $TDAQ_PARTITION -D $TDAQ_DB_DATA -d $1 -v 0 &>rdb_server_$1.log </dev/null &
   local cnt=0
   sleep 2
   test_rdb_server -p $TDAQ_PARTITION -n $1 >/dev/null
   while [ $? -ne 0 ]; do
      sleep 1
      let cnt+=1
      if [ $cnt -gt 10 ]; then
         echo "ERROR [$me] rdb_server $1 not ready. Abort test."
         stop_test 100
      fi
      test_rdb_server -p $TDAQ_PARTITION -n $1 >/dev/null
   done      
}

function start_rc_transition_monitor()
{
   echo "INFO [$me] Starting rc transition monitor ..."
   rc_transition_monitor --oks -d $TDAQ_DB_DATA >rc_transition.log 2>rc_transition.err </dev/null &
   rc_trans_pid=$!  
}
