#!/usr/bin/env bash
#$Id$

echo "*********************************************************************"
echo "*"
echo "* TEST 1: PMG start/kill, ProcessDescriptionList and ProcessList test"
echo "*"
echo "*********************************************************************"
echo 

export TDAQ_IPC_INIT_REF="file:$HOME/ipc_root.ref"
export TDAQ_LOGS_PATH="/tmp/pmg_test1.$$"

export TDAQ_PARTITION="pmg_test"
export TDAQ_DB_DATA="$PWD/temp.data.xml"
export PMG_ROOT="$PWD/.."

export TEST_DB="${PMG_ROOT}/test/test.data.xml"
$PMG_ROOT/test/CS_modify_tag.py

. $PMG_ROOT/test/CS_test_functions.sh

mkdir -p $TDAQ_LOGS_PATH
pushd $TDAQ_LOGS_PATH >/dev/null

export LD_LIBRARY_PATH=${PMG_ROOT}/${CMTCONFIG}:${LD_LIBRARY_PATH}
export PATH=${PMG_ROOT}/${CMTCONFIG}:${PATH}

start_global_ipc_server
start_partition_ipc_server
#start_rm_server
start_new_pmgserver

popd >/dev/null

DescriptionTest -n 50

stop_test $?

