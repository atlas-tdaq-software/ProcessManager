$Id$

ProcessManager Status
---------------------

25.08.05
Contact: jean-philippe.pellet@epfl.ch

(This file is to be deleted when it becomes irrelevant)


I have tested ProcessDescriptionList and ProcessList with the start() and signal(), stop(), methods respectively. I have not written new tests for them since I changed their desctructor and added the remove_and_delete() and delete_all() methods.

I have created new simple standalone tools; see folder src/helpers. We have pmg_test_agent, which returns 0 if a pmgserver could be kind of "ping'd" on the passed host (actually I added a method alive() on the server side which just returns true); pmg_kill_partition, which asks Singleton to kill the passed partition; and pmg_killall_on_host, which contacts the pmgserver of the passed host and asks it to terminate all apps running there. I have only tested the first app; kill_partition() is actually not totally implemented in Singleton since we need to query the IPC server to find all pmgservers involved in a specific partition, and I haven't had a chance to try and find out how to do that.

The current test that we have (test1.sh) should run fine -- namely construct a two-item ProcessDescriptionList, start it, get a ProcessList back, issue three queries to Singleton (the third of which should fail and return null -- think about changing the possibly hard-coded hostname in the test files), wait a few seconds and then stop or kill the ProcessList. This is were we should get two SIGNALLED and then two EXITED callbacks; this is still to be fixed.

Try a 'grep FIXME -r .' or 'grep ASSUMPTION -r .' in the src/ folder to check other details where I was unsure about my code.

Other things that I have implemented but not tested include most query methods/getters on Process/ProcessImpl and on Proxy, as well as add/remove/getX on ProxyTable.

What still needs to be fixed: we have this callback problem about not getting a notification about each state and about SIGNALLED not being described as an end state; for the moment we also have this problem on the server side when the log directory is missing (we should probably call a C++ equivalent for 'mkdir -p' somewhere).

--
