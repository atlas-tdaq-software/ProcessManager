General changes
---------------
- In your requirement file you should explicitly link the libraries processManagerClient and processManagerCore
- The header files of the ProcessManager server part are no more installed. Anyway your code should not include any of those files
- Some constant definitions are moved to the defs.h header to make the code cleaner
- Libraries have been a little ri-organized. Now 5 libraries exists:
libprocessManagerClient.so, libprocessManagerCore.so, libprocessManagerDaemon.so, libprocessManagerLauncher.so, libprocessManagerServer.so

Client interface changes
------------------------
- DFMutex mutexes have been replaced by the boost ones
- Now the "unlink" method can be called by the Process object even within the callback. In the previous implementation this caused a deadlock. The "unlink" method un-registers the callback so you could think to call it once you receive a callback and the process exited
- The Process object is removed internally (by a separate thread) once the process is no more running and in unlinked. So consider the unlink as a "delete". Once you unlink you should no more use that Process pointer
- The "is_running()" method in the Process class has been renamed to "exited()". This is better since the method checks if the process in in one of the end states
- The p_state_t enum (contating all the process states) has been removed. There is no need to have a second enum since it is already defined in idl. The defs.h header file contains some typedef you can find useful
- The structure describing the process status is now initialized as soon as the process is started. Previously this was done when the first callback arrived. This is much more safe
- The only Process class constructor takes a Proxy as the only arguments. In this way the Proxy must be already there when a new Process is created.

Server changes
--------------
- boost mutexes and threads replace the DFThread implementation
- The Partition object is removed once the partition is no more present in IPC and all the started processes in that partition exited
- The Agent does not publish in IS for partitions in which no more processes are running
- The partition directory containing FIFOs and manifests is now removed once the Partition object itself has been deleted
- Fixed a possible problem causing the RM resources to not be freed
- If something goes wrong while starting a process the Application object is tagged as invalid
- The Application object is now removed once the report thread exits (i.e., the associated process exited). Previously the Application object was removed once a new Application was started
- The last two actions should reduce the probability to have a lookup returning a handle for a process which already exited (anyway the lookup->get_process->status chain is recommended)
- If the pmglauncher is not started after 5 seconds only a warning is sent. This avoids receiving unexpected callbacks
- Better handling of the situation in which the report thread can not be started
- Fixed problem when calling 'select' on the report FIFO
- The directory where FIFOs and manifest will be placed can now be passed to the ProcessManager via an env variable: TDAQ_PMG_MANIFEST_AND_FIFOS_DIR
- The manifest size has been increased to 32KB 
